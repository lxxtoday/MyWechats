<?php
error_reporting(0);

if ( extension_loaded('zlib') ) ob_start('ob_gzhandler');

// --- 基本设置
$gettype = 'js';
$allowed_content_types = array('js');
$offset = 60 * 60 * 24 * 7; // 过期7天

// 获取参数
$module = strip_tags($_GET['module']); // 模块
$lang = strip_tags($_GET['lang']); // 语言

if ( $gettype == 'css' ) {
    $content_type = 'text/css';
} elseif ( $gettype == 'js' ) {
    $content_type = 'application/x-javascript';
}

// --- 头部设置
header("Content-type:" . $content_type . ";Charset:utf-8");
// header("cache-control: must-revalidate");
header("cache-control: max-age=" . $offset);
header("Last-Modified: " . gmdate("D, d M Y H:i:s", time()) . "GMT");
header("Pragma: max-age=" . $offset);
header("Expires:" . gmdate("D, d M Y H:i:s", time() + $offset) . " GMT");

set_cache_limit($offset); // 缓存时间
ob_start("compress");

// --- 获取所有语言文件
$files = scandir('../application', 0); // 扫描目录下
$now_module = array();
$list = array();

foreach ( $files as $v ) {
    if ( is_dir('../application/' . $v) and ($v!=".") and ($v!="..") ) { // 排除当前目录和父目录
        $file = '../application/' . $v . '/lang/' . $lang . '.php'; // 语言文件
        if ( is_file($file) ) {
            if ( ucfirst($module) == $v ) { // 当前末块
                $now_module = include $file;
            } elseif ( $v == "common" ) { // 公共模块
                $common_lang = include $file;
            } else { // 其他语言
                $list[] = include $file;
            }
        }
    }
}

$list[] = $now_module; // 当前末块
$list[] = $common_lang; // 公共模块

// --- 所有语言
$module_lang = array();
foreach ( $list as $val ) {
    $module_lang = array_merge($module_lang, (array)$val);
}

// --- 生成js格式语言文件
$fileData = "var LANG = new Array();\n";
foreach ( $module_lang as $key => $val ) {
    $val = str_replace("'", "‘", $val); // 处理掉单引号
    $content[] = "LANG['{$key}'] = '{$val}';";
}
$fileData .= implode("\n", $content);

echo $fileData;

function set_cache_limit($second=1) {
    $second = intval($second);
    if ( $second == 0 ) return;

    $etag = time() . "||" . base64_encode($_SERVER['REQUEST_URI']); // 当前文件

    if ( !isset($_SERVER['HTTP_IF_NONE_MATCH']) ) {
        header("Etag:$etag", true, 200);
        return;
    } else {
        $id = $_SERVER['HTTP_IF_NONE_MATCH'];
    }

    list($time, $uri) = explode("||", $id);

    if ( $time < (time() - $second) ) { // 过期了，发送新tag
        header("Etag:$etag", true, 200);
    } else { // 未过期，发送旧tag
        header("Etag:$id", true, 304);
        exit(-1);
    }
}

if ( extension_loaded('zlib') ) ob_end_flush();