/**
 * 操纵toastor的便捷类
 * @type {{success: success, error: error, info: info, warning: warning}}
 */
var toast = {
    /**
     * 成功提示
     * @param text 内容
     * @param title 标题
     */
    success: function (text,url) {
        toast.show(text,{icon: 1},url);
    },
    /**
     * 失败提示
     * @param text 内容
     * @param title 标题
     */
    error: function (text,url) {
       toast.show(text,{icon: 2},url);
    },
    /**
     * 信息提示
     * @param text 内容
     * @param title 标题
     */
    info: function (text,url) {
        toast.show(text,{icon: 3},url);
    },
    /**
     * 警告提示
     * @param text 内容
     * @param title 标题
     */
    warning: function (text, title,url) {
        toast.show(text,{icon: 3},url);
    },

    show: function (text,option,url) {
        if (url){
        
        layer.msg(text, option,function(){
//                      setTimeout(function () {
                            window.location.href =url;
//                      }, 100);
        });
        }else{
        layer.msg(text, option);    
        }
       
    },
    /**
     *  显示loading
     * @param text
     */
    showLoading: function (i) {
       layer.load(i);
    },
    /**
     * 隐藏loading
     * @param text
     */
    hideLoading: function () {
        layer.closeAll('loading');
    }
}