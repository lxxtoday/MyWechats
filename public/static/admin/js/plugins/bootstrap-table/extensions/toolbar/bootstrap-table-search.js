!function($) {
	'use strict';

	var sprintf = $.fn.bootstrapTable.utils.sprintf; // 打印工具

	$.extend($.fn.bootstrapTable.defaults, {
		showFilter: false,
		idFilterForm: 'filter-form',
		actionFilterForm: '',
	});
	$.extend($.fn.bootstrapTable.defaults.icons, { // 图标扩展
		filterIcon: 'glyphicon-search icon-search'
	});

	$.extend($.fn.bootstrapTable.locales, { // 国际化 - 英语
        filterButtonText: function () {
            return 'Search';
        }
    });
    $.extend($.fn.bootstrapTable.defaults, $.fn.bootstrapTable.locales);

	var BootstrapTable = $.fn.bootstrapTable.Constructor,
		_initToolbar = BootstrapTable.prototype.initToolbar,		
		_initSearch = BootstrapTable.prototype.initSearch;

	BootstrapTable.prototype.initToolbar = function() { // 初始化工具栏
		_initToolbar.apply(this, Array.prototype.slice.apply(arguments));

		if (!this.options.showFilter) return;

		var that = this,
			html = [];

		html.push(sprintf('<button class="btn btn-default%s' + '" type="button" name="filter" title="搜索">', that.options.iconSize === undefined ? '' : ' btn-' + that.options.iconSize));
		html.push(sprintf('<i class="%s %s"></i>', that.options.iconsPrefix, that.options.icons.filterIcon))
		html.push('</button>');
		this.$toolbar.find('>.btn-group.columns-right').prepend(html.join(''));

		that.$toolbar.find('button[name="filter"]').off('click').on('click', function() {
			if ( $(".filter-wrap").css("display") == "none" ) {
				$(".filter-wrap").show();
			} else {
				$(".filter-wrap").hide();
			}
			
			$(".filter-wrap").toggleClass("col-sm-2");
			$(".main-wrap").toggleClass("col-sm-12").toggleClass("col-sm-10");
			
			that.resetView();
		});
	};
	BootstrapTable.prototype.initSearch = function () {
		_initSearch.apply(this, Array.prototype.slice.apply(arguments));

		if (!this.options.showFilter) return;

		var that = this;
		
		$("#searchForm").find('input.btn-search').off('click').on('click', function(event) {
			that.onFilter(event);
		});
	};
	BootstrapTable.prototype.onFilter = function (event) {
		var that = this, searches = $("#searchForm").find('.filter');
		
		searches.each(function(){
			var text = $.trim($(this).val());
			var $field = $(this)[0].name;

			if ( $.isEmptyObject(that.filterColumnsPartial) ) that.filterColumnsPartial = {};

			if ( text ) {
				that.filterColumnsPartial[$field] = text;
			} else {
				delete that.filterColumnsPartial[$field];
			}
		});

		this.onSearch($.extend(event, {
			currentTarget: searches[0]
		}));

		if ( this.options.pageSize ) this.options.pageNumber = 1;
		this.updatePagination();
	};
}(jQuery);
!function($) { // 国际化 - 中文
    'use strict';

    $.fn.bootstrapTable.locales['zh-CN'] = {
        filterButtonText: function () {
            return '搜索';
        }
    };
    $.extend($.fn.bootstrapTable.defaults, $.fn.bootstrapTable.locales['zh-CN']);
}(jQuery);