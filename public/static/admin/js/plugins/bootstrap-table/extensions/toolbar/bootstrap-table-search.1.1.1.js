!function($) {
	'use strict';

	var sprintf = $.fn.bootstrapTable.utils.sprintf; // 打印工具

	$.extend($.fn.bootstrapTable.defaults, {
		showFilter: false,
		idFilterForm: 'filter-form',
		actionFilterForm: '',
		onFilter: function (field, text) { // 自定义事件处理
			return false;
		}
	});

	$.extend($.fn.bootstrapTable.defaults.icons, { // 图标扩展
		filterIcon: 'glyphicon-search icon-search'
	});
	$.extend($.fn.bootstrapTable.Constructor.EVENTS, { // 事件扩展
		'filter.bs.table': 'onFilter'
	});

	var BootstrapTable = $.fn.bootstrapTable.Constructor,
		_initToolbar = BootstrapTable.prototype.initToolbar,		
		_initSearch = BootstrapTable.prototype.initSearch;

	BootstrapTable.prototype.initToolbar = function() { // 初始化工具栏
		_initToolbar.apply(this, Array.prototype.slice.apply(arguments));

		if (!this.options.showFilter) return;

		var that = this,
			html = [];

		html.push(sprintf('<button class="btn btn-default%s' + '" type="button" name="filter" title="搜索">', that.options.iconSize === undefined ? '' : ' btn-' + that.options.iconSize));
		html.push(sprintf('<i class="%s %s"></i>', that.options.iconsPrefix, that.options.icons.filterIcon))
		html.push('</button>');
		this.$toolbar.find('>.btn-group').prepend(html.join(''));

		that.$toolbar.find('button[name="filter"]').off('click').on('click', function() {
			if ( $(".search-wrap").css("display") == "none" ) {
				$(".search-wrap").show();
			} else {
				$(".search-wrap").hide();
			}
			
			$(".search-wrap").toggleClass("col-sm-2");
			$(".main-wrap").toggleClass("col-sm-12").toggleClass("col-sm-10");
		});
	};
	BootstrapTable.prototype.initSearch = function () {
		_initSearch.apply(this, Array.prototype.slice.apply(arguments));

		if (!this.options.showFilter) return;

		var that = this;

		$("#searchForm").find('.form-control').off('blur').on('blur', function(event) {
			that.onFilter(event);
		});
	};
	BootstrapTable.prototype.onFilter = function (event) {
		var text = $.trim($(event.currentTarget).val());
		var $field = $(event.currentTarget)[0].name;

		if ($.isEmptyObject(this.filterColumnsPartial)) this.filterColumnsPartial = {};

		if (text) {
			this.filterColumnsPartial[$field] = text;
		} else {
			delete this.filterColumnsPartial[$field];
		}

		if ( this.options.pageSize ) this.options.pageNumber = 1;
		this.onSearch(event);
		this.updatePagination(); // 更新分页系统
		this.trigger('filter', $field, text); // 触发自定义方法
	};
}(jQuery);