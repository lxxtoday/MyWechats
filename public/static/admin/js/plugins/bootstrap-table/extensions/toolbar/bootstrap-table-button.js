!function($){
    'use strict';

    var sprintf = $.fn.bootstrapTable.utils.sprintf; // 打印工具

    $.extend($.fn.bootstrapTable.defaults, { // 默认配置
        leftButtons: true, // 左浮动按钮组
        leftButtonsAlign: "left" // 按钮浮动
    });

    var BootstrapTable = $.fn.bootstrapTable.Constructor, // 构造函数
        _initToolbar = BootstrapTable.prototype.initToolbar;

    BootstrapTable.prototype.initToolbar = function() { // 初始化工具栏
        _initToolbar.apply(this, Array.prototype.slice.apply(arguments));

        if ( !this.options.leftButtons ) return;

        var that = this,
            html = [];
        var buttons = $("#buttonList").find("span");

        if ( buttons ) {
	        html.push(sprintf('<div class="columns columns-%s btn-group pull-%s" role="group">', this.options.leftButtonsAlign, this.options.leftButtonsAlign));

	        buttons.each(function(){
		        html.push(sprintf('<a type="button" href="%s" title="%s" class="btn btn-default %s %s %s">',
		        	$(this).data("url"), $(this).data("title"), $(this).data("confirm")?'confirm':'', $(this).data("ajax")?'ajax-post':'', that.options.iconSize===undefined?'':'btn-'+that.options.iconSize));
		        html.push(sprintf('<i class="%s %s"></i> %s', that.options.iconsPrefix, $(this).data("icon"), $(this).data("showtitle")==true ? $(this).data("title") : ""))
		        html.push('</a>');
	        });

	        html.push('</div>');
	    }
        that.$toolbar.prepend(html.join(''));
    };
}(jQuery);