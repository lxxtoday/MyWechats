!function($){
    'use strict';

    var firstLoad = false; // 首次加载
    var sprintf = $.fn.bootstrapTable.utils.sprintf; // 打印工具

    var showAvdSearch = function(pColumns, searchTitle, searchText, that) { // 显示高级搜索
    	var thatid = "#avdSearchModal_" + that.options.idTable,
    		thatcontentid = '#avdSearchModalContent_' + that.options.idTable;

        if (!$(thatid).hasClass("modal")) {
            var vModal = sprintf("<div id=\"%s\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"mySmallModalLabel\" aria-hidden=\"true\">", thatid);
            vModal += "<div class=\"modal-dialog modal-xs\">";
            vModal += " <div class=\"modal-content\">";
            vModal += "  <div class=\"modal-header\">";
            vModal += "   <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" >&times;</button>";
            vModal += sprintf("   <h4 class=\"modal-title\">%s</h4>", searchTitle);
            vModal += "  </div>";
            vModal += "  <div class=\"modal-body modal-body-custom\">";
            vModal += sprintf("   <div class=\"container-fluid\" id=\"%s\" style=\"padding-right: 0px;padding-left: 0px;\" >", thatcontentid);
            vModal += "   </div>";
            vModal += "  </div>";
            vModal += "  </div>";
            vModal += " </div>";
            vModal += "</div>";
            $("body").append($(vModal));

            var vFormAvd = createFormAvd(pColumns, searchText, that),
                timeoutId = 0;
            $(thatcontentid).append(vFormAvd.join(''));

            $('#' + that.options.idForm).off('keyup blur', 'input').on('keyup blur', 'input', function (event) {
                clearTimeout(timeoutId);
                timeoutId = setTimeout(function () {
                    that.onColumnAdvancedSearch(event); // 搜索
                }, that.options.searchTimeOut);
            });

            $("#btnCloseAvd" + "_" + that.options.idTable).click(function() { // 关闭
                $("#avdSearchModal" + "_" + that.options.idTable).modal('hide');
            });
        }

        $(thatid).modal();
    };
    var createFormAvd = function(pColumns, searchText, that) { // 表单部分
        var htmlForm = [];

        htmlForm.push(sprintf('<form class="form-horizontal" id="%s" action="%s" >', that.options.idForm, that.options.actionForm));

        for (var i in pColumns) {
            var vObjCol = pColumns[i];
            if (!vObjCol.checkbox && vObjCol.visible && vObjCol.searchable) {
                htmlForm.push('<div class="form-group">');
                htmlForm.push(sprintf('<label class="col-sm-4 control-label">%s</label>', vObjCol.title));
                htmlForm.push('<div class="col-sm-6">');
                htmlForm.push(sprintf('<input type="text" class="form-control input-md" name="%s" placeholder="%s" id="%s">', vObjCol.field, vObjCol.title, vObjCol.field));
                htmlForm.push('</div>');
                htmlForm.push('</div>');
            }
        }

        htmlForm.push('<div class="form-group">');
        htmlForm.push('<div class="col-sm-offset-9 col-sm-3">');
        htmlForm.push(sprintf('<button type="button" id="btnCloseAvd%s" class="btn btn-default" >%s</button>', "_" + that.options.idTable, searchText));
        htmlForm.push('</div>');
        htmlForm.push('</div>');
        htmlForm.push('</form>');

        return htmlForm;
    };

    $.extend($.fn.bootstrapTable.defaults, { // 默认配置
        advancedSearch: false, // 高级搜索
        idForm: 'advancedSearch', // 表单标识
        actionForm: '', // 表单提交地址
        idTable: undefined, // 控件标识
        onColumnAdvancedSearch: function (field, text) { // 搜索事件
            return false;
        }
    });
    $.extend($.fn.bootstrapTable.defaults.icons, { // 图标扩展
        advancedSearchIcon: 'glyphicon-chevron-down'
    });
    $.extend($.fn.bootstrapTable.Constructor.EVENTS, { // 事件扩展
        'column-advanced-search.bs.table': 'onColumnAdvancedSearch'
    });
    $.extend($.fn.bootstrapTable.locales, { // 国际化
        formatAdvancedSearch: function() {
            return 'Advanced search';
        },
        formatAdvancedCloseButton: function() {
            return "Close";
        }
    });
    $.extend($.fn.bootstrapTable.defaults, $.fn.bootstrapTable.locales); // 国际化合并

    var BootstrapTable = $.fn.bootstrapTable.Constructor, // 构造函数
        _initToolbar = BootstrapTable.prototype.initToolbar,        
        _load = BootstrapTable.prototype.load,
        _initSearch = BootstrapTable.prototype.initSearch;

    BootstrapTable.prototype.initToolbar = function() { // 初始化工具栏
        _initToolbar.apply(this, Array.prototype.slice.apply(arguments));

        if (!this.options.search) return;
        if (!this.options.advancedSearch) return;
        if (!this.options.idTable) return;

        var that = this,
            html = [];

        html.push(sprintf('<div class="columns columns-%s btn-group pull-%s" role="group">', this.options.buttonsAlign, this.options.buttonsAlign));
        html.push(sprintf('<button class="btn btn-default%s" type="button" name="advancedSearch" title="%s">', that.options.iconSize === undefined ? '' : ' btn-' + that.options.iconSize, that.options.formatAdvancedSearch()));
        html.push(sprintf('<i class="%s %s"></i>', that.options.iconsPrefix, that.options.icons.advancedSearchIcon))
        html.push('</button></div>');
        that.$toolbar.prepend(html.join(''));

        that.$toolbar.find('button[name="advancedSearch"]')
        .off('click').on('click', function() {
            showAvdSearch(that.columns, that.options.formatAdvancedSearch(), that.options.formatAdvancedCloseButton(), that);
        });
    };
    BootstrapTable.prototype.load = function(data) { // 加载
        _load.apply(this, Array.prototype.slice.apply(arguments));

        if (!this.options.advancedSearch) return;

        if (typeof this.options.idTable === 'undefined') {
            return;
        } else {
            if (!firstLoad) { // 首次加载
                var height = parseInt($(".bootstrap-table").height());
                height += 10;
                $("#" + this.options.idTable).bootstrapTable("resetView", {height: height});

                firstLoad = true;
            }
        }
    };
    BootstrapTable.prototype.initSearch = function () { // 初始化
        _initSearch.apply(this, Array.prototype.slice.apply(arguments));

        if (!this.options.advancedSearch) return;

        var that = this;
        var fp = $.isEmptyObject(this.filterColumnsPartial) ? null : this.filterColumnsPartial;

        this.data = fp ? $.grep(this.data, function (item, i) {
            for (var key in fp) {
                var fval = fp[key].toLowerCase();
                var value = item[key];
                value = $.fn.bootstrapTable.utils.calculateObjectValue(that.header,
                    that.header.formatters[$.inArray(key, that.header.fields)],
                    [value, item, i], value);

                if (!($.inArray(key, that.header.fields) !== -1 &&
                    (typeof value === 'string' || typeof value === 'number') &&
                    (value + '').toLowerCase().indexOf(fval) !== -1)) return false;
            }
            return true;
        }) : this.data;
    };
    BootstrapTable.prototype.onColumnAdvancedSearch = function (event) {
        var text = $.trim($(event.currentTarget).val()); // 标题
        var $field = $(event.currentTarget)[0].id; // 内容

        if ($.isEmptyObject(this.filterColumnsPartial)) this.filterColumnsPartial = {};

        if (text) {
            this.filterColumnsPartial[$field] = text;
        } else {
            delete this.filterColumnsPartial[$field];
        }

        this.options.pageNumber = 1;
        this.onSearch(event);
        this.updatePagination(); // 更新分页
        this.trigger('column-advanced-search', $field, text); // 触发
    };
}(jQuery);