$(function(){
    $('.checkbox').change(function(e){
        var fieldName = $(this).attr('data-field-name');
        var checked = $('.checkbox[data-field-name=' + fieldName + ']:checked');

        var result = [];
        for ( var i = 0; i < checked.length; i++ ) {
            var checkbox = $(checked.get(i));
            result.push(checkbox.attr('value'));
        }

        result = implode(',', result);
        $('.checkbox-hidden[data-field-name=' + fieldName + ']').val(result);
    });
});
// --- 函数
function implode(x, list) {
    var result = "";
    for ( var i = 0; i < list.length; i++ ) {
        if ( result == "" ) {
            result += list[i];
        } else {
            result += ',' + list[i];
        }
    }

    return result;
}
function upAttachVal(input, type, attachId) {
    console.log(input);
    var attachVal = input.val();
    var attachArr = attachVal.split(',');
    var newArr = [];

    for ( var i in attachArr ) {
        if ( attachArr[i] !== '' && attachArr[i] !== attachId.toString() ) newArr.push(attachArr[i]);
    }
    type === 'add' && newArr.push(attachId);
    input.val(newArr.join(','));
}