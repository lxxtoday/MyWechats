$(function(){
	$('.ajax-post').click(function(){
		var target, query, form;
		var target_form = $(this).attr('target-form'); // 对象
		var need_confirm = false; // 需要确认
		var that = this;

		if ( (target = $(this).attr('href')) || (target = $(this).attr('url')) ) {
			form = $('[name=btSelectItem]');

			if ( form.get(0).nodeName == 'INPUT' ) {
				form.each(function(k, v){
					if ( v.type == 'checkbox' && v.checked == true ) need_confirm = true;
				});

				if ( need_confirm && $(this).hasClass('confirm') ) {
					var confirm_info = $(that).attr('confirm-info');

					confirm_info = confirm_info ? confirm_info : "确认要执行该操作吗?";
					if ( !confirm(confirm_info) ) return false;
				}

				var $table = $("#cusTable");

				query = JSON.stringify($table.bootstrapTable('getSelectionsId'));
			}

			if ( query=='' && $(this).attr('hide-data') != 'true' ) {
				alert('请勾选操作对象！');
				return false;
			}

			$(that).addClass('disabled').attr('autocomplete', 'off').prop('disabled', true);

			$.post(target, {id: query}).success(function(data){
				if ( data.status == 1 ) {
					if ( data.url ) {
						console.log("44444");
						alert(data.info + ' 页面即将自动跳转~');
					} else {
						console.log("555555");
						alert(data.info);
					}

					setTimeout(function(){
						if ( data.url ) {
							location.href = data.url;
						} else if ( $(that).hasClass('no-refresh') ) {
							$('#top-alert').find('button').click();
							$(that).removeClass('disabled').prop('disabled', false);
						} else {
							location.reload();
						}
					}, 1500);
				} else {
					console.log(data.info);
					alert(data.info);
					
					setTimeout(function () {
						if ( data.url ) {
							location.href = data.url;
						} else {
							$('#top-alert').find('button').click();
							$(that).removeClass('disabled').prop('disabled', false);
						}
					}, 1500);
				}
			});
		}

		return false;
	});
});