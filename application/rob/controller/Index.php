<?php
namespace app\rob\controller;
use think\Controller;




class Index extends Controller{


    /**
     * 微信消息接口入口
     * 所有发送到微信的消息都会推送到该操作
     * 所以，微信公众平台后台填写的api地址则为该操作的访问地址
     * $this 498944516@qq.com
     */
    public function index($id=0){
      
       return $this->fetch();

    }

    public function game($game=4,$openid=''){
       $game_info=model('rob/RobGame')->info($game);
       $fans=model('we/WeFans')->info($openid);
       if ( request()->isPost() ) { 
         $data=input('post.');
         if (!model('RobGameLog')->check($game,$openid)) $this->error('每天只有一次兑换机会');

       //处理奖品
         $card_id='';
         if ($data['score']>$game_info['gift1_score'] and $data['score']<=$game_info['gift2_score']) $card_id=$game_info['gift1'];
         if ($data['score']>$game_info['gift2_score'] and $data['score']<=$game_info['gift3_score']) $card_id=$game_info['gift2'];
         if ($data['score']>$game_info['gift3_score'] and $data['score']<=$game_info['gift4_score']) $card_id=$game_info['gift3'];
         if ($data['score']>$game_info['gift4_score'] and $data['score']<=$game_info['gift5_score']) $card_id=$game_info['gift4'];
         
         if ($data['score']>$game_info['gift5_score'] ) $card_id=$game_info['gift5'];
         if($card_id){
         $custom_message=['msgtype'=>'wxcard','wxcard'=>['card_id'=>$card_id]];
         $error= custom_message($fans['openid'],$custom_message);
         }else{
         $error='游戏分数不够！';
         }
         if (!$error){
          $data['aid']=$fans['aid'];
          $data['status']=0;
          $data['create_time']=time();
          $res=db('RobGameLog')->insert($data);
          if($res)$this->success('兑换成功！卡券已经发送到您的微信');
         }
         $this->error('失败！'.$error);
 
       }else{

         $gift_str=model('rob/RobGame')->getGiftStr($game);
         $this->assign('myJssdk',['appid'=>$fans['appid']]);
         $this->assign('game_info',$game_info);
         $this->assign('fans',$fans);
         $this->assign('gift_str',$gift_str);
         return $this->fetch('2048');
       }

    }

    public function news($id=0,$appid='',$from_openid=''){
        $news=model('rob/RobNews')->info($id);
        $from_fans=model('we/WeFans')->info($from_openid);
       if($news['url']){
        header('Location:'.$news['url']);
        die;
       }
       
       $this->assign('news',$news);
       $this->assign('from_fans',$from_fans);
       $this->assign('myJssdk',['appid'=>$appid]);
       return $this->fetch();

    }

    
  

}