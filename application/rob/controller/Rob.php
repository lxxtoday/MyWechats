<?php
namespace app\admin\controller;

use app\admin\my\MyPage;
use app\admin\my\MyConfig;

class Rob extends Base{

    private $type=['text'=>'文本回复','news'=>'图文回复','more'=>'多条回复'];
    public function rob() {
        $my = new MyPage();
       
        $my ->title("机器人管理") ->buttonNew("robEdit")
           
            ->keyId()
            ->keyText('title', '名称')
            ->keyText('description', '简介')
            ->actionEdit()
            ->actionStatus('rob')
            ->map('title', '名称')
             ->mapStatus();

       
           $data=my_data('rob');
           return $my->data($data)->fetch();
       
    }



    public function robEdit($id=0) {
        if ( request()->isPost() ) { 
            $res = model('rob/Rob')->editData();
            if ($res>0) $this->success('更新成功','rob');
            $this->error('更新失败');
        } else {
            
            $data=my_edit('rob/Rob',$id);
            $my = new MyConfig();
            return  $my->title("编辑机器人")->keyId()
                        ->keyText('title', '名称')
                        ->keyText('description', '简介')
                        ->buttonSubmit()->buttonBack()
                        ->data($data)->fetch();
        }
    }



     public function answer() {
        $my = new MyPage();
       
        $my ->title("问答管理")->buttonNew(url('answerEdit', ['type'=>'text']), '文本消息', ['showText' => true])
            ->buttonNew(url('answerEdit', ['type'=>'news']), '图文消息', ['showText' => true])
         
            
            ->keyId()
            ->keyText('title', '名称')
            // ->keyText('description', '简介')
            ->keyText('keyword', '关键词')
            ->keyJoin('rob', '机器人', 'id', 'title', 'rob')
            // ->keyMap('type','回复类型',$this->type)
            ->keyText('message', '回复')
            ->keyCreateTime()
            ->actionEdit()
            ->actionStatus('RobAnswer')
            ->map('title', '名称')
            ->mapStatus();

       
           $data=my_data('RobAnswer');
           return $my->data($data)->fetch();
       
    }



    public function answerEdit($id=0) {
        
        if ( request()->isPost() ) { 
            $res = model('rob/RobAnswer')->editData();
            if ($res>0) $this->success('更新成功','answer');
            $this->error('更新失败');
        } else {
            $robs=my_select('rob');
            $data=my_edit('rob/RobAnswer',$id);

            $my = new MyConfig();
            return  $my->title("编辑问答")->keyId()
                        ->keyText('title', '名称')
                        ->keyText('description', '简介')
                        ->keyText('keyword', '关键词')
                        ->keySelect('type', '类型',$this->type)
                        ->keySelect('rob', '机器人',$robs)
                        ->keyTextArea('message', '回复')
                        ->keySelect('limit_sex', '可见性别',['所有','男','女'])
                        ->keySelect('limit_time', '限制时间',[1=>'每小时',2=>'每天',3=>'每周',4=>'每月',5=>'每年'])
                        ->keyText('limit_week', '限制星期')
                        ->keySelect('send_type', '发送方式',['随机','追加'])
                        ->keyText('limit_times', '限制次数','留空或者0为不限制')
                        ->keyText('limit_um', '限制人次','留空或者0为不限制')
                        
                        ->group('基本','fa fa-wechat','id,title,description,keyword,type,rob,message')
                        ->group('条件','fa fa-wechat','limit_sex,limit_time,limit_week,send_type,limit_times,limit_um')
                        ->buttonSubmit()->buttonBack()
                        ->data($data)->fetch();
        }
    }

    public function news() {
        $my = new MyPage();
       
        $my ->title("图文素材库") ->buttonNew("newsEdit")
         
            ->keyId()
            ->keyText('title', '标题')
            ->keyText('description', '描述')
            ->keyText('url', '外部链接')
            ->keyText('picurl', '小图片')
            ->keyText('picurl_big', '大图片')
            
            ->actionEdit()
            ->actionStatus('RobNews')
            ->map('title', '名称')
            ->mapStatus();

       
           $data=my_data('RobNews');
          return $my->data($data)->fetch();
        
    }



    public function newsEdit($id=0) {
        
        if ( request()->isPost() ) { 

            $res = model('rob/RobNews')->editData();
            if ($res>0) $this->success('更新成功','news');
            $this->error('更新失败');
        } else {
            
            $data=my_edit('rob/RobNews',$id);

            $my = new MyConfig();
            return  $my->title("编辑机器人")->keyId()
                        ->keyText('title', '标题')
                        ->keyText('description', '描述')
                        ->keyText('url', '外部链接')
                        ->keyImage('picurl', '小图片')
                        ->keyImage('picurl_big', '大图片')
                        ->keyEditor('content', '正文')
                        
                        ->buttonSubmit()->buttonBack()
                        ->data($data)->fetch();
        }
    }

    public function game() {
        $my = new MyPage();
        
        $my ->title("游戏素材")
            
            ->buttonNew("gameEdit")
          
            ->keyId()
            ->keyText('title', '标题')
            ->keyJoin('appid', '微信', 'appid', 'title', 'we')
            ->keyJoin('gift1', '奖品1', 'card_id', 'title', 'we_card')
            ->keyJoin('gift2', '奖品2', 'card_id', 'title', 'we_card')
            ->keyJoin('gift3', '奖品3', 'card_id', 'title', 'we_card')
            ->keyJoin('gift4', '奖品4', 'card_id', 'title', 'we_card')
            ->keyJoin('gift5', '奖品5', 'card_id', 'title', 'we_card')
           
            
            
            ->actionEdit()
            ->actionStatus('RobGame')
            ->map('title', '名称')
            ->mapStatus();

       
           $data=my_data('RobGame');
           return $my->data($data)->fetch();
        
    }



    public function gameEdit($id=0) {
        
        if ( request()->isPost() ) { 

            $res = model('rob/RobGame')->editData();
            if ($res>0) $this->success('更新成功','more');
            $this->error('更新失败');
        } else {
            $cards=my_select('we_card','card_id','title');
            $wes=my_select('we','appid','title');
            $data=my_edit('rob/RobGame',$id);

            $my = new MyConfig();
            return  $my->title("编辑机器人")->keyId()
                        ->keyText('title', '标题')
                        ->keySelect('appid', '微信',$wes)
                        ->keyText('gift1_score', '1等奖分数')
                        ->keyText('gift2_score', '2等奖分数')
                        ->keyText('gift3_score', '3等奖分数')
                        ->keyText('gift4_score', '4等奖分数')
                        ->keyText('gift5_score', '5等奖分数')
                        
                        ->keySelect('gift1', '奖品1',$cards)
                        ->keySelect('gift2', '奖品2',$cards)
                        ->keySelect('gift3', '奖品3',$cards)
                        ->keySelect('gift4', '奖品4',$cards)
                        ->keySelect('gift5', '奖品5',$cards)
                        ->keySelect('gift6', '奖品6',$cards)
                       
                        
                        ->buttonSubmit()->buttonBack()
                        ->data($data)->fetch();
        }
    }


   

    public function media() {
        $my = new MyPage();
       
        $my ->title("图文素材库")->buttonNew("mediaEdit")
           
            ->keyId()
            ->keyText('title', '标题')
            ->keyText('description', '描述')
            ->keyText('media_id', 'media_id')
           
            
            
            ->actionEdit()
            ->actionStatus('RobMedia')
            ->map('title', '名称')
            ->mapStatus();

           $data=my_data('RobMedia');
           return $my->data($data)->fetch();
       
    }



    public function mediaEdit($id=0) {
        
        if ( request()->isPost() ) { 

            $res = model('Rob/RobMedia')->editData();
            if ($res>0) $this->success('更新成功','more');
            $this->error('更新失败');
        } else {
            
            $data=my_edit('Rob/RobMedia',$id);

            $my = new MyConfig();
            return  $my->title("编辑机器人")->keyId()
                        ->keyText('title', '标题')
                        ->keyText('description', '描述')
                        ->keyText('media_id', 'media_id')
                       
                        
                        ->buttonSubmit()->buttonBack()
                        ->data($data)->fetch();
        }
    }


    
   

  

}