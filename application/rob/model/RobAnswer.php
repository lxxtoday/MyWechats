<?php
// +----------------------------------------------------------------------
// | my
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://baiyf.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: $this <498944516@qq.com>
// +----------------------------------------------------------------------
namespace app\rob\model;

use think\Model;

class RobAnswer extends Model
{

    
    protected $insert = ['status'=>1];  
  
   

    /**
     * 获取机器人，微信被动响应的唯一入口
     * 微信开启的APP优先
     * 查询自定义机器人
     * 如果是客服时间则转人工客服
     * 最后到图灵机器人
     * 这个逻辑过程中会对进入和输出的关键词做解析！！！！！！！
     * 参数
     * keyword 关键字
     * data 参数信息数组
     * getType 是模糊匹配还是精准匹配
     * getApp  是否内置应用优先，默认优先。
     * */
    public function reply($keyword="my",$robs,$data=[],$getType=0 ){
    

        $map['rob']=['in',$robs];
        
        //默认为模糊查询，支持精准查询
        switch ($getType) {
            case 1:
                $map['keyword']=$keyword;
                break;
            case 2:
                $map['id']=$keyword;
                break;
            default:
                $map['keyword']=['like','%'.$keyword.'%'];
                break;
        }

        $reply=$this->infoByMap($map,$data);
        
        return $reply;
    }

    

    public function infoByMap($map="",$data=[]){

        $answer = $this->where($map)->find();

   
        if ($answer){

           if ($answer['limit_times']>0){
              $limit=model('rob/RobAnswerLog')->check($answer,$data);
              if($limit['code']==1){
                 $log['aid']=$data['aid'];
                 $log['openid']=$data['openid'];
                 $log['answer']=$answer['id'];
                 model('rob/RobAnswerLog')->add($log);
              }else{
                return $limit['error'];
              }
           }

            //处理more
            if ($answer['type']=='more'){
               return $this->more($answer,$data);
            }
            
      
         
          
          return $this->Format($answer,$data);
      }

    }

    //单独处理问答集合
    public function more($answer,$data){
 
      $map['id']=['in',$answer['message']];
      $answers=$this->where($map)->select();
   
      switch ($answer['send_type']) {
        case '0':  //随机
          $answers=array_values($answers); //从新编号键值
          $luck=rand(0,count($answers)-1);
          $reply=model('rob/RobAnswer')->infoByMap(['id'=>$answers[$luck]['id']],$data);
          
          break;

        case '1':  //追加
        
          $ok=1;
          foreach ($answers as $key => $value) {
            if ($ok==1)$reply=$this->infoByMap(['id'=>$value['id']],$data);
            if ($ok==2)custom_message($data['fans']['openid'],$value['id']);
            $ok=2;
          }
          
          break;

      }
      return $reply;
        
     
    }

    public function Format($answer,$data){
      
      switch ($answer['type']) {
            case 'text':
                $reply['type']='text';
                $message_first=mb_substr($answer['message'],0,1);
                if ("卡"==$message_first){
                    $card_id=str_replace("卡","",$answer['message']);
                    if (!$card_id){
                      $reply['message']=$answer['message'];
                    }else{
                      $card=model('we/weCard')->info($card_id);
                      $custom_message=['msgtype'=>'wxcard','wxcard'=>['card_id'=>$card['card_id']]];
                      $error= custom_message($data['openid'],$custom_message);
                      $reply['message']=$error?$error:'恭喜您，成功发送卡券';
                  }

                }else{
                  $reply['message']=$answer['message'];  
                }
                
                break;
            case 'news':
                $reply['type']='news';
                $articles=model('rob/RobNews')->reply($answer['message'],$data);
                $reply['message']=$articles;
                break;
            case 'image':
                $reply['type']='image';
                $pic=upload_media($data['appid'],$answer['message']);
                $reply['message']=$pic['media_id'];
                break;
            case 'more':
                $reply=model('rob/RobMore')->reply($answer['message'],$data);
                break;
            
            default:
                $reply=$answer;
                break;
          }
          return $reply;
    }



     

     public function editData(){

      $data=input("post.");

      $data['aid']=session('aid');
      
      if ($data['id']){
         $res = $this->allowField(true)->where('id',$data['id'])->update($data);
      }else{
         $res = $this->allowField(true)->data($data)->save();
      }
     
      return $res;
    }
   
}