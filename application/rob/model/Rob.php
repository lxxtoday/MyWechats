<?php
// +----------------------------------------------------------------------
// | my
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://baiyf.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: $this <498944516@qq.com>
// +----------------------------------------------------------------------
namespace app\rob\model;

use think\Model;

class Rob extends Model
{

    protected $table = "my_rob";
    protected $insert = ['status'=>1];

    public function info($key, $field = true)
    {
        
        if (!$key) return false;
        if (is_numeric($key)){
            $map['id'] = $key;
        }else{
            $map['title'] = $key;
        }

        $fans=$this->field($field)->where($map)->find();

        return $fans;
    }



     public function editData(){

      $data=input("post.");
      $data['aid']=session('aid');
      if (isset($data['id'])){
         $res = $this->allowField(true)->where('id',$data['id'])->update($data);
      }else{
         $res = $this->allowField(true)->data($data)->save();
      }
      
      return $res;
    }


  
  //根据关键字机器人回复，首先处理应用，然后处理回复
    public function reply($keyword='', $data,$getType=0,$getApp=true)
    {
        if (!$keyword) return false;
        
        $keyword=we_replace($keyword,$data); //对用户的keyword进行关键词处理，从而实现变量功能
     
       
        //优先应用处理 TODO::应用这里应该缓存以提高效率
        if($getApp==true) {
            $apps=[
               'F'=>'we',
               'P'=>'printer',
               'K'=>'we',
               'Q'=>'we',
               ];
               
            if ($apps){
                    foreach ($apps as $key => $app) {
                       if (strstr($keyword,$key)){
                        $data['keyword']=$key;
                        $data['before'] = substr($keyword,0,strrpos($keyword,$key));
                        $data['back']=str_replace($data['before'].$key,"",$keyword);
                        $reply=model($app.'/WeApp')->index($data);
                        return $reply;
                        }  
                    }
            }
        }
       
        //使用问答系统
        $answer_reply=model('rob/RobAnswer')->reply($keyword,$data['options']['rob'],$data,$getType);


        if (!$answer_reply){
            $answer_reply=$this->turing($keyword,$data['options']['turing']);
            if(!isset($answer_reply['type']))$answer_reply['message']="机器人：".$answer_reply['message']."【客服妹纸不在，智能机器人为您服务】";
        }

       
        //如果没有就使用图灵机器人
        if ($answer_reply) return $answer_reply;

    }

     // 图灵机器人
    // TODO :: 每个企业都可以去配置自己的图灵
    public function turing($keyword="",$api_key='9df6c5ea2698473f943d9a7a18d148d4') {

        $settings['api_url'] = 'http://www.tuling123.com/openapi/api';
        $settings['api_key'] = $api_key?$api_key:'9df6c5ea2698473f943d9a7a18d148d4';
        $api_url = $settings['api_url'] . "?key=" . $settings['api_key'] . "&info=" . $keyword;
         
        $result = file_get_contents ( $api_url );
        $result = json_decode ( $result, true );
        
       
        if ($result ['code'] > 40000 && $result['code'] < 40008) {
            if ($result ['code'] < 40008 && ! empty ( $result ['text'] )) {
                $reply['message']= $result ['text'];
                return $reply;
            } else {
                return false;
            }
        }

        switch ($result ['code']) {
            case '100000' :
                $reply['message']=$result['text'];
                return $reply ;
                break;
            case '200000' :
                $text = $result ['text'] . ',<a href="' . $result ['url'] . '">点击进入</a>';
                $reply['message']=$text;
                return $reply ;
               
                break;
            case '301000' :
                foreach ( $result ['list'] as $info ) {
                    $articles [] = array (
                            'Title' => $info ['name'],
                            'Description' => $info ['author'],
                            'PicUrl' => $info ['icon'],
                            'Url' => $info ['detailurl'] 
                    );
                }
                $reply['type']='news';
                $reply['message']= $articles;
                return $reply;
                break;
            case '302000' :
                foreach ( $result ['list'] as $info ) {
                    $articles [] = array (
                            'Title' => $info ['article'],
                            'Description' => $info ['source'],
                            'PicUrl' => $info ['icon'],
                            'Url' => $info ['detailurl'] 
                    );
                }
                $reply['type']='news';
                $reply['message']= $articles;
                return $reply;
                break;
            case '304000' :
                foreach ( $result ['list'] as $info ) {
                    $articles [] = array (
                            'Title' => $info ['name'],
                            'Description' => $info ['count'],
                            'PicUrl' => $info ['icon'],
                            'Url' => $info ['detailurl'] 
                    );
                }
                $reply['type']='news';
                $reply['message']= $articles;
                return $reply;
                break;
            case '305000' :
                foreach ( $result ['list'] as $info ) {
                    $articles [] = array (
                            'Title' => $info ['start'] . '--' . $info ['terminal'],
                            'Description' => $info ['starttime'] . '--' . $info ['endtime'],
                            'PicUrl' => $info ['icon'],
                            'Url' => $info ['detailurl'] 
                    );
                }
                $reply['type']='news';
                $reply['message']= $articles;
                return $reply;
                break;
            case '306000' :
                foreach ( $result ['list'] as $info ) {
                    $articles [] = array (
                            'Title' => $info ['flight'] . '--' . $info ['route'],
                            'Description' => $info ['starttime'] . '--' . $info ['endtime'],
                            'PicUrl' => $info ['icon'],
                            'Url' => $info ['detailurl'] 
                    );
                }
                $reply['type']='news';
                $reply['message']= $articles;
                return $reply;
                break;
            case '307000' :
                foreach ( $result ['list'] as $info ) {
                    $articles [] = array (
                            'Title' => $info ['name'],
                            'Description' => $info ['info'],
                            'PicUrl' => $info ['icon'],
                            'Url' => $info ['detailurl'] 
                    );
                }
                $reply['type']='news';
                $reply['message']= $articles;
                return $reply;
                break;
            case '308000' :
                foreach ( $result ['list'] as $info ) {
                    $articles [] = array (
                            'Title' => $info ['name'],
                            'Description' => $info ['info'],
                            'PicUrl' => $info ['icon'],
                            'Url' => $info ['detailurl'] 
                    );
                }
                $reply['type']='news';
                $reply['message']= $articles;
                return $reply;
                break;
            case '309000' :
                foreach ( $result ['list'] as $info ) {
                    $articles [] = array (
                            'Title' => $info ['name'],
                            'Description' => '价格 : ' . $info ['price'] . ' 满意度 : ' . $info ['satisfaction'],
                            'PicUrl' => $info ['icon'],
                            'Url' => $info ['detailurl'] 
                    );
                }
                $reply['type']='news';
                $reply['message']= $articles;
                return $reply;
                break;
            case '310000' :
                foreach ( $result ['list'] as $info ) {
                    $articles [] = array (
                            'Title' => $info ['number'],
                            'Description' => $info ['info'],
                            'PicUrl' => $info ['icon'],
                            'Url' => $info ['detailurl'] 
                    );
                }
                $reply['type']='news';
                $reply['message']= $articles;
                return $reply;
                break;
            case '311000' :
                foreach ( $result ['list'] as $info ) {
                    $articles [] = array (
                            'Title' => $info ['name'],
                            'Description' => '价格 : ' . $info ['price'],
                            'PicUrl' => $info ['icon'],
                            'Url' => $info ['detailurl'] 
                    );
                }
                $reply['type']='news';
                $reply['message']= $articles;
                return $reply;
                break;
            case '312000' :
                foreach ( $result ['list'] as $info ) {
                    $articles [] = array (
                            'Title' => $info ['name'],
                            'Description' => '价格 : ' . $info ['price'],
                            'PicUrl' => $info ['icon'],
                            'Url' => $info ['detailurl'] 
                    );
                }
                $reply['type']='news';
                $reply['message']= $articles;
                return $reply;
                break;
            default :
                if (empty ( $result ['text'] )) {
                    return false;
                } else {
                     $reply['message']=$result ['text'];
                    return  $reply;
                }
        }
        return true;
    }
}

