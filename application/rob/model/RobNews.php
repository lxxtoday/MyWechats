<?php
// +----------------------------------------------------------------------
// | my
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://baiyf.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: $this <498944516@qq.com>
// +----------------------------------------------------------------------
namespace app\rob\model;

use think\Model;

class RobNews extends Model
{

    protected $insert = ['status'=>1];

    public function info($id,$field=true){
      
    
        $map['id']=$id;
     
      $news = $this->field($field)->where($map)->find();
     
      return  $news;
    }

    public function reply($ids='',$data){
    	
       $list = $this->all($ids);
    
      	foreach($list as $key=>$new){
    			
                    $articles[$key]=[
        		            'Title' => $new->title,
        		            'Description' => $new->description,
        		            'Url' =>request()->domain().'/'.url('rob/index/news',['id'=>$new->id,'appid'=>$data['appid'],'from_openid'=>$data['openid']])
        		     ];
                 if ($key==0){
                  $articles[$key]['PicUrl']=request()->domain().'/'.get_cover($new->picurl_big, 'path');
                 }else{
                  $articles[$key]['PicUrl']=request()->domain().'/'.get_cover($new->picurl, 'path');
                 }

    		}

       return $articles;
      
    }

    public function editData(){

     $data=input("post.");
     if ( isset($data['file']) ) unset($data['file']);
      //处理链接
     if(strstr($data['url'],'http://')){
        $short= get_short_url(we_must_appid(),$data['url']);
        if(strstr($short,'http://w')){
          $data['url']=$short;
        }
     }
     
     $data['aid']=session('aid');
      if ($data['id']){
         $res = $this->allowField(true)->where('id',$data['id'])->update($data);
      }else{
         $res = $this->allowField(true)->data($data)->save();
      }
      
      return $res;
    }

    
}