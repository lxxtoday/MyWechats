<?php
// +----------------------------------------------------------------------
// | my
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://baiyf.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: $this <498944516@qq.com>
// +----------------------------------------------------------------------
namespace app\rob\model;

use think\Model;

class RobGameLog extends Model
{

   
    protected $insert = ['status'=>1];

    public function info($key, $field = true)
    {
        
        if (!$key) return false;
        if (is_numeric($key)){
            $map['id'] = $key;
        }else{
            $map['title'] = $key;
        }

        $fans=$this->field($field)->where($map)->find();

        return $fans;
    }

    public function check($game,$openid)
    {
        $map['game'] = $game;
        $map['openid'] = $openid;
       
        $log=$this->where($map)->whereTime('create_time','today')->find();
        if($log) return false;
        return true;
    }



  
  
}

