<?php
// +----------------------------------------------------------------------
// | my
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://baiyf.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: $this <498944516@qq.com>
// +----------------------------------------------------------------------
namespace app\rob\model;

use think\Model;

class RobGame extends Model
{

    
    protected $insert = ['status'=>1];

    public function info($id, $field = true)
    {
        
        if (!$id) return false;
        if (is_numeric($id)){
            $map['id'] = $id;
        }else{
            $map['title'] = $id;
        }

        $fans=$this->field($field)->where($map)->find();

        return $fans;
    }



     public function editData(){

      $data=input("post.");
     
      $data['aid']=session('aid');
      if ($data['id']){
         $res = $this->allowField(true)->where('id',$data['id'])->update($data);
      }else{
         $res = $this->allowField(true)->data($data)->save();
      }
      
      return $res;
    }


     public function getGiftStr($id){

      $game=$this->find($id);
      $gift_str='';
      
      if ($game['gift1']){
        $card_title=db('WeCard')->where('card_id',$game['gift1'])->value('title');
        $gift_str.="</br>".$game['gift1_score']."-".$game['gift2_score']."分：".$card_title;
      } 
      if ($game['gift2']){
        $card_title=db('WeCard')->where('card_id',$game['gift2'])->value('title');
        $gift_str.="</br>".$game['gift2_score']."-".$game['gift3_score']."分：".$card_title;
      } 
      if ($game['gift3']){
        $card_title=db('WeCard')->where('card_id',$game['gift3'])->value('title');
        $gift_str.="</br>".$game['gift3_score']."-".$game['gift4_score']."分：".$card_title;
      } 
      if ($game['gift4']){
        $card_title=db('WeCard')->where('card_id',$game['gift4'])->value('title');
        $gift_str.="</br>".$game['gift4_score']."-".$game['gift5_score']."分：".$card_title;
      } 
      if ($game['gift5']){
        $card_title=db('WeCard')->where('card_id',$game['gift5'])->value('title');
        $gift_str.="</br>>".$game['gift5_score']."分：".$card_title;
      }  
     
     
      return $gift_str;
    }


}

