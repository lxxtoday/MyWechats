<?php
// +----------------------------------------------------------------------
// | my
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://baiyf.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: NickBai <1902822973@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\admin\model\Node;
use think\Controller;

class Base extends Controller
{
    public function _initialize()
    {
        if(empty(session('user_auth'))){

            $this->redirect(url('login/index'));
        }


        //检测权限
        $control = request()->controller();
        $action = request()->action();

        //跳过登录系列的检测以及主页权限
        if(!in_array($control, ['login', 'index']) and !is_admin() ){

            if(!in_array($control . '/' . $action, session('action'))){
               $this->error('没有权限',url('login/index'));
            }
        }

        //获取权限菜单
        $node = new Node();
        $this->assign([
            'username' => session('user_auth.username'),
            'menu' => $node->getMenu(is_admin()?'':session('rule')),
            'rolename' => session('role')
        ]);

    }
}