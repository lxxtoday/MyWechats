<?php
// +----------------------------------------------------------------------
// | my
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://baiyf.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: NickBai <1902822973@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;

class Index extends Base
{
    public function index()
    {
        return $this->fetch('/index');
    }

    /**
     * 后台默认首页
     * @return mixed
     */
    public function indexPage()
    {
        return $this->fetch('index');
    }

    public function xls(){//导出Excel
       
        $xls=session('XLS');
        session('XLS','');
        //处理表头
       
       foreach ($xls['cell'] as $k => $v) {
          if (!in_array($v[0],array_keys($xls['data'][0]))) unset($xls['cell'][$k]);
       }
      
       
        foreach ($xls['data'] as $k => $v)
        {
            $xls['data'][$k]['create_time']=time_format($v['create_time']);
        }
        exportExcel($xls['name'],array_values($xls['cell']),$xls['data']);
         
    }
}
