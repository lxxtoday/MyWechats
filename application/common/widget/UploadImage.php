<?php
namespace app\common\widget;

use think\controller;

// 图片上传 - 单文件
class UploadImage extends Controller {
	public function render($attributes=array()) {
		$value = isset($attributes['value']) ? $attributes['value'] : "";
		if ( $value ) {
			$image = db('common_picture')->find($value);
			$this->assign('image', $image);
		}

		$isLoadScript = isset($attributes['isLoadScript']) ? 1 : 0;
		$this->assign('isLoadScript', $isLoadScript);

		$width = isset($attributes['width']) ? $attributes['width'] : 100;
		$height = isset($attributes['height']) ? $attributes['height'] : 100;

		$attributes['id'] = isset($attributes['id']) ? $attributes['id'] : $attributes['name'];
		$attributes['config'] = array('text' => lang('file select'));
		$this->assign($attributes);

		return $this->fetch('common@widget/uploadimage');
	}
} 