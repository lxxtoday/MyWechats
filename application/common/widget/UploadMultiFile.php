<?php
namespace app\common\widget;

use think\controller;

class UploadMultiFile extends Controller {
	public function render($attributes=array()) {
		if ( isset($attributes['value']) && $attributes['value']!="" ) {
			$value = $attributes['value'];
			$files_ids = explode(',', $value);
			if ( $files_ids ) {
				foreach ( $files_ids as $v ) {
					$files[] = db('common_file')->find($v);
				}
				unset($v);
				$this->assign('files', $files);
			}
		}

		$isLoadScript = isset($attributes['isLoadScript']) ? 1 : 0;
		$this->assign('isLoadScript', $isLoadScript);

		$attributes['id'] = isset($attributes['id']) ? $attributes['id'] : $attributes['name'];
		$attributes['config'] = array('text' => lang('file select'));
		$this->assign($attributes);

		return $this->fetch('common@widget/uploadmultifile');
	}
}