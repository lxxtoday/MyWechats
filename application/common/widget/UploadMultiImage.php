<?php
namespace app\common\widget;

use think\controller;

class UploadMultiImage extends Controller {
	public function render($attributes=array()) {
		if ( isset($attributes['value']) && $attributes['value']!="" ) {
			$value = $attributes['value'];
			$files_ids = explode(',', $value);
			if ( $files_ids ) {
				foreach ( $files_ids as $v ) {
					$pictures[] = db('common_picture')->find($v);
				}
				unset($v);
				$this->assign('pictures', $pictures);
			}
		}

		$isLoadScript = isset($attributes['isLoadScript']) ? 1 : 0;
		$this->assign('isLoadScript', $isLoadScript);

		$attributes['width'] = $width = isset($attributes['width']) ? $attributes['width'] : 100;
		$attributes['height'] = $height = isset($attributes['height']) ? $attributes['height'] : 100;

		$attributes['id'] = isset($attributes['id']) ? $attributes['id'] : $attributes['name'];
		$attributes['config'] = array('text' => lang('select images'));
		$this->assign($attributes);

		return $this->fetch('common@widget/uploadmultiimage');
	}
}