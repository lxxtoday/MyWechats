<?php
namespace app\common\widget;

use think\controller;

// 文件上传 - 单文件
class UploadFile extends Controller {
	public function render($attributes=array()) {
		$value = isset($attributes['value']) ? $attributes['value'] : "";
		if ( $value ) {
			$file = db('common_file')->find($value);
			$this->assign('file', $file);
		}

		$isLoadScript = isset($attributes['isLoadScript']) ? 1 : 0;
		$this->assign('isLoadScript', $isLoadScript);

		$attributes['id'] = isset($attributes['id']) ? $attributes['id'] : $attributes['name'];
		$attributes['config'] = array('text' => lang('file select'));
		$this->assign($attributes);

		return $this->fetch('common@widget/uploadfile');
	}
}