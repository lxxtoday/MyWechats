<?php
// +----------------------------------------------------------------------
// | my [负责builder的处理]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: $this <498944516.qq.com>
//  
// +----------------------------------------------------------------------


/**
 * 处理搜索相关
 * @return 
 * @author  $this <498944516@qq.com>
 * 《无题》
 *     ——致查查
 * 秋日红叶暖，
 * 黎黄楼顶，沐浴斜阳，望江滩。
 * 天道大秦，岂畏难？
 * 三年弹指，然一挥之间。
 *
 * 冬雪梧桐染，
 * 常青五院，共谱春秋，写长港。
 * 夫妻同心，斩荆棘！
 * 蓦然回首，当一马平川。
 * 
 */
function my_map($param=[]) {
   
        $map=$like = $in = array();
        if ( isset($param['like']) ) {
            $like = $param['like'];
            unset($param['like']);
        }
        if ( isset($param['in']) ) {
            $in = $param['in'];
            unset($param['in']);
        }

        if ( isset($param["create_time_start"]) ) {
            $map['create_time']=['between time',[$param["create_time_start"],$param["create_time_end"]]];
            unset($param["create_time_start"]);
            unset($param["create_time_end"]);
        }
        if(is_array($param)){
            foreach ( $param as $key => $value ) {
                if (!$value)continue;
                $key = str_replace(['[', ']'], ['', ''], $key);
                if ( in_array($key, $like) ) {
                    $map[$key] = ['like', '%'.$value.'%'];
                } else if ( in_array($key, $in) ) {
                    $map[$key] = ['in', $value];
                } else {
                    $map[$key] = $value;
                }
            }
        }
    
    return $map;
}

/**
 * 根据搜索条件获取结果数据
 * @return boolean true-管理员，false-非管理员
 * @author 麦当苗儿 <zuojiazi@vip.qq.com>
 */
function my_data($model,$where=[],$field=true,$order='id desc',$group='',$debug=1)
{
    $this_page=request()->controller().request()->action()."_map";
    $param=session($this_page);
   
    if (input('post.')){
      $param =input('post.');
      session($this_page,$param);
      $page=1;
      input('get.page',1);
    }else{
      $page=input('get.page');
    }
  
    //处理掉关键字段
    $bases=['limit','group','field','order','sum','count'];
    foreach ($bases as $key => $base) {
      if(isset($param[$base])) {
       $$base=$param[$base];
       unset($param[$base]); 
      }
    }
    
    if (!isset($page))$page=1;
    if (!isset($limit))$limit=20;
 

    $map=my_map($param);
  
    if (!$where){
        if (!is_admin(session('id'))) $where['aid']=session('aid');
        $where['status']=['EGT',0];
    }
    
    $map=array_merge ($where,$map);
   
    $table=db($model);
    if (isset($map['group']) and !$group){
       switch ($map['group']) {
            case 'month':
                $field='FROM_UNIXTIME(create_time,"%Y%m") as month, status, '.$map['field'];
                $group='month';
                $order=$map['order'];
                break;
             case 'week':
                $field= 'FROM_UNIXTIME(id,"%Y%u")  as week,status, '.$map['field'];
                $group='week';
                $order=$map['order'];
                break;
             case 'day':
                $field= 'FROM_UNIXTIME(date,"%Y%m%d") as  day,status, '.$map['field'];
                $group='day';
                $order=$map['order'];
               break;
            
            default:
                $field=$map['group'].', status, '.$map['field'];
                $group=$map['group'];
                $order=$map['order'];
                break;
        }
         
    }
   
    if ($group){
     $return['rows'] =$table->field($field)->where($map)->page($page,$limit)->order($order)->group($group)->select();
     if($debug==1) mymysql($table);
     $rows_total=count($return['rows']);

     if (isset($map['sum'])){
        $sums=explode(',',$map['sum']);
        $$sums[0]=0;
        $$sums[1]=0;
        $$sums[2]=0;
        foreach ($return['rows'] as $key => &$value) {
         $$sums[0]+=$value[$sums[0]];
         $$sums[1]+=$value[$sums[1]];
         $$sums[2]+=$value[$sums[2]];
        }
        $return['rows'][$rows_total][$sums[0]]=$$sums[0];
        $return['rows'][$rows_total][$sums[1]]=$$sums[1];
        $return['rows'][$rows_total][$sums[2]]=$$sums[2];
        $return['rows'][$rows_total]['status']='22';
        
     }
     
     $group_arr = $table->where($map)->group($group)->select();
     $return['total']=count($group_arr);
   
    }else{
 
      $return['rows'] =$table->field($field)->where($map)->page($page,$limit)->order($order)->select();

      if($debug==1) mymysql($table);
      $list = $table->where($map)->page($page,$limit)->paginate(20);
      $return['page']=$list->render();
    }
   
    return $return;
}


function my_select($db,$key='id',$value='title',$map=[]){
        if (!$map){
        $map['aid']=session('aid');
        }
        $map['status']=['EGT',0];

        $list=db($db)->field($key.','.$value)->where($map)->select();
        $list =array_column($list, $value, $key);
        return $list;
    }

function my_edit($model,$id,$default=[])
{
    if ( $id ) {
        $data = model($model)->find($id);
    } else {
        $data = $default;
    }
    return $data;
}

function my_domain(){
  // return $_SERVER['SERVER_NAME'];
  return request()->domain();
}

// 纠错级别：L、M、Q、H
// 点的大小：1到10,用于手机端4就可以了
// 下面注释了把二维码图片保存到本地的代码,如果要保存图片,用$fileName替换第二个参数false
 // 生成的文件名
function my_qrcode($data,$level = 'L', $size = 4,$path ='default'){
    vendor("phpqrcode.phpqrcode");
    
    if ($path){
        $path="uploads/qrcode/".$path;
        createFolder($path); 
        $fileName = $path.'/'.$data.'.png'; 
        \QRcode::png($data, $fileName, $level, $size);
        return $fileName;
    }else{
        $fileName =false;
        return \QRcode::png($data, $fileName, $level, $size);
    }
    
    
}

//处理表情
 function ubbReplace($str){
        $str = str_replace("<",'&lt;',$str);
        $str = str_replace(">",'&gt;',$str);
        $str = str_replace("\n",'<br/>',$str);
        $str = preg_replace("[\[/表情([0-9]*)\]]","<img src=\"/static/admin/js/plugins/qqface/face/$1.gif\" />",$str);
        return $str;
}








