<?php
// +----------------------------------------------------------------------
// | my [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: $this <498944516.qq.com> 
// +----------------------------------------------------------------------


/**
 * 消息集成
 * @param  to 发送给谁
 * @param  reply 消息体
 * @param  way 发送渠道,如果没有，则使用优先级
 * @return string       
 * @author $this 498944516@qq.com
 */
 // 发送消息
    function send($to,$reply,$way='')
    {
      
      switch ($way) {
          case 'templateMessage':
              $res=template_message($to,$reply);
              break;
          case 'customMessage':
              $res=custom_message($to,$reply);
              break;
          case 'sendGroupMassMessage':
              $res=send_group_mass_message($to,$reply);
              break;
         
          case 'email':
              break;

          case 'sns':
              break;

          default:
             if (isset($reply['templateMessage'])){
               $templateMessage_res=template_message($to,$reply['templateMessage']);
                 if($templateMessage_res!=1){  //模板消息发送失败
                      if (isset($reply['customMessage'])){
                         $customMessage_res=template_message($to,$reply['customMessage']);
                         if($customMessage_res!=1){ //客服消息发送失败

                         }else{
                          $res="customMessage";
                         }
                       }
                 }else{
                  $res="templateMessage";
                 }
             }
              
          break;
      }
       
        
    }
