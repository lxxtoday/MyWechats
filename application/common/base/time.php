<?php
/**
 * Created by PhpStorm.
 * User: caipeichao
 * Date: 14-3-14
 * Time: AM9:28
 */

/**
 * 友好的时间显示
 *
 * @param int    $sTime 待显示的时间
 * @param string $type  类型. normal | mohu | full | ymd | other
 * @param string $alt   已失效
 * @return string
 */
function friendlyDate($sTime,$type = 'normal',$alt = 'false') {
    if (!$sTime)
        return '';
    //sTime=源时间，cTime=当前时间，dTime=时间差
    $cTime      =   time();
    $dTime      =   $cTime - $sTime;
    $dDay       =   intval(date("z",$cTime)) - intval(date("z",$sTime));
    //$dDay     =   intval($dTime/3600/24);
    $dYear      =   intval(date("Y",$cTime)) - intval(date("Y",$sTime));
    //normal：n秒前，n分钟前，n小时前，日期
    if($type=='normal'){
        if( $dTime < 60 ){
            if($dTime < 10){
                return "刚刚";    //by yangjs
            }else{
                return intval(floor($dTime / 10) * 10).'秒前';
            }
        }elseif( $dTime < 3600 ){
            return intval($dTime/60)."分钟前";
            //今天的数据.年份相同.日期相同.
        }elseif( $dYear==0 && $dDay == 0  ){
            //return intval($dTime/3600).L('_HOURS_AGO_');
            return '今天'.date('H:i',$sTime);
        }elseif($dYear==0){
            return date("m月d日 H:i",$sTime);
        }else{
            return date("Y-m-d H:i",$sTime);
        }
    }elseif($type=='mohu'){
        if( $dTime < 60 ){
            return $dTime."秒前";
        }elseif( $dTime < 3600 ){
            return intval($dTime/60)."分钟前";
        }elseif( $dTime >= 3600 && $dDay == 0  ){
            return intval($dTime/3600)."小时前";
        }elseif( $dDay > 0 && $dDay<=7 ){
            return intval($dDay)."天前";
        }elseif( $dDay > 7 &&  $dDay <= 30 ){
            return intval($dDay/7) . "星期前";
        }elseif( $dDay > 30 ){
            return intval($dDay/30) . "月前";
        }
        //full: Y-m-d , H:i:s
    }elseif($type=='full'){
        return date("Y-m-d , H:i:s",$sTime);
    }elseif($type=='ymd'){
        return date("Y-m-d",$sTime);
    }else{
        if( $dTime < 60 ){
            return $dTime."秒前";
        }elseif( $dTime < 3600 ){
            return intval($dTime/60)."分前";
        }elseif( $dTime >= 3600 && $dDay == 0  ){
            return intval($dTime/3600)."小时前";
        }elseif($dYear==0){
            return date("Y-m-d H:i:s",$sTime);
        }else{
            return date("Y-m-d H:i:s",$sTime);
        }
    }
}

function dateformat($date){
    if(!empty($date))
        return date('Y-m-d',$date);
    else
        return '';
}

function dateTimeFormat($date){
    if(!empty($date))
        return date('Y-m-d H:i',$date);
    else
        return '';
}

function dateTformat($date){
   return strtotime(str_replace("T", " ", $date));
}

/* 
*function：计算两个日期相隔多少年，多少月，多少天 
*param string $date1[格式如：2011-11-5] 
*param string $date2[格式如：2012-12-01] 
*return array array('年','月','日'); 
*/  
function diffDate($date1,$date2){  

    $back['bool']=1;
    if(strtotime($date1)>strtotime($date2)){  
        $tmp=$date2;  
        $date2=$date1;  
        $date1=$tmp;
        $back['bool']=0;
    }  
    list($Y1,$m1,$d1)=explode('-',$date1);  
    list($Y2,$m2,$d2)=explode('-',$date2);  
    $Y=$Y2-$Y1;  
    $m=$m2-$m1;  
    $d=$d2-$d1;  
    if($d<0){  
        $d+=(int)date('t',strtotime("-1 month $date2"));  
        $m--;  
    }  
    if($m<0){  
        $m+=12;  
        $y--;  
    }

    $back['year']=$Y;
    $back['month']=$m;
    $back['day']=$d;
    
   
    return $back;  
}  


    /*
        节日信息
    */
 function holiday( $date ){
        
        
        
        $res = array();
        
    
        
        //农历节日
        $holiday = array(
            '01-01'=>'春节',
            '01-15'=>'元宵节',
            '02-02'=>'二月二',
            '05-05'=>'端午节',
            '07-07'=>'七夕节',
            '08-15'=>'中秋节',
            '09-09'=>'重阳节',
            '12-08'=>'腊八节',
            '12-23'=>'小年'
        );
        //公历转农历，并截取月份的日期
        $yangli = date('Y-m-d',$date );
        $nongli=D('Common/Lunar')->getLar($yangli,2);
        $days = date('m-d',$nongli ) ;
  
        
        if( isset( $holiday[ $days ] ) ){
            $res[] = $holiday[ $days ];
        }
        
        ////////////////////////////////////////
        
        $days = date('m-d',$date ) ;
    
        //公历节日
        $holiday = array(
            '01-01'=>'元旦',
            '02-02'=>'世界湿地日(1996)',
            '02-14'=>'情人节',
            '03-03'=>'全国爱耳日',
            '03-08'=>'妇女节(1910)',
            '03-12'=>'植树节(1979)',
            '03-15'=>'国际消费者权益日',
            '03-20'=>'世界睡眠日',
            '03-25'=>'世界气象日',
            '04-01'=>'愚人节',
            '04-07'=>'世界卫生日',
            '05-01'=>'国际劳动节',
            '05-04'=>'中国青年节',
            '05-08'=>'世界红十字日',
            '05-12'=>'国际护士节',
            '05-19'=>'全国助残日',
            '06-01'=>'国际儿童节',
            '06-05'=>'世界环境日',
            '06-22'=>'中国儿童慈善活动日',
            '06-23'=>'国际奥林匹克日',
            '07-01'=>'中国共产党成立(1921)',
            '07-07'=>'中国人民抗日战争纪念日',
            '08-01'=>'中国人民解放军建军(1927)',
            '09-03'=>'抗日战争胜利纪念日(1945)',
            '09-08'=>'国际扫盲日',
            '09-10'=>'教师节',
            '09-16'=>'世界臭氧层保护日',
            '09-18'=>'九一八纪念日',
            '09-27'=>'世界旅游日',
            '09-29'=>'国际聋人节',
            '10-01'=>'国庆节',
            '10-14'=>'世界标准日',
            '10-24'=>'联合国日',
            '12-05'=>'国际志愿人员日',
            '12-29'=>'12.9运动纪念日',
            '12-25'=>'圣诞节'
        );
        
        if( isset( $holiday[ $days ] ) ){
            $res[] = $holiday[ $days ];
        }
        
        return implode( '，', $res );
    
    }



 function birthdayReminder($birthday,$reminder ){

     $preg = '/^(\d{4}|\d{2}|)[- ]?(\d{2})[- ]?(\d{2})$/';
     $Ymd = array();
     
     preg_match($preg, $birthday, $Ymd);
     if (empty($Ymd) ||empty($birthday)) return false;

     
     $birthday = $Ymd[2].'-'.$Ymd[3];
     $time = time();

     for ($i = 1; $i <=  $reminder; $i++){
         
      if (date('m-d', $time) == $birthday) {
         if ($i==1) {
            return "(今天生日)";
          }else {
             return '('.$i."天后生日)";
          }
      }
      $time = $time + 24 * 3600;
     }
     return false;

}

/**
 * 时间戳格式化
 * @param int $time
 * @return string 完整的时间显示
 * @author huajie <banhuajie@163.com>
 */
function time_format($time = NULL, $format = 'Y-m-d H:i')
{
   
    if ($time != 0 and $format == 'Y-m-dTH:i'){
    
      $time = $time === NULL ? '' : intval($time);
      return date('Y-m-d', $time).'T'.date('H:i', $time);
    }

    if ($time != 0) {
    $time = $time === NULL ? '' : intval($time);

    return date($format, $time);
    }

}



