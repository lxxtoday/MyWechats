<?php 
//  - 根据 图片标识 获取 数据库图片信息
function get_cover($cover_id, $field=null) {
    if ( empty($cover_id) ) return false;

    $picture = cache('picture_' . $cover_id);
    if ( empty($picture) ) {
        $picture = db('common_picture')->where('id', $cover_id)->find();
        cache('picture_' . $cover_id, $picture);
    }

    $picture['path'] ='/'. config('upload_path') . get_pic_src($picture['path']);

    return empty($field) ? $picture : $picture[$field];
}
// - 别名 get_cover
function pic($cover_id) {
    return get_cover($cover_id, 'path');
}


// - 根据 图片标识 获取 图片缩略图
function get_thumb_byid($cover_id, $width=100, $height='auto', $type=0, $replace=false) {
    $picture = cache('picture_' . $cover_id);
    if ( empty($picture) ) {
        $picture = db('common_picture')->where(array('status' => 1))->getById($cover_id);
        cache('picture_' . $cover_id, $picture);
    }

    if ( empty($picture) ) return get_pic_src('static/images/nopic.png'); // 无图

    if ( $picture['type'] == 'local' ) { // 本地
        $attach = get_thumb($picture['path'], $width, $height, $type, $replace); // 获取缩略图
        return get_pic_src($attach['src']);
    } else { // 非本地 ？？？？？？？？？？？？？？？
        $new_img = $picture['path'];
        $name = get_addon_class($picture['type']);
        if ( class_exists($name) ) {
            $class = new $name();
            if ( method_exists($class, 'thumb') ) $new_img = $class->thumb($picture['path'], $width, $height, $type, $replace);
        }
        return get_pic_src($new_img);
    }
}
// - 别名 get_thumb_byid
function thumb($cover_id, $width=100, $height='auto', $type=0, $replace=false) {
    return get_thumb_byid($cover_id, $width, $height, $type, $replace);
}

function get_thumb($filename, $width=100, $height='auto', $type=0, $replace=false) {
    $upload_url = ''; // 图片上传 网络路径
    $upload_path = ''; // 图片上传 系统路径

    $filename = str_ireplace($upload_url, '', $filename);
    $info = pathinfo(config('upload_path'). $filename);

    $originFile = $info['dirname'] . DIRECTORY_SEPARATOR . $info['filename'] . '.' . $info['extension']; // 源文件
    $thumbFile = $info['dirname'] . DIRECTORY_SEPARATOR . $info['filename'] . '_' . $width . '_' . $height . '.' . $info['extension']; // 缩略图文件

    $originFile = str_replace('\\', '/', $originFile);
    $thumbFile = str_replace('\\', '/', $thumbFile);

    $filename = ltrim($filename, '/');
    $originFile = ltrim($originFile, '/');
    $thumbFile = ltrim($thumbFile, '/');

    if ( !file_exists($upload_path . $originFile) ) { // 原图不存在
        @unlink($upload_path . $thumbFile); // 删除缩略图文件

        $info['src'] = $originFile;
        $info['width'] = intval($width);
        $info['height'] = intval($height);
        return $info;
    } elseif ( file_exists($upload_path . $thumbFile) && !$replace ) { // 缩图已存在 并且 不替换
        $imageinfo = getimagesize($upload_path . $thumbFile);

        $info['src'] = $thumbFile;
        $info['width'] = intval($imageinfo[0]);
        $info['height'] = intval($imageinfo[1]);
        return $info;
    } else { // 执行缩图操作
        $originimageinfo = getimagesize($upload_path . $originFile);
        $origin_image_width = intval($originimageinfo[0]);
        $origin_image_height = intval($originimageinfo[1]);

        if ( $origin_image_width <= $width && $origin_image_height <= $height ) { // 原图片很小
            @unlink($upload_path . $thumbFile);
            @copy($upload_path . $originFile, $upload_path . $thumbFile);

            $info['src'] = $thumbFile;
            $info['width'] = $origin_image_width;
            $info['height'] = $origin_image_height;
            return $info;
        } else { // 原图片很大
            if ( $height == "auto" ) $height = $origin_image_height * $width / $origin_image_width;
            if ( $width == "auto" ) $width = $origin_image_width * $width / $origin_image_height;
            if ( intval($height) == 0 || intval($width) == 0 ) return 0;

            $image = \think\Image::open($upload_path . config('upload_path'). $filename);
            $image->thumb($width, $height, \think\Image::THUMB_FILLED)->save($upload_path . $thumbFile);

            $info['src'] = $upload_path . $thumbFile;
            $info['width'] = $origin_image_width;
            $info['height'] = $origin_image_height;
            return $info;
        }
    }
}

// 获取第一张图
function get_pic($str_img) {
    preg_match_all("/<img.*\>/isU", $str_img, $ereg); //正则表达式把图片的整个都获取出来了
    $img = $ereg[0][0]; //图片
    $p = "#src=('|\")(.*)('|\")#isU"; //正则表达式
    preg_match_all($p, $img, $img1);
    $img_path = $img1[2][0]; //获取第一张图片路径
    return $img_path;
}

// 获取网站的根Url
function get_root_url() {
    if ( __ROOT__ != '' ) return __ROOT__ . '/';
    if ( config('URL_MODEL') == 2 ) return __ROOT__ . '/';
    return __ROOT__;
}

// - 根据 数据库图片路径 获取 图片链接
function get_pic_src($path) {
    $not_http_remote = (strpos($path, 'http://') === false); // 不存在http://
    $not_https_remote = (strpos($path, 'https://') === false); // 不存在https://
    if ( $not_http_remote && $not_https_remote ) { // 本地url
        return str_replace('//', '/',  $path); // 防止双斜杠的出现
    } else { // 远端url
        return $path;
    }
}

// 下载网络图片get_file_from_net("http://www.baidu.com/img/baidu_logo.gif","file","baidu.jpg");
function get_file_from_net($url,$folder,$pic_name){  
        set_time_limit(24*60*60); //限制最大的执行时间
        $destination_folder=$folder?$folder.'/':''; //文件下载保存目录
        $newfname=$destination_folder.$pic_name;//文件PATH
        $file=fopen($url,'rb');
         
        if($file){          
            $newf=fopen($newfname,'wb');
            if($newf){              
                while(!feof($file)){                    
                    fwrite($newf,fread($file,1024*8),1024*8);
                }
            }
            if($file){              
                fclose($file);
            }
            if($newf){              
                fclose($newf);
            }
        }       
    }
?>