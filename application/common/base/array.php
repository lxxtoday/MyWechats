<?php
// 数组排序
function multi_array_sort($multi_array, $sort_key, $sort = SORT_ASC){
        if (is_array($multi_array)) {
            foreach ($multi_array as $row_array) {
                if (is_array($row_array)) {
                    $key_array[] = $row_array[$sort_key];
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
        array_multisort($key_array, $sort, $multi_array);
        return $multi_array;
}

  /* 返回一个字符的数组 */
function chararray($str,$charset="utf-8"){
    $re['utf-8']   = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|[\xe0-\xef][\x80-\xbf]{2}|[\xf0-\xff][\x80-\xbf]{3}/";
    $re['gb2312'] = "/[\x01-\x7f]|[\xb0-\xf7][\xa0-\xfe]/";
    $re['gbk']    = "/[\x01-\x7f]|[\x81-\xfe][\x40-\xfe]/";
    $re['big5']   = "/[\x01-\x7f]|[\x81-\xfe]([\x40-\x7e]|\xa1-\xfe])/";
    preg_match_all($re[$charset], $str, $match);
    return $match;
}
// 自动换行 
function autoHang($str,$um=15,$ge="\n"){
    $str_arr_one=chararray($str);
    $str_arr=$str_arr_one[0];
    $long=count($str_arr);
    $text='';
    if ($long<$um) {
        return ['total'=>1,'text'=>$str];
    }else{
        $str_arr_all=array_chunk($str_arr,$um);
        
        foreach ($str_arr_all as $key => $value) {
            echo $text.=implode('',$value).$ge;
           
        }
    }
    return ['total'=>count($str_arr_all),'text'=>$text];
}
//数组转一维数组
function arrToOne ($arr) {
      static $tmp=array(); 

      if (!is_array ($arr)) 
      {
         return false;
      }
      foreach ($arr as $key=>$val ) 
      {
         if (is_array ($val)) 
         {
            $this->arrToOne ($val);
         } 
         else 
         {
            $tmp[$key]=$val;
         }
      }
      return $tmp;

   }

function object_array($array)
{
   if(is_object($array))
   {
    $array = (array)$array;
   }
   if(is_array($array))
   {
    foreach($array as $key=>$value)
    {
     $array[$key] = object_array($value);
    }
   }
   return $array;
}
  

?>
