<?php

// ---
// 钩子
function hook($hook, $params=array()) {
    \think\Hook::listen($hook, $params);
}
function modconfig($key, $default='', $module='') { // 模块配置
    $mod = $module ? $module : $request->module();
    $tag = 'conf_' . strtoupper($mod).'_'.strtoupper($key);
    $result = cache($tag);

    if ( $result === false ) {
        $config = db('config')->field('value')->where(array('name' => '_'.strtoupper($mod).'_'.strtoupper($key)))->find();
        if ( !$config ) {
            $result = $default;
        } else {
            $result = $config['value'];
        }
        cache($tag, $result);
    }

    return $result;
}
function get_upload_config($driver) { // 获取 上传配置
    if ( $driver == 'local' ) { // 完成
        $uploadConfig = config("upload_{$driver}");
    } else {
        $name = get_addon_class($driver);
        $class = new $name();
        $uploadConfig = $class->uploadConfig();
    }

    return $uploadConfig;
}
function get_addon_class($name) { // 获取 插件类
    return "addons\\{$name}\\{$name}";
}