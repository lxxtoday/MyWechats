<?php
/**
* 验证手机号是否正确
* @author 范鸿飞
* @param INT $mobile
*/
 function is_mobile($mobile) {
    if (!is_numeric($mobile)) {
        return false;
    }
    return preg_match('#^13[\d]{9}$|^14[5,7]{1}\d{8}$|^15[^4]{1}\d{8}$|^17[0,6,7,8]{1}\d{8}$|^18[\d]{9}$#', $mobile) ? true : false;
 }

 function weather($city='wuhan',$apikey='74480f1841aeb07248f19727bf14b702',$get='today') {
    $ch = curl_init();
    $url = 'http://apis.baidu.com/heweather/weather/free?city='.$city;
    $header = array(
        'apikey: '.$apikey,
    );
    // 添加apikey到header
    curl_setopt($ch, CURLOPT_HTTPHEADER  , $header);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    // 执行HTTP请求
    curl_setopt($ch , CURLOPT_URL , $url);
    $res = curl_exec($ch);
    $weather=object_array(json_decode($res));
    $today=$weather['HeWeather data service 3.0'][0]['daily_forecast'][0];

    return $$get;
}
 ?>