<?php
// +----------------------------------------------------------------------
// | my [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: $this <498944516.qq.com> 
// +----------------------------------------------------------------------

//调试方法
function mylog($log,$desc="",$file=""){
   log_file($log,$desc,$file);
}

function mymysql($module){
   mylog($module->getLastSql());
}

// 页面输出 - behero
function log_page($data) {
	if ( is_array($data) || is_object($data) ) {
		echo "<pre>";
		print_r($data);
		echo "</pre>";
	} else {
		echo $data;
	}
}


// 文件输出 - behero
function log_file($log, $desc="", $file="") {

	$path=ROOT_PATH . "runtime/mylog/".date('Ym',time());
    createFolder($path); 
	if ( empty($file) ) $file = date("Ymd") . ".log";
	$file = $path.'/'. $file;
    if ($desc){
    	 file_put_contents($file,$desc."\n",FILE_APPEND); 
    }
	if (is_array($log) || is_object($log)) $log = print_r($log,true);
    file_put_contents($file,$log."\n",FILE_APPEND); 
}

?>
