<?php
// +----------------------------------------------------------------------
// | my
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://baiyf.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: NickBai <1902822973@qq.com>
// +----------------------------------------------------------------------
namespace app\we\model;

use think\Model;

class WeFansCfoLog extends Model
{

    protected $insert = ['status'=>1];  
    /**
     * 获取微信配置信息，需要缓存
     */
    public function info($openid,$field=true){
      
      $map['openid']=$openid;
      $cfo = $this->field($field)->where($map)->find();
     
      return  $cfo;
    }

     public function infoForFans($openid,$field=true){
      $fans=$this->info($openid,$field=true);
      if (!$fans) return false;
      $printer=model('printer/PrinterFans')->info($fans['openid'],'balance');

      $message="粉丝信息";
      $message.="\nID：".$fans['id'];
      $message.="\n昵称：".$fans['nickname'];
      if($printer) $message.="\n打印余额：".$printer['balance'];
      return  ['fans'=>$fans,'message'=>$message];
    }

    public function infoForFansByOrder($data){
     $fans = $this->infoForFans($data['before']);
     if (!$fans) return ['message'=>'粉丝不存在，请核对粉丝ID'];
     return ['message'=>$fans['message']]; //空尾巴查询
    }

     public function getAdmins($appid){
      $map['appid']=$appid;
      $map['userid']=['NEQ',''];
     
      $admins=$this->where($map)->select();

      
      return  $admins;
    }




    public function editData($data){

      
      if (isset($data['id'])){
         $res = $this->allowField(true)->where('id',$data['id'])->update($data);
      }else{

         $res = $this->allowField(true)->data($data)->save();
      }
    
      return $res;
    }

    //是否使用过这个二维码
  public function haveAdd($openid='')
  {
      $have=$this->where('openid',$openid)->where('um>0')->whereTime('create_time','today')->limit(1)->find();
      if ($have) return true;
      return false;
  }
    
}