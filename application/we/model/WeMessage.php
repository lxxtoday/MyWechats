<?php
// +----------------------------------------------------------------------
// | my
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://isofttime.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: $this <498944516@qq.com>
// +----------------------------------------------------------------------
namespace app\we\model;

use think\Model;



class WeMessage extends Model
{
   
    // 根据openid 获取用户
    public function info($key, $field = true)
    {
        
        if (!$key) return false;
        $map['MsgId'] = $key;
       

        return $this->field($field)->where($map)->find();
    }

   

    // 保存消息记录
    public function saveMessage($data)
    {
       
        try{
            $result =  $this->allowField(true)->save($data);

        }catch( PDOException $e){
            $res=  ['code' => -2, 'data' => '', 'msg' => $e->getMessage()];
        }
        $res=  ['code' => 1, 'data' => '', 'msg' => '添加消息成功'];
        return $res;
    }

    

 

    
   
}