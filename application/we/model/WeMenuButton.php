<?php
// +----------------------------------------------------------------------
// | my
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://isofttime.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: $this <498944516@qq.com>
// +----------------------------------------------------------------------
namespace app\we\model;

use think\Model;



class WeMenuButton extends Model
{
    protected $insert = ['status'=>1]; 
    protected function initialize()
    {
        $this->buttonType=[
        'rselfmenu_0_0'=>'scancode_waitmsg',
        'rselfmenu_0_1'=>'scancode_push',
        'rselfmenu_1_0'=>'pic_sysphoto',
        'rselfmenu_1_1'=>'pic_photo_or_album',
        'rselfmenu_1_2'=>'pic_weixin',
        'rselfmenu_2_0'=>'location_select',
        ];
    }

    // 根据openid 获取用户
    public function info($key, $field = true)
    {
        
        if (!$key) return false;
        if (is_numeric($key)){
            $map['id'] = $key;
        }else{
            $map['openid'] = $key;
        }

        $fans=$this->field($field)->where($map)->find();

        return $fans;
    }

   

     public function editData(){

      $data=input("post.");
      
      if(isset($data['file'])) unset($data['file']);
      $data['aid']=session('aid');
      if ($data['id']){
         $res = $this->allowField(true)->where('id',$data['id'])->update($data);
      }else{
         $res = $this->allowField(true)->data($data)->save();
      }
      
      return $res;
    }

     /**
     * 获得分类树
     * @param int $id
     * @param bool $field
     * @return array
     * @author 郑钟良<zzl@ourstu.com>
     */
    public function getTree($id = 0, $field = true,$map=array(),$url='Wechat/Menu',$pid=null,$appid=0){
        
        $map['status'] = array('gt', -1);
        $map['aid']=session('aid');
      
        /* 获取所有分类 */
        $list = $this->field($field)->where($map)->order('sort desc')->select();

       
        foreach ($list as $key => $value) {
           
            $list[$key]['href']=  U($url,array('id'=>$appid,'pid'=>$value['id'])) ;
            if ($value['id']==$pid) $list[$key]['state']['selected']=true;

        }
        $list = list_to_tree($list, $pk = 'id', $pid = 'pid', $child = 'nodes', $root = $id);

       

        /* 获取返回数据 */
        if(isset($info)){ //指定分类则返回当前分类极其子分类
            $info['nodes'] = $list;
        } else { //否则返回所有分类
            $info = $list;
        }
      
        return $info;
    }

   




   
}