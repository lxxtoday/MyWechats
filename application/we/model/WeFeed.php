<?php
// +----------------------------------------------------------------------
// | my
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://baiyf.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: NickBai <1902822973@qq.com>
// +----------------------------------------------------------------------
namespace app\we\model;

use think\Model;

class WeFeed extends Model
{

    protected $insert = ['status'=>1];  
    
    public function info($id,$field=true){
      
     
      $map['id']=$id;
      
      $feed = $this->field($field)->where($map)->find();

      $shop=model('admin/AdminShop')->info($feed['shopid']);
      $feed['shop']=$shop['title'];
      return  $feed;
    }


    public function editData($data=''){

      if (!$data)$data=input('post.');

      if (isset($data['id'])){
         $res = $this->allowField(true)->where('id',$data['id'])->update($data);
      }else{
          $this->allowField(true)->data($data)->save();
          $res =$this->id;
      }
    
      return $res;
    }

   

     public function getFeeds($data){
       if (!is_numeric($data['before'])){
           $shop=db('AdminShop')->where('title',$data['before'])->find();
           if($shop)$map['shopid']=$shop['id'];
           
           if ($data['back']=='好评')$map['product|service']=1;
           if ($data['back']=='中评')$map['product|service']=2;
           if ($data['back']=='差评')$map['product|service']=3;
           if ($data['back']=='产品好评')$map['product']=1;
           if ($data['back']=='产品中评')$map['product']=2;
           if ($data['back']=='产品差评')$map['product']=3;
           if ($data['back']=='服务好评')$map['service']=1;
           if ($data['back']=='服务中评')$map['service']=2;
           if ($data['back']=='服务差评')$map['service']=3;
           if ($data['back']=='未处理'){
            $map['admin']='';
            $map['product|service']=3;
           }
          
           $feeds=$this->where($map)->whereTime('create_time', 'today')->limit(10)->select();
          
           $today_total=$this->where($map)->whereTime('create_time', 'today')->count();
           $yesterday_total=$this->where($map)->whereTime('create_time', 'yesterday')->count();
           $week_total=$this->where($map)->whereTime('create_time', 'week')->count();
           $last_week_total=$this->where($map)->whereTime('create_time', 'last week')->count();
           $message="今日".$today_total."；昨日".$yesterday_total;
           $message.="\n本周".$week_total."; 上周".$last_week_total;
           $message.="\n本次查询结果:";
           foreach ($feeds as $key => $feed) {
              $url=my_domain().url('we/feed/chatAdmin',['feed_id'=>$feed['id'],'admin_openid'=>$data['openid']]);
              $message.="\n".$feed['id'].'：'.$feed['nickname'].'-'.$feed['content'].'<a href="'.$url.'">回复</a>';
           }

      }else{
        
         if ($data['back']=='改成产品好评')$update['product']=1;
         if ($data['back']=='改成产品中评')$update['product']=2;
         if ($data['back']=='改成产品差评')$update['product']=3;
         if ($data['back']=='改成服务好评')$update['service']=1;
         if ($data['back']=='改成服务中评')$update['service']=2;
         if ($data['back']=='改成服务差评')$update['service']=3;
         if ($data['back']=='感动')$update['status']=4;
         $res=$this->where('id',$data['before'])->update($update);
         if ($res)$message=$data['back'].'成功！';
      }
      return ['message'=>$message];
       

     }


     public function paySuccess(){
       //标记打赏了多少钱
       $pay=session('pay');
       $res=db('we_feed')->where('id',$pay['data_id'])->update(['pay'=>$pay['price']]);
 
    }

 
    
}