<?php
// +----------------------------------------------------------------------
// | my
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://isofttime.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: $this <498944516@qq.com>
// +----------------------------------------------------------------------
namespace app\we\model;
use think\Model;
use think\helper\Time;

class WeSceneScan extends Model
{

  
  protected $insert = ['status'=>1];  

 
    // 处理带参数二维码入口
    // 可能是扫码事件，可能是带参二维码扫码
  public function scan($data)
  {
    
      if ($data['EventKey']) {
       
        $scene_id=str_replace("qrscene_","", $data['EventKey']);
        $qrcode=model('WeSceneQrcode')->info($data['appid'],$scene_id);
    
        if(!$qrcode) return false;
        $answer=$qrcode[$data['Event'].'_answer'];//获取机器人回答
        
        model('WeSceneQrcode')->where('id',$qrcode['id'])->setInc($data['Event']);  //统计扫描和关注

       //添加日志需要的字段
        $data['shopid']=$qrcode['shopid'];
        $data['qrcode_id']=$qrcode['id'];
        $data['scene']=$qrcode['scene'];
        $data['qrcode']=$qrcode['title'];
        $data['qrcode_type']=$qrcode['qrcode_type'];
        $data['admin']=$qrcode['admin'];
        $data['is_desk']=$qrcode['is_desk'];
 
        //如果是桌码就记录老顾客,昨天扫桌码
        if($qrcode['is_desk']==1){
          list($start,$end)=Time::today();
          $data['last_admin']= $this->where('is_desk=1 and admin<>""')->where('openid',$data['openid'])->where('create_time<'.$start)->value('admin');
        }
       
      }else{
        $scene_id=0;  //扫码母码
        $we=model('We')->info($data['appid'],$data['Event'].'_answer');
        $answer=$we[$data['Event'].'_answer'];
        $data['qrcode_id']=0;
        $data['qrcode']='微信二维码';
        $data['qrcode_type']=0;
        $data['scene_type']=0;
      }
 

      //记录到扫码记录
      $this->addScan($data);

      return [$answer,$data];
  }

  public function addScan($data)
  {
    
       // 如果是关注就把这个粉丝的关注场景和来源场景更新
       $data['do']=0;
       if($data['Event']=='subscribe'){
          $data['do']=1;
          $fans=model('we/WeFans')->info($data['openid']);
          if (!$fans['subscribe_qrcode']){
          $res=db('WeFans')->where('openid',$data['openid'])->update(['subscribe'=>1,'subscribe_qrcode'=>$data['qrcode_id'],'subscribe_time'=>time()]);
          }
       }
       $map['openid']=$data['openid'];
       $map['qrcode_id']=$data['qrcode_id'];
       $map['create_time']=['EGT',time()-3600]; //一个小时内不重复记录
       $have=$this->where($map)->order('create_time desc')->find();
       if(!$have)$this->allowField(true)->save($data);
      
  }

  //获取用户最后一次扫码
  //可以是
  public function getLastScan($openid,$map=[])
  {
      $map['openid']=$openid;
      $map['status']=['egt',0]; 
      $scan=$this->whereTime('create_time', 'today')->where($map)->order('create_time desc')->limit(1)->find();
      return $scan;
  }

 

   //获取用户最后一次扫码的管理员
   //如果是反馈就没有时间限制
   //如果是老顾客则是当餐以前
  public function getLastAdmin($openid,$map=[])
  {
      
      $map['openid']=$openid;
      $map['status']=['egt',0];
      $map['shopid']=['neq',''];
      $scan=$this->where($map)->order('create_time desc')->limit(1)->find();
      if($scan['admin']){
          $admin=model('admin/AdminUser')->info($scan['admin']);
          $scan['admin_name']=$admin['name'];
      }
      return $scan;
  }


  //获取用户最后一次扫码
  public function getScanDays($openid,$do=0)
  {
    
      $map['openid']=$openid;
      $map['qrcode_type']=1;
      $scans=$this->where($map)->order('create_time desc')->group('FROM_UNIXTIME( create_time,"%Y-%m-%d")')->select();
      if ($do==1)return $scans;
      return count($scans); 
      
  }

  //是否使用过这个二维码
  public function haveScan($openid,$qrcode_id)
  {
      
      $map['openid']=$openid;
      $map['qrcode_id']=$qrcode_id;
      
      $have=$this->where($map)->limit(1)->find();
      if ($have['create_time']<time()-600) return true;
      
      return false;
  }




 
    
   
}