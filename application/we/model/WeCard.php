<?php
// +----------------------------------------------------------------------
// | my
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://isofttime.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: $this <498944516@qq.com>
// +----------------------------------------------------------------------
namespace app\we\model;

use think\Model;



class WeCard extends Model
{


    // 根据openid 获取用户
    public function info($key, $field = true)
    {
        
        if (!$key) return false;
        $map['id|card_id'] = $key;
       
        return $this->field($field)->where($map)->find();
    }

    

    public function editData(){

      $data=input("post.");
      
      if ($data['id']){
         $res = $this->allowField(true)->where('id',$data['id'])->update($data);
      }else{
         $res = $this->allowField(true)->data($data)->save();
      }
      
      return $res;
    }

     public function updateCard($card){

      $map['card_id']=$card['card_id'];
      $map['aid']=session('aid');
      $card['aid']=session('aid');

      $have = $this->where($map)->find();
      
      if ($have){
         $res = $this->allowField(true)->where('card_id',$have['card_id'])->update($card);
      }else{
         $res = $this->allowField(true)->data($card)->save();
      }
      
      return $res;
    }
    
   
}