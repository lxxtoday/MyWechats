<?php
// +----------------------------------------------------------------------
// | my
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://baiyf.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: $this <498944516@qq.com>
// +----------------------------------------------------------------------
namespace app\we\model;

use think\Model;

class WeApp extends Model
{

   
    /**
     * 登录/
     */
    public function index($data=''){

      $this->data=$data;
      //验证权限
     
    	   
       
       switch ($this->data['keyword']) {
          
          case 'F':
            $reply= $this->F();
            break;
          case 'K':
            $reply= $this->K();
            break;
          case 'Q':
            $reply= $this->Q();
            break;
       
          default:
            # code...
            break;
        } 



       return $reply;
      
    }


    
   
    public function F() {
       if (!$this->data['before'] and !$this->data['back']) {
         $message='F -粉丝(fans)管理工具';
         $message.="\n-------------------";
         $message.="\n/:hug0.登录：\n17092610050FA123456 (登录系统)";
         $message.="\n/:hug1.查询：\n1F (查询1号粉丝信息)";
         $message.="\n/:hug2.充值：\n1FM1 (给1号粉丝充值1元)";
         $message.="\n/:hug3.积星：\n1FS1 (给1号粉丝集1星)";
         $message.="\n/:hug4.消息：\n1FC文字 (给1号粉丝发送一句话)";
         $message.="\n/:hug5.卡券：\n1FC卡 (获取所有可用卡券)";
         $message.="\n/:hug6.卡券：\n1FC卡2 (给1号粉丝发2号卡券)";
         $message.="\n/:hug7.消费：\n1F2+100 (1号粉丝正2号桌消费100元)";
         $message.="\n/:hug8.消费：\n1F2++100 (同上,强制积星)";
        
         return $message;
       }


       //查询自己的账户{粉丝OPENID}F
       if ($this->data['before'] and !$this->data['back']) {
          if (is_numeric($this->data['before'])){
              $admin=model('we/WeFans')->checkAdmin($this->data['openid']);
              if($admin['status']==0) return $admin['text']; 
          }
         
         return model('we/WeFans')->infoForFansByOrder($this->data);
       }
        
       
      
       if ($this->data['before'] and $this->data['back']) return model('we/WeFans')->F($this->data);
    }

     public function K() {
     
       $admin=model('we/WeFans')->checkAdmin($this->data['openid']);
       if($admin['status']==0) return $admin['text']; 
       if (!$this->data['before'] and !$this->data['back']) {
         $message='K -客情管理工具';
         $message.="\n-------------------";
         $message.="\n/:hug1.查询：\n武昌店K好评（不带店名为查询所有店，可以查询好评，中评，差评，产品好评，产品差评，服务好评，服务差评）";
         $message.="\n/:hug2.查询：\n武昌店K产品中评 ";
         $message.="\n/:hug3.查询：\nK服务差评 ";
         $message.="\n/:hug4.查询：\n武昌店K未回复 ";
         $message.="\n/:hug5.修改：\n1K改成产品差评";
         $message.="\n/:hug5.修改：\n1K感动（感动案例）";
          
         return $message;
       }

       $message=model('we/WeFeed')->getFeeds($this->data);
       return $message;

    }

    public function Q(){
    //获取用户头像，二维码合并成推广图片
    //发送图片
    //用户识别图片
    //响应机器人完成业务
    //TODO增加永久二维码
      if (!$this->data['before'] and !$this->data['back']) {
         $admin=model('we/WeFans')->checkAdmin($this->data['openid']);
         if($admin['status']==0) return $admin['text'];

         $message='Q -二维码管理工具';
         $message.="\n-------------------";
         $message.="\n/:hug1.积星码：\n积星Q60 (生产一个一分钟有效期的积星码)\n";
         $message.="\n/:hug2.绑定桌码：\n1Q (一号桌服务员是我) ";
         $url=$_SERVER['HTTP_HOST'].url('we/desk/desks',['aid'=>$this->data['aid']]);
        
         $message.="\n".'<a href="http://'.$url.'">批量绑定</a>'; 
         
          
         return $message;
      }
      

      if(!$this->data['before'] and $this->data['back']){
        
        return model('we/WeSceneQrcode')->scanOrSubscribe($this->data); //后方命令集合
      }

      if($this->data['before'] ){
        if (is_numeric($this->data['before'])){
         return model('we/WeSceneQrcode')->deskAdmin($this->data); //绑定管理员
        }else{
         return model('we/WeSceneQrcode')->beforeWord($this->data); //
        }
        
      }

    }

    

    
}