<?php
// +----------------------------------------------------------------------
// | my
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://baiyf.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: NickBai <1902822973@qq.com>
// +----------------------------------------------------------------------
namespace app\we\model;

use think\Model;

class WeFeedChat extends Model
{

    protected $insert = ['status'=>1];  
    
    public function info($id,$field=true){
      $map['id']=$id;
      $feed = $this->field($field)->where($map)->find();
      return  $feed;
    }

    public function chat_total($feed){
      $map['feed']=$feed;
      $total = $this->where($map)->count('id');
      return  $total;
    }

    public function editData($data=''){

      if (!$data)$data=input('post.');

      if (isset($data['id'])){
         $res = $this->allowField(true)->where('id',$data['id'])->update($data);
      }else{
          $this->allowField(true)->data($data)->save();
          $res =$this->id;
      }
      
    
      return $res;
    }

 
    
}