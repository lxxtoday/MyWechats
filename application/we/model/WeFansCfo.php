<?php
// +----------------------------------------------------------------------
// | my
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://baiyf.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: NickBai <1902822973@qq.com>
// +----------------------------------------------------------------------
namespace app\we\model;

use think\Model;

class WeFansCfo extends Model
{

    protected $insert = ['status'=>1];  
    /**
     * 获取微信配置信息，需要缓存
     */
    public function info($openid,$field=true){
      
      $map['openid']=$openid;
      $cfo = $this->field($field)->where($map)->find();

      return  $cfo;
    }

     public function infoForFans($openid,$field=true){
      $fans=$this->info($openid,$field=true);
      if (!$fans) return false;
      $printer=model('printer/PrinterFans')->info($fans['openid'],'balance');

      $message="粉丝信息";
      $message.="\n粉丝ID：".$fans['id'];
      $message.="\n昵称：".$fans['nickname'];
      if($printer) $message.="\n打印余额：".$printer['balance'];
      return  ['fans'=>$fans,'message'=>$message];
    }

    public function infoForFansByOrder($data){
     $fans = $this->infoForFans($data['before']);
     if (!$fans) return ['message'=>'粉丝不存在，请核对粉丝ID'];
     return ['message'=>$fans['message']]; //空尾巴查询
    }

     public function getAdmins($appid){
      $map['appid']=$appid;
      $map['userid']=['NEQ',''];
     
      $admins=$this->where($map)->select();

      
      return  $admins;
    }




    public function editData($data){

      
      if (isset($data['id'])){
         $res = $this->allowField(true)->where('id',$data['id'])->update($data);
      }else{

         $res = $this->allowField(true)->data($data)->save();
      }
    
      return $res;
    }


    public function cfoScore($fans,$um,$data,$description='',$message='',$qrcode="") {
        
        $map['openid']=$fans['openid'];
        $have=$this->info($fans['openid']);
        //兼容多次积星
        $no_only=0;
          
        if (strstr($um,'*')){
          $um=str_replace('*',"",$um);
          $no_only=1;
        }
       
        if ($um>0){

          if ($qrcode){
            $have_qrcode=db('WeFansCfoLog')->where('openid',$fans['openid'])->where('qrcode',$qrcode)->limit(1)->find();
             if ($have_qrcode) return ['code'=>0,'message'=>'你已经扫过这个积星码，你可以分享给你朋友扫码积星'];
          }
          //有效性判断1.今天是否积过，2.是否扫码
          if ($no_only==0){
            $have_score=model('we/WeFansCfoLog')->haveAdd($fans['openid']);
            if ($have_score) return ['code'=>0,'message'=>'今天已经积过分'];
          } 
          if ($have){
            $res= $this->where($map)->setInc('score',$um);
          }else{
            $score['aid']=$fans['aid'];
            $score['openid']=$fans['openid'];
            $score['score']=$um;
            $res=$this->save($score);
          }
        } 
        
        if ($um<0) {
         
          if ($have and ($have['score']+$um)>=0){
           $res= $this->where($map)->setDec('score',abs($um));
          }else{
           $message='粉丝'.$fans['nickname'].'积星不足，积星结余'.($have['score']);
           return ['code'=>1,'message'=>$message];
          }
        }
        $cfo=$this->info($fans['openid']);
        if ($res){
        //记录日志流水 
         $log['aid']=$fans['aid'];
         $log['appid']=$data['appid'];
         $log['shopid']=(isset($data['fans']['admin_info'])?$data['fans']['admin_info']['shopid']:'');
         $log['type']=1;
         $log['pay_type']=0;
         $log['openid']=$fans['openid'];
         $log['admin']=(isset($data['admin'])?$data['admin']:'');
         $log['um']=$um;
         $log['balance']=$cfo['score'];
         $log['description']=$description;
         $log['qrcode']=$qrcode;
         $log_res=model('we/WeFansCfoLog')->editData($log);
         $message=$data['nickname'].'成功为'.$fans['nickname'].'变更积星'.$um.'，积星结余'.($cfo['score']).'——'.date('Y-m-d H:i:s');
         $notice=['msgtype'=>'text','text'=>['content'=>$message]];

         $error= custom_message($fans['openid'],$notice);
         return ['code'=>1,'message'=>$message];
       }else{
         return ['code'=>0,'message'=>'失败'];
       }
      }

    public function cfoAmount($fans,$um,$data,$description='',$message='') {

         $map['openid']=$fans['openid'];
         $have=$this->info($fans['openid']);
         if ($um>0){
            if ($have){
              $res= $this->where($map)->setInc('amount',$um);
            }else{
              $amount['aid']=$fans['aid'];
              $amount['openid']=$fans['openid'];
              $amount['amount']=$um;
              $res=$this->save($amount);
            }
          } 
          
          if ($um<0) {
           
            if ($have and ($have['amount']+$um)>=0){
             $res= $this->where($map)->setDec('amount',abs($um));
            }else{
             return $message='粉丝'.$fans['nickname'].'储值不足，储值结余'.($have['amount']);
            }
          }
          $cfo=$this->info($fans['openid']);
          if ($res){
          //记录日志流水 
         
           $log['aid']=$fans['aid'];
           $log['appid']=$fans['appid'];
           $log['shopid']=(isset($data['fans']['admin_info'])?$data['fans']['admin_info']['shopid']:'');
           $log['type']=0;
           $log['pay_type']=0;
           $log['openid']=$fans['openid'];
           $log['admin']=(isset($data['admin'])?$data['admin']:'');
           $log['um']=$um;
           $log['balance']=$cfo['score'];
           $log['description']=$description;
          
           $log_res=model('we/WeFansCfoLog')->editData($log); 
           if (!$message)$message=$data['nickname'].'成功为'.$fans['nickname'].'变更储值'.$um.'，储值结余'.($cfo['amount']).'，备注:'.$description.'——'.date('Y-m-d H:i:s');
           $notice=['msgtype'=>'text','text'=>['content'=>$message]];

           $error= custom_message($fans['openid'],$notice);
           return $message;
         }else{
           return false;
         }

      }

    
  
    

    
}