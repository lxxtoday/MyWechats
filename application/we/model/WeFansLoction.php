<?php
// +----------------------------------------------------------------------
// | my
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://baiyf.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: NickBai <1902822973@qq.com>
// +----------------------------------------------------------------------
namespace app\we\model;
use think\Model;
use think\helper\Time;

class WeFansLoction extends Model
{

    protected $insert = ['status'=>1];  
    /**
     * 获取微信配置信息，需要缓存
     */
    public function info($openid,$field=true,$map){
      
     
      $map['openid']=$openid;
      $loction = $this->field($field)->where($map)->find();
     
      return  $loction;
    }

    

    public function loctionAdd($data){
       $loction['openid']=$data['openid'];
       $loction['aid']=$data['aid'];
       $loction['Latitude']=$data['Latitude'];
       $loction['Longitude']=$data['Longitude'];
       $loction['Precision']=$data['Precision'];
      
       $have=$this->where('create_time','between time',Time::today())->find();
       if (!$have)$res = $this->allowField(true)->data($loction)->save();
       return $res;
    }


}