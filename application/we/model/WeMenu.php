<?php
// +----------------------------------------------------------------------
// | my
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://isofttime.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: $this <498944516@qq.com>
// +----------------------------------------------------------------------
namespace app\we\model;

use think\Model;
use app\we\sdk\Thinkwechat;


class WeMenu extends Model
{

    protected function initialize()
    {
        $this->buttonType=[
        'rselfmenu_0_0'=>'scancode_waitmsg',
        'rselfmenu_0_1'=>'scancode_push',
        'rselfmenu_1_0'=>'pic_sysphoto',
        'rselfmenu_1_1'=>'pic_photo_or_album',
        'rselfmenu_1_2'=>'pic_weixin',
        'rselfmenu_2_0'=>'location_select',
        ];
    }

    // 根据openid 获取用户
    public function info($key, $field = true)
    {
        
        if (!$key) return false;
        if (is_numeric($key)){
            $map['id'] = $key;
        }else{
            $map['openid'] = $key;
        }

        $fans=$this->field($field)->where($map)->find();

        return $fans;
    }

    
    // 更新用户
    public function createMenu($appid)
    {
       
       $menus=$this->where('appid'.$appid)->where('status','>',0)->select();
       foreach ($menus as $key => &$menu) {
           if($value['linkurl'])$menu['type']='view';
       }
        return $res;
    }

     


     /**
     * 获取菜单
     * @param int $id
     * @param bool $field
     * @return array
     * @author 郑钟良<zzl@ourstu.com>
     */
    public function sendMenu($id){
       
        $menu=$this->get($id);
      
        $buttons=db('WeMenuButton')->where('menu',$id)->where('status','>',0)->order('sort', 'desc')->select();

        //处理菜单类型
        foreach ($buttons as $key => &$button) {
            
          if(!$button['key']) continue;
            $formatMenu=$this->formatMenu($button);
            $button['type']= $formatMenu['type'];
            if (isset($formatMenu['key']))$button['key']= $formatMenu['key'];
            if (isset($formatMenu['url'])){
              $button['url']= $formatMenu['url'];
              unset($formatMenu['key']);
            }
           
        }
        
        $buttons = list_to_tree($buttons, $pk = 'id', $pid = 'pid', $child = 'sub_button', $root = 0);

        $my_menu['button']=$buttons;
        if ($menu['tag_id'] || $menu['sex'])$my_menu['matchrule']=$menu;
        
        return send_menu($menu['appid'],$my_menu);
        
        
    }

    protected function formatMenu($button){
       
        if(strstr($button['key'],'http://') or strstr($button['key'],'https://') ){
            return [ 
              'type' => 'view',
              'url' => $button['key'],
            ];
    
        } else {
            if (array_key_exists($button['key'],$this->buttonType)){
                $type= $this->buttonType[$button['key']];
            }else{
                $type= 'click';
            }
            return array(
               'type' => $type,
               'key' => $button['key']
            );
        }
    }


     public function editData(){

      $data=input("post.");
      
      if(isset($data['file'])) unset($data['file']);
      $data['aid']=session('aid');
     
      if ($data['id']){
         $res = $this->allowField(true)->where('id',$data['id'])->update($data);
      }else{
         $res = $this->allowField(true)->data($data)->save();
      }
      
      return $res;
    }

   




   
}