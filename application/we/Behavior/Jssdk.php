<?php 

namespace app\we\behavior;


/**
 * 引入JSSDK行为类
 * @author 艾逗笔<765532665@qq.com>
 */
class Jssdk  {

	public function run(&$params) {
		
		
		$appid=isset($params['appid'])?$params['appid']:'wx88ea8103c338755a';
		$jssdk = jssdk($appid);
		
	    return "
	    <script src='http://res.wx.qq.com/open/js/jweixin-1.0.0.js'></script>
	    <script>
	    var JSON_PAY = '".url('we/pay/json_pay')."';
	    wx.config({
	    	
	        appId: '".$jssdk["appId"]."',
	        timestamp: '".$jssdk["timestamp"]."',
	        nonceStr: '".$jssdk["nonceStr"]."',
	        signature: '".$jssdk["signature"]."',
	        jsApiList: [
	        'checkJsApi',
	            'onMenuShareTimeline',
	            'onMenuShareAppMessage',
	            'onMenuShareQQ',
	            'onMenuShareWeibo',
	            'hideMenuItems',
	            'showMenuItems',
	            'hideAllNonBaseMenuItem',
	            'showAllNonBaseMenuItem',
	            'translateVoice',
	            'startRecord',
	            'stopRecord',
	            'onRecordEnd',
	            'playVoice',
	            'pauseVoice',
	            'stopVoice',
	            'uploadVoice',
	            'downloadVoice',
	            'chooseImage',
	            'previewImage',
	            'uploadImage',
	            'downloadImage',
	            'getNetworkType',
	            'openLocation',
	            'getLocation',
	            'hideOptionMenu',
	            'showOptionMenu',
	            'closeWindow',
	            'scanQRCode',
	            'chooseWXPay',
	            'openProductSpecificView',
	            'addCard',
	            'chooseCard',
	            'openCard'
	        ]
	      });
		</script>
		<script src='/static/admin/js/jssdk.js'></script>
		";
	}
}

?>