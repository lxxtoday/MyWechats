<?php
namespace app\we\controller;
use think\Controller;
use app\we\sdk\Thinkwechat;


class Space extends Controller{

    public function login($openid=''){
        if (!$openid)$this->error('需要识别您的微信身份');
        $fans=model('we/WeFans')->info($openid);
        if(request()->isPost()){
            $data=input('post.');
            $mobile = explode('@', $data['mobile']);
            $map['mobile']=$mobile[0];
            $map['aid']=$mobile[1];
            $map['status']=['egt',1];
            $table=db('admin_user');
            $hasUser =$table ->where($map)->find();
           
            if(empty($hasUser)) $this->error('管理员不存在');
            if(md5(md5($data['password']) . config('data_auth_key'))!= $hasUser['password']) $this->error('密码错误');
            if(1 != $hasUser['status']) $this->error('账号被禁用'); 
            $res=db('WeFans')->where('openid',$openid)->update(['admin'=>$hasUser['id']]);
            if ($res) $this->success($hasUser['name'].'登录成功',url('index',['openid'=>$openid]));
            $this->error('不需要重复登录');
        }else{
            $this->assign('fans',$fans);
            return $this->fetch();
        }
    }

    public function index($appid='',$openid=''){
        
        $fans=we_auth($appid,$openid);
        if (!$fans['admin']) $this->error('请登录',url('login',['openid'=>$fans['openid']]));
        $factorys=db('factory')->where('aid',$fans['admin_info']['aid'])->select();
       
       
        $this->assign('fans',$fans);
        $this->assign('factorys',$factorys);
        $this->assign('myJssdk',['appid'=>$fans['appid']]);

        return $this->fetch();
      
    }

    public function amount($openid='',$pay=''){
      $fans=model('we/WeFans')->info($openid);
      if(request()->isPost()){
        if (!$pay)$this->error('请输入金额');
        switch ($pay) {
            case '0.01':
                $price=1;
                break;
            default:
            $price=$pay;
        }
        $pay=[
              'aid'=>$fans['aid'],
              'appid'=>$fans['appid'],
              'openid'=>$fans['openid'],
              'product'=>'会员充值',
              'description'=>'会员充值优惠多多',
              'price'=>$price,
              'um'=>1,
              'url'=>'we/WeFans',
              'data_id'=>$openid
              ];
        session('pay',$pay);
        $this->success('即将进入微信支付',url('we/pay/jssdk'));
      }else{
        $map['openid']=$openid;
        $map['type']=0;
        $map['status']=['egt',0];
        $logs=db('WeFansCfoLog')->where($map)->order('create_time desc')->select();
       
        $this->assign('openid',$openid);
        $this->assign('logs',$logs);
        $this->assign('fans',$fans);
        return $this->fetch();
      }
      
    }

     public function score($openid=''){
        $fans=model('we/WeFans')->info($openid);
        $map['openid']=$openid;
        $map['type']=1;
        $map['status']=['egt',0];
        $db=db('WeFansCfoLog');
        $logs=$db->where($map)->order('create_time desc')->select();
      
        $this->assign('logs',$logs);
        $this->assign('fans',$fans);
        return $this->fetch();
      
    }

    public function edit($openid=''){
      $fans=model('we/WeFans')->info($openid);
      
      if(request()->isPost()){
        $data=input('post.');
        $data['birthday']=strtotime($data['birthday']);
        $res=db('WeFans')->where('openid',$openid)->update($data);
        if ($res)$this->success('成功');
      }else{
        $this->assign('openid',$openid);
        $this->assign('fans',$fans);
        return $this->fetch();
      }
    }


}