<?php
namespace app\we\controller;
use think\Controller;


class Pay extends Controller{

    public function _initialize()
    {
        $this->buy_url ="http://".$_SERVER['HTTP_HOST']. url('we/pay/pay');
        $this->notify_url ="http://".$_SERVER['HTTP_HOST']. url('we/pay/notify');
    }

   
    public function index(){
        
    
    }

    public function jssdk(){
        
        $pay=session('pay');
        //创建订单
       
        
        //支付
        $jsApiParameters = get_jsapi_parameters($pay);
       
        $this->assign('pay',$pay);
        $this->assign('jsApiParameters',$jsApiParameters);
        $this->assign('myJssdk',['appid'=>$pay['appid']]);
        return $this->fetch();
    
    }


    public function paySuccess(){
        $pay=session('pay');
        $pay['status']=1;
        $pay['create_time']=time();

        $res=db('we_pay')->insertGetId($pay);
        model($pay['url'])->paySuccess();
        $this->success('成功');
    }



    public function jsapi($appid="wx88ea8103c338755a"){
        $pay=[
        'appid'=>$appid,
        'product'=>'商品名称',
        'description'=>'商品描述',
        'price'=>'1',
        ];
        $jsApiParameters = get_jsapi_parameters($pay);
        $this->assign('jsApiParameters',json_encode($jsApiParameters));
        $this->assign('myJssdk',['appid'=>$appid]);
        return $this->fetch();
    
    }

     /**
     * 获取微信支付参数
     * @author 艾逗笔<765532665@qq.com>
     */
    public function json_pay() {
        $jsApiParameters = get_jsapi_parameters(input('post.'));
        $this->json($jsApiParameters);
    }

    /**
     * 零时直接支付
     * @param openid 谁，顾客，可能不是支付者本人
     * @param product 商品
     * @param um 数量
     * @param price 价格
     * @param url 自动回调
     * @param ext 回调参数
     * 
     * @author $this 498944516@qq.com
     */
    public function pay($appid='wx88ea8103c338755a'){
         
        ini_set('date.timezone','Asia/Shanghai');

        $options = model('We')->info($appid);
     
        $data=[
        'buyer'=>'1111111',
        'product'=>'商品名称',
        'disc'=>'无描述',
        'um'=>2,
        'price'=>2,
        'url'=>url('paySuccess'),
        'ext'=>''
        ];

         //数据有效性
        if(!isset($data['buyer']) || !isset($data['um'])) $this->error('缺少支付参数');
         //全局引入微信支付类
        require_once(APP_PATH.'we/paysdk/example/WxPay.JsApiPay.php');
        //①、获取用户openid
        $tools = new \JsApiPay();
        $openId = $tools->GetOpenid();

        //②、统一下单
        $input = new \WxPayUnifiedOrder();

        $input->SetBody($data['product']);
        $input->SetAttach($data['product']);
         
        $input->SetOut_trade_no($out_trade_no=($options['mchid']?$options['mchid']:\WxPayConfig::MCHID).date("YmdHis"));
        
        $input->SetTotal_fee($data['price']*100);
        $input->SetTime_start(date("YmdHis"));
        $input->SetTime_expire(date("YmdHis", time() + 600));
        $input->SetGoods_tag($data['disc']);
        $input->SetNotify_url( $this->notify_url);
        $input->SetTrade_type("JSAPI");
        $input->SetOpenid($openId);
      
      
        $order = \WxPayApi::unifiedOrder($input);
        // $this->printf_info($order);
        $jsApiParameters = $tools->GetJsApiParameters($order);
         
        //获取共享收货地址js函数参数
        $editAddress = $tools->GetEditAddressParameters();

        //写入支付记录，标记为未支付
        $data['out_trade_no']=$out_trade_no;
        // $this->addPay($data);

      
       $this->assign('jsApiParameters',$jsApiParameters);
       $this->assign('data',$data);
       $this->assign('appid',$appid);
       return $this->fetch('pay');


    }

   


}