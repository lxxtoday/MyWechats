<?php
namespace app\we\controller;
use think\Controller;
use app\we\sdk\Thinkwechat;


class Desk extends Controller{

    public function index($shopId='',$tableNum='',$scan='',$openid=''){
      
        if ($scan+600>time()){
           $url="http://m.dianping.com/hobbit/order/scanredirect?shopid=".$shopId."&label=3&tablenum=".$tableNum."&fromthird=1";
           header("Location:".$url);
           die; 
         }else{
           $this->error('页面已经过期，请重新扫码',url('we/feed/index',array('openid'=>$openid)));
         }
      
    }

    public function desks($aid=2){
       if ( request()->isAjax() ) {
        $data=input();

        $this->success('努力搜索桌码',url('admin',['shopid'=>$data['shopid'],'appid'=>$data['appid'],'scene'=>$data['scene']]));
       }else{
        $shops=my_select('AdminShop','id','title',['aid'=>$aid]);
        $wes=my_select('we','appid','title',['aid'=>$aid,'we_type'=>3]);
        $scenes=my_select('WeScene','id','title',['aid'=>$aid]);

        $this->assign('shops',$shops);
        $this->assign('wes',$wes);
        $this->assign('scenes',$scenes);
        $this->assign('aid',$aid);
        return $this->fetch();
      }

       
      
    }

    public function admin($shopid=1,$appid='',$scene=''){
      $shop=model('admin/AdminShop')->info($shopid);
      if ( request()->isAjax() ) {
        $data=input();
        $qrcodes=array_values($data['qrcode']);
        $qrcodes_str=implode(',', $qrcodes);
       

        $res=db('WeSceneQrcode')->where('shopid',$shopid)->where('appid',$appid)->where('code','in',$qrcodes_str)->update(['admin'=>$data['admin']]);
        if ($res)$this->success('成功',url('admin',['shopid'=>$shopid,'appid'=>$appid,'scene'=>$scene,'time'=>time()]));



      }else{
        $map['shopid']=$shopid;
        $map['appid']=$appid;
        $map['scene']=$scene;
        $qrcodes=model('we/WeSceneQrcode')->where($map)->where('status>-1')->select();
        foreach ($qrcodes as $key => &$qrcode) {
          $qrcode['admin']=db('AdminUser')->where('id',$qrcode['admin'])->value('name');
          $qrcode['scan']=db('we_scene_scan')->where('qrcode_id',$qrcode['id'])->where('do',0)->whereTime('create_time','today')->count('id');
          $qrcode['subscribe']=db('we_scene_scan')->where('qrcode_id',$qrcode['id'])->where('do',1)->whereTime('create_time','today')->count('id');
          $um=str_replace('DESK',"",$qrcode['code']);
          $qrcode['buy']=db('we_fans_buy')->where('desk',$um)->where('shopid',$shopid)->whereTime('create_time','today')->sum('price');

        }
        $admins=my_select('AdminUser','id','name',['shopid'=>$shopid]);
       
        $this->assign('shopid',$shopid);
        $this->assign('appid',$appid);
        $this->assign('scene',$scene);
        $this->assign('admins',$admins);
        $this->assign('qrcodes',$qrcodes);
        $this->assign('shop',$shop);
        return $this->fetch();

      }

        
      
    }

}