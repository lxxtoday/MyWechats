<?php
namespace app\we\controller;
use think\Controller;
use app\we\sdk\Thinkwechat;
class Index extends Controller{
    /**
     * 微信消息接口入口
     * 所有发送到微信的消息都会推送到该操作
     * 所以，微信公众平台后台填写的api地址则为该操作的访问地址
     * 所有的响应都有机器人响应
     * $this 498944516@qq.com
     */
    public function index($appid=0){

        //获取配置连接微信
        $this->appid=$appid;
        $weObj=we_init($appid);
        $res=$weObj->valid();

        //分解数据获得常用字段
        $this->openid = $weObj->getRev()->getRevFrom();
        $this->type = $weObj->getRev()->getRevType();
        $this->data = $weObj->getRev()->getRevData();
      
        // if ($this->openid !='oq9Vctwv-Vigpz0Ls7cJZdhAdPZo')$weObj->text('22:00-24:00系统升级维护')->reply();
        //用户检测，如果有就存入data,没有则存入数据库
        $fans=model('we/WeFans')->info($this->openid);
        if (!$fans){
           $fans=get_user_info($this->appid,$this->openid );
           if ($fans){
            model('WeFans')->editData($fans); 
            $fans=model('WeFans')->info($this->openid);
           }else{
            //todo 报警
            mylog('没有获取到用户信息，请检查接口权限'.date('Y-m-d H:i'));
           }
        }
  
        //补充常用相关数据到DATA
        $this->data['aid']=$weObj->options['aid'];
        $this->data['appid']=$weObj->options['appid'];
        $this->data['openid']=$fans['openid'];
        $this->data['admin']=$fans['admin'];
        $this->data['nickname']=$fans['nickname'];
        $this->data['options']=$weObj->options;
        $this->data['fans']=$fans;
      
        //查询是否已经接受过此消息
        if ($this->have()){
            $weObj->text('处理中，请稍后！')->reply();
            die;
        }

        model('WeMessage')->saveMessage($this->data); //记录消息
        db('WeFans')->where('openid',$this->openid)->setField('update_time',time());  //记录心跳
        $message=$this->messageType();  //处理消息
    
        //处理兼容
        if (!is_array($message)){
            $reply['message']=$message;
        }else{
            $reply=$message;
        }
        if (!isset($reply['type'])) $reply['type']='text';


        $reply['message']=isset($reply['message'])?we_replace($reply['message'],$this->data):'请记住代码和我爱着你';
        //todo 图文关键词替换

    
        $weObj->$reply['type']($reply['message'])->reply();
 
        die; 
    }
    // 处理微信消息总入口
    public function messageType(){
       
        switch($this->type) {
            case Thinkwechat::MSGTYPE_TEXT:
                   $reply=model('rob/Rob')->reply($this->data['Content'],$this->data);
                    break;
            case Thinkwechat::MSGTYPE_IMAGE:
                   $reply=model('rob/Rob')->reply("imageP",$this->data,1);  //根据类型进准匹配
                    break;
            case Thinkwechat::MSGTYPE_VOICE:
                   if (isset($this->data['Recognition'])){
                    $keyword=$this->data['Recognition']; 
                   }else{
                     $keyword=$this->type;
                   }
                  
                   $reply=model('rob/Rob')->reply($keyword,$this->data);
                   break;
            case Thinkwechat::MSGTYPE_MUSIC:
                   
                    break;
            
            case Thinkwechat::MSGTYPE_VIDEO:
                

                    break;
            case Thinkwechat::MSGTYPE_LOCATION:
                 
                    break;
            case Thinkwechat::MSGTYPE_LINK:
                  
                    break;
            case Thinkwechat::MSGTYPE_EVENT:
                  $reply=$this->messageEvent();
                    break;
            default: 
                 $reply=['type'=>'text','message'=>'success'];
                  break;
                    
        }
        return $reply;
    }
    // 处理微信事件消息
    public function messageEvent(){
        $reply=[];
        switch($this->data['Event']){
            case Thinkwechat::EVENT_SUBSCRIBE:
                $res=db('WeFans')->where('openid',$this->openid)->update(['subscribe'=>1,'subscribe_time'=>time()]);
                list($answer,$data)=model('WeSceneScan')->scan($this->data);
                $this->data=$data;
                if ($answer) $reply=model('rob/Rob')->reply($answer,$this->data);
               
                break;
            case Thinkwechat::EVENT_UNSUBSCRIBE:
                $res=db('WeFans')->where('openid',$this->openid)->update(['subscribe'=>0,'unsubscribe_time'=>time()]);
                $reply=['type'=>'text','message'=>'success'];
                break;
            case Thinkwechat::EVENT_SCAN:
                list($answer,$data)=model('WeSceneScan')->scan($this->data);
                $this->data=$data;
                if ($answer) $reply=model('rob/Rob')->reply($answer,$this->data);
                break;
            case Thinkwechat::EVENT_LOCATION:
                $res=model('we/WeFansLoction')->loctionAdd($this->data);
                $reply['message']="";
                break;
            case Thinkwechat::EVENT_MENU_CLICK:
                $reply=model('rob/Rob')->reply($this->data['EventKey'],$this->data,1);
                break;
            case Thinkwechat::EVENT_MENU_SCAN_PUSH:
                 
                break;
            case Thinkwechat::EVENT_MENU_SCAN_WAITMSG:
               
                
                break;
            case Thinkwechat::EVENT_MENU_PIC_SYS:
                
                break;
            case Thinkwechat::EVENT_MENU_PIC_PHOTO:
               
                break;
            case Thinkwechat::EVENT_MENU_PIC_WEIXIN:
               
                break;
            case Thinkwechat::EVENT_MENU_LOCATION:
               
                break;
            case Thinkwechat::EVENT_SEND_MASS:
                $reply['message']='success';
                break;
            case Thinkwechat::EVENT_SEND_TEMPLATE:
                //模板消息发送成功
                // $map['ToUserName']=$this->data['FromUserName'];
                // $map['FromUserName']='templateMessage';
                // $message=db('WeMessage')->where($map)->order('CreateTime desc')->find();
                // $res=db('WeMessage')->where(['id'=>$message['id']])->setField('status',1);
                $reply['message']='success';
                break;
            case Thinkwechat::EVENT_KF_SEESION_CREATE:
                
                break;
            case Thinkwechat::EVENT_KF_SEESION_CLOSE:
                
                break;
            case Thinkwechat::EVENT_KF_SEESION_SWITCH:
                
                break;
            case Thinkwechat::EVENT_CARD_PASS:
                
                break;
            case Thinkwechat::EVENT_CARD_NOTPASS:
                
                break;
            case Thinkwechat::EVENT_CARD_USER_GET:
                $reply['message']='恭喜您获得卡券，请在微信卡包查看，欢迎到店使用！';
                 break;
            case Thinkwechat::EVENT_CARD_USER_DEL:
                $reply['message']='您删除了卡券，是我们的卡券优惠力度不够么？反馈建议，获得更大卡券！';
                
                break;
            case Thinkwechat::WifiConnected :
                
                break;
            case Thinkwechat::ShakearoundUserShake :
                
                break;
            default:
                $reply['message']='success';
                break;
           }
            return $reply;
        }


        public function have(){
            
            if (isset($this->data['MsgId'])){
                $have=model('WeMessage')->info($this->data['MsgId']);
            }else{
                $have=db('WeMessage')->where('FromUserName',$this->openid)->where('CreateTime',$this->data['CreateTime'])->find();
            }
            return $have;
            
        }
   
  
}


