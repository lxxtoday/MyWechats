<?php
namespace app\admin\controller;

use app\admin\my\MyPage;
use app\admin\my\MyConfig;


class We extends Base {
   
	
    function _initialize()
    {
        $this->we_type = [ '订阅号', '认证订阅号', '服务号','认证服务号' ];
        $this->rank=array(1=>'好评',2=>'中评',3=>'差评');
        $this->sex = ['未知','男','女'];
        $this->qrcode_type=['临时二维码','数值型永久二维码','字符型永久二维码'];
        parent::_initialize();
    }

    public function we() {
		$my = new MyPage();
	   
	    $my ->title("微信公众号管理")
		   
		    ->buttonNew()
		    ->buttonTrash('we')
		    ->buttonRestore('we')
		    ->buttonEnable('we')
		    ->buttonDisable('we')

		 
		    ->keyId()
		    ->keyLink('title', '名称','we/index/index?appid={$appid}','new')
		    ->keyText('description', '简介')
		    ->keyMap('we_type', '类型', $this->we_type)
		    ->actionEdit()
		    ->action('getCards?appid={$appid}','拉取卡券')
		    ->actionStatus('we')
		    ->map('title', '名称', '微信公众号昵称')
		    ->map('wechat_type', '类型',  'select', $this->we_type)
		    ->mapStatus();

    	
    		 $data=my_data('We');
    		 return $my->data($data)->fetch();
	    
    }



    public function weEdit($id=0) {
		if ( request()->isPost() ) { 
			$res = model('we/We')->editData();
			if ($res>0) $this->success('更新成功','we');
			$this->error('更新失败');
		} else {
			$data=my_edit('we/We',$id);
			$cards=[];
            if (isset($data['appid']))$cards=my_select('WeCard','card_id','title',['appid'=>$data['appid'],'quantity'=>['GT',0]]);

			$rob_array = db("rob")
			->field("id,title")
			->where([
				'status' => ['gt', -1],
			])->select();

			$my = new MyConfig();
			return  $my->title("编辑微信公众号")->keyId()
			    ->keyText('title', '名称')
			    ->keySelect('we_type', '类型', $this->we_type)
			    ->keyText('description', '简介')
			    
			    ->keyText('token', 'Token')
			    ->keyText('encodingaeskey', 'EncodingAESKey')
			    ->keyText('appid', 'AppID')
			    ->keyText('appsecret', 'AppSecret')
			    
			    ->keyText('mchid', 'mchid')
			    ->keyText('pay_key', 'pay_key')
			    ->keyText('apiclient_cert', 'apiclient_cert')
			    ->keyText('apiclient_key', 'apiclient_key')
			    // ->keyChosen('rob','机器人', null, $rob_array)
			    ->keyText('rob','机器人')
			    ->keyText('turing','图灵秘钥')
			    ->keyImage('qrcode', '二维码')
			    ->keyImage('tui_pic', '推广背景图')
			     ->keyText('tui_reward','推广提成','例如0.01')

			    ->keyText('feed_url','反馈跳转')
			    ->keySelect('feed_card','反馈卡券',$cards)
			    ->keySelect('tui_scan_card','推广扫描卡券',$cards)
			    ->keySelect('tui_subscribe_card','推广关注卡券',$cards)

			    ->group( '基本', 'fa-cog', 'id,title,we_type,desc,token,encodingaeskey,appid,appsecret')
			   
			    ->group( '微信支付', 'fa-wechat', 'mchid,pay_key,apiclient_cert,apiclient_key')
			    ->group( '机器人', 'fa-wechat', 'rob,turing')
			    ->group( '运营资料', 'fa-wechat','qrcode,feed_url,feed_card')
			    ->group( '推广资料', 'fa-wechat', 'tui_pic,tui_scan_card,tui_subscribe_card,tui_reward')
			    ->buttonSubmit()->buttonBack()
                ->data($data)->fetch();
		}
	}


	 public function fans() {
		$my = new MyPage();
	    $Wes=my_select('We','appid','title');
	    $my ->title("微信粉丝管理")->buttonNew('fansEdit')
		    
		    ->keyId()
		    ->keyText('nickname', '昵称')
		    ->keyText('remark', '备注')
		    ->keyMap('sex', '性别', ['未知','男','女'])
		    ->keyText('city', '城市')
		    ->keyJoin('appid', '微信','appid','title','we')
		    ->keyMap('subscribe', '关注', ['-','ok'])
		    // ->keyText('scan', '来访')
		    // ->keyText('score', '积星')
		    // ->keyText('amount', '储值')
		    

		    ->actionEdit()
		    ->actionStatus('WeFans')
		    ->actionDelete()

		    
		    ->mapLike('nickname', '昵称')
		    ->map('appid', '微信', 'select', $Wes)
		    ->map('wechat_type', '性别', 'select', ['未知','男','女'])
		    ->mapSelect('subscribe','关注状态',['取消关注','关注']);
		    

    
    		$data= my_data('WeFans');
    		// foreach ($data['rows'] as $key => &$value) {
    		// // 	$value['scan']=model('we/WeSceneScan')->getScanDays($value['openid']);
    		// 	$cfo=model('we/WeFansCfo')->where('openid',$value['openid'])->field('score,amount')->find();
    		// 	$value['score']=$cfo['score'];
    		// 	$value['amount']=$cfo['amount'];
    		// }
    		
	  
		    return $my->data($data)->fetch();
	  
    }


    public function fansEdit($id=0) {
		

		if ( request()->isPost() ) { 
			$res = model('we/WeFans')->editData();
			if ($res>0) $this->success('更新成功','we');
			$this->error('更新失败','we');
		} else {
			
			$data=my_edit('we/WeFans',$id);

			$my = new MyConfig();
			return  $my->title("编辑微信公众号")->keyId()
					    ->keyText('nickname', '昵称')->keyText('remark', '备注')
					    ->keySelect('sex', '性别', $this->sex)
					    

					    ->group('basic', '基本', 'fa-cog', 'id,nickname,remark')
					    ->group('wechat', '微信', 'fa-wechat', 'sex')
					    ->buttonSubmit()->buttonBack()
                        ->data($data)->fetch();
		}
	}


	public function fansCfo() {
		$data= my_data('fans_cfo');
		$Wes=my_select('We','appid','title');
		$my = new MyPage();
	    $my ->title("粉丝财产")->buttonNew('fansEdit')
		    
		    ->keyId()
		    ->keyText('nickname', '昵称')
		    ->keyText('remark', '备注')
		    ->keyMap('sex', '性别', ['未知','男','女'])
		    ->keyText('city', '城市')
		    ->keyJoin('appid', '微信','appid','title','we')
		    ->keyMap('subscribe', '关注', ['-','ok'])
		    ->keyText('scan', '来访')
		    ->keyText('score', '积星')
		    ->keyText('amount', '储值')
		    

		    ->actionEdit()
		    ->actionStatus()
		    ->actionDelete()

		    
		    ->mapLike('nickname', '昵称')
		    ->map('appid', '微信', 'select', $Wes);

    		
    	
    	
		    return $my->data($data)->fetch();
	  
    }

	
    // 财产记录
	public function cfoLog() {
		$data=my_data('WeFansCfoLog');
		$my = new MyPage();
	   
	    $my ->title("粉丝财产流水记录")
		   
		    ->keyJoin('openid', '粉丝', 'openid', 'nickname', 'we_fans')
		    ->keyJoin('appid', '微信', 'appid', 'title', 'we')
		    ->keyJoin('shopid', '分店', 'id', 'title', 'admin_shop')
		    ->keyMap('type', '类型', ['储值','积星'])
		    ->keyMap('pay_type', '支付', ['命令','微信'])
		    ->keyText('um', '数量')
		    ->keyText('balance', '余额')
		    ->keyCreateTime()
		   
		    ->actionEdit()
		    ->actionStatus('WeFansCfoLog')
		    ->mapLike('nickname', '名称')
		    ->map('openid', 'openid')
		    ->mapStatus();
		     return $my->data($data)->fetch();
	   
    }

    // 财产记录
	public function buy() {
		
	    $wes=my_select('we','appid','title');
		$shops=my_select('admin_shop');
		$admins=my_select('admin_user','id','name');
	    $data=my_data('WeFansBuy');
	    $group_arr=['months'=>'按月统计','weeks'=>'按周统计','days'=>'按天统计','shopid'=>'分店统计','last_admin'=>'老顾客统计'];
	    $cell=array(array('openid','粉丝'), array('appid','微信'), array('shopid','分店'),array('desk','桌号'),array('customer','人数'),array('price','金额'),array('this_admin','服务员'),array('last_admin','老顾客'),array('admin','操作'));
	   
	    $my = new MyPage();
	    $my ->title("粉丝消费流水");
	    if (isset($data['group'])){
	    	if ($data['group']=='shopid'){
	    		 $my ->keyJoin($data['group'],$group_arr[$data['group']],'id', 'title', 'admin_shop');
            }else{
            	 $my ->keyText($data['group'],$group_arr[$data['group']]);
            }
        $my ->keyText('desk', '桌号')
		    ->keyText('customer', '人数')
		    ->keyText('price', '金额')
		    ->echart('统计图表','小标题',['桌数'=>'desk', '顾客'=>'customer', '营业额'=>'price'],$data['group']);
	    }else{
	    $my	->keyId()
		    ->keyJoin('openid', '粉丝', 'openid', 'nickname', 'we_fans')
		    ->keyJoin('appid', '微信', 'appid', 'title', 'we')
		    ->keyJoin('shopid', '分店', 'id', 'title', 'admin_shop')
		    ->keyText('desk', '桌号')
		    ->keyText('customer', '人数')
		    ->keyText('price', '金额')
		    ->keyJoin('this_admin', '服务员', 'id', 'name', 'admin_user')
		    ->keyJoin('last_admin', '老顾客', 'id', 'name', 'admin_user')
		    ->keyJoin('admin', '操作', 'id', 'name', 'admin_user')
		    ->keyCreateTime()
		    ->actionEdit()->actionStatus('WeFansBuy');
		}
		$my ->mapLike('desk', '桌号')->mapCreateTime()
		    ->mapSelect('appid', '微信',$wes)
		    ->mapSelect('shopid', '分店',$shops)
		    ->mapSelect('this_admin', '服务员',$admins)
		    ->mapSelect('last_admin', '老顾客',$admins)
		    ->mapSelect('admin', '操作',$admins)
		    ->mapGroup('统计',$group_arr,'id','desk,customer,price');
		    return $my->data($data)->xls('考核流水表',$cell)->fetch();
	    
    }



	 public function menu() {
		$my = new MyPage();
	    $Wes=my_select('We','appid','title');
	    $my ->title("微信导航管理") ->buttonNew('menuEdit')
		   
		    ->keyId()
		    ->keyJoin('appid', '微信','appid','title','we')
		    ->keyText('title', '菜单')
		    ->keyText('tag_id', '标签')
		    ->keyMap('sex', '性别',['全部','男','女'])
		    ->keyText('language', '语言')
		    ->keyText('country', '祖国')
		    ->keyText('province', '省份')
		    ->keyText('city', '城市')
		    ->keyText('client_platform_type', '终端')
		    ->actionEdit()
		    ->action('menuButton?menu=####','按钮')
		    ->action('sendMenu?id=####','推送')
		    ->actionStatus('WeMenu')
		    
		    
		    ->map('title', '菜单')
		    ->map('appid', '微信', 'select', $Wes)
		    ->mapStatus();

    	
    		 $data=my_data('WeMenu');
	         return $my->data($data)->fetch();
	    
    }

    public function sendMenu($id) {
        $res=model('we/WeMenu')->sendMenu($id);
        if (!$res){
        	$this->success('同步成功');
        }else{
        	$this->error($res);
        }

	}


	// 编辑&添加
    public function menuEdit($id=0) {
		

		if ( request()->isPost() ) { 
			$res = model('we/WeMenu')->editData();
			if ($res>0) $this->success('更新成功','menu');
			$this->error('更新失败');
		} else {
			$Wes=my_select('We','appid','title');
			$data=my_edit('we/WeMenu',$id);

			$my = new MyConfig();
			return  $my->title("编辑微信公众号")->keyId()
					    ->keyText('title', '菜单')
					    ->keySelect('appid','微信',$Wes)
					    ->keySelect('sex', '性别',$this->sex)
					    ->keySelect('tag_id', 'tag_id', $this->sex)
					    ->keySelect('language', 'language', $this->sex)
					    ->keySelect('country', 'country', $this->sex)
					    ->keySelect('province', 'province', $this->sex)
					    ->keySelect('city', 'city',$this->sex)
					    ->keySelect('client_platform_type', 'client_platform_type',$this->sex)
					    
					    ->buttonSubmit()->buttonBack()
                        ->data($data)->fetch();
		}
	}

	public function menuButton($menu='') {
		$my = new MyPage();
	   
	    $my ->title("按钮管理")  ->buttonNew('menuButtonEdit?menu='.$menu)
		    
		    ->keyId()
		    ->keyJoin('menu', '菜单','id','title','we_menu')
		    ->keyLink('title_show', '按钮','menuButton?menu={$menu}&pid=####')
		    
		    ->keyText('key', '机器人')
		    ->keyText('sort', '排序')
		    

		    ->actionEdit('menuButtonEdit?menu={$menu}&id=####')
		    ->actionStatus()
		    ->actionDelete()

		    
		    ->map('name', '菜单')
		    ->mapStatus();

    	
    		$data=$data=my_data('WeMenuButton',['menu'=>$menu]);
    		$data['rows'] = model('common/Tree')->toFormatTree($data['rows']);
    		
    		
		    return $my->data($data)->fetch();
	    
    }


	
    public function menuButtonEdit($id=0,$menu='') {
		
       
		if ( request()->isPost() ) { 
			
		    if (!input('menu')) $this->error('请选择所属菜单');
			$res = model('we/WeMenuButton')->editData();
			if ($res>0) $this->success('更新成功',url('menuButton',['menu'=>input('menu')]));
			$this->error('更新失败');
		} else {
			$data=my_edit('we/WeMenuButton',$id);
			$data['menu']=$menu;
			$menus=my_select('WeMenu');
			$buttons=my_select('WeMenuButton','id','name',['pid'=>0,'menu'=>$menu]);
			
            $answers=my_select('RobAnswer','keyword','title');
			$my = new MyConfig();
			return  $my->title("编辑按钮")->keyId()
					    ->keyText('name', '按钮')
					    ->keySelect('menu', '菜单',$menus)
					    ->keySelect('pid', '父按钮',['0'=>'根按钮']+$buttons)
					    ->keyText('key', '关键词','可以是网址')
					    ->keyText('sort', '排序')
					    
					    ->buttonSubmit()->buttonBack()
                        ->data($data)->fetch();
		}
	}


    // 场景二维码开始
	public function scene() {
		$my = new MyPage();
	   
	    $my ->title("场景管理") ->buttonNew("sceneEdit")
		  
		    ->keyId()
		    ->keyText('title', '名称')
		    ->keyText('desc', '简介')
		    ->keyCreateTime()->keyUpdateTime()
		    ->actionEdit()
		    ->actionStatus('WeScene')
		    ->map('title', '名称')
		    ->mapStatus();

    	
    		$data=my_data('WeScene');
	  
		    return $my->data($data)->fetch();
	  
    }


    public function sceneEdit($id=0) {
		

		if ( request()->isPost() ) { 

			$res = model('we/WeScene')->editData();
			if ($res>0) $this->success('更新成功','scene');
			$this->error('更新失败');
		} else {
			
			$data=my_edit('we/WeScene',$id);

			$my = new MyConfig();
			return  $my->title("编辑场景")->keyId()
					    ->keyText('title', '名称')
					    ->keyText('desc', '简介')
					   
					    ->buttonSubmit()->buttonBack()
                        ->data($data)->fetch();
		}
	}

	// 场景二维码开始
	public function qrcode() {
		
	    $shops=my_select('AdminShop');
		$wes=my_select('we','appid','title');
		$scenes=my_select('WeScene');
		$data=my_data('WeSceneQrcode',['qrcode_type'=>1]);
		$cell=array(array('name','姓名'), array('shopid','分店'), array('mobile','电话'),array('idcard','身份证'),array('bankcard','银行卡'),array('amount','余额'),array('status','状态'));
		$group_arr=['shopid'=>'分店统计'];
		$my = new MyPage();
	    $my ->title("二维码管理") ->buttonNew("qrcodeEdit",'增加永久二维码')->buttonNew(url('',['xls'=>1]),'导出')
	        ->buttonNew("qrcodePi",'批量新增[危险]')
		    ->buttonTrash('WeSceneQrcode')
		    ->buttonRestore('WeSceneQrcode')
		    ->buttonEnable('WeSceneQrcode')
		    ->buttonDisable('WeSceneQrcode');

		if (isset($data['group'])){
	    	if ($data['group']=='shopid'){
	    		 $my ->keyJoin($data['group'],$group_arr[$data['group']],'id', 'title', 'admin_shop');
            }else{
            	 $my ->keyText($data['group'],$group_arr[$data['group']]);
            }
        $my ->keyText('um', '数量')
		    ->keyText('price', '单价')
		    ->keyText('total_price', '总绩效')
		    ->echart('统计图表','小标题',['数量'=>'um', '单价'=>'price', '总绩效'=>'total_price'],$data['group']);
	    }else{
		$my ->keyId()
		    ->keyLink('title', '名称','{$short_url}')
		    ->keyJoin('shopid', '分店', 'id', 'title', 'admin_shop')
		    ->keyJoin('admin', '管理员', 'id', 'name', 'admin_user')
		    ->keyJoin('scene', '场景', 'id', 'title', 'we_scene')
		    ->keyMap('qrcode_type', '类型', $this->qrcode_type)
		    ->keyText('code', '标示')
		    ->keyText('short_url', '二维码')
		    ->keyText('subscribe', '关注')
		    ->keyText('SCAN', '扫码')
		    ->actionEdit()
		    ->actionStatus('WeSceneQrcode');
		}
		   
		$my ->map('title', '名称')
		    ->mapSelect('shopid', '分店',$shops)
		    ->mapSelect('appid', '微信',$wes)
		    ->mapSelect('scene', '场景',$scenes)
		    ->mapGroup('统计',$group_arr,'id','subscribe,SCAN'); ;

		    return $my->data($data)->xls('场景二维码表',$cell)->fetch();
	   
    }


    public function qrcodeEdit($id=0,$qrcode_type=1) {
		if ( request()->isPost() ) { 
			$res = model('we/WeSceneQrcode')->editData();
			if ($res>0) $this->success('更新成功','qrcode');
			$this->error('更新失败');
		} else {

			$shops=my_select('AdminShop');
			$wes=my_select('we','appid','title');
			$scenes=my_select('WeScene');
			$admins=my_select('AdminUser','id','name');
			$data=my_edit('we/WeSceneQrcode',$id);
			$data['qrcode_type']=1;
			$my = new MyConfig();
			$my->title("编辑推广二维码")->keyId()
					    ->keyText('title', '二维码')
					    ->keyText('describe', '备注')
					    ->keySelect('appid','微信',$wes)
					    ->keySelect('shopid','分店',$shops)
					    ->keySelect('scene','场景',$scenes)
					    ->keyText('describe', '简介')
					    ->keyText('code', '特殊识别')
					    ->keyReadOnly('qrcode_type', '类型')
					    ->keyBool('is_desk','桌码')
					    ->keySelect('admin','管理员',$admins)
					    ->keyText('subscribe_answer', '关注机器人')
					    ->keyText('SCAN_answer', '扫描机器人');
			
			$my ->buttonSubmit()->buttonBack()
                ->data($data)
            ->group('基础','fa fa-wechat','id,title,appid,shopid,scene,desc,code,is_desk,admin')
            ->group('机器人','fa fa-wechat','subscribe_answer,SCAN_answer')
            ->group('系统','fa fa-wechat','qrcode_type');
            return $my->data($data)->fetch();
		}
	}

	public function qrcodePi($id=0) {
		set_time_limit(0);
		if ( request()->isPost() ) { 
			$data=input();
			//有效性
			if(!$data['appid'] || !$data['shopid'] || !$data['scene']) $this->error('微信，分店，场景必须选择！');
			$qrcode['aid']=session('aid');
			$qrcode['shopid']=$data['shopid'];
			$qrcode['appid']=$data['appid'];
			$qrcode['scene']=$data['scene'];
			$qrcode['qrcode_type']=1;
			$qrcode['status']=1;
			$qrcode['create_time']=time();
			
			$qrcode['subscribe_answer']='桌码机器人';
			$qrcode['SCAN_answer']='桌码机器人';
			$shop=model('admin/AdminShop')->info($data['shopid']);
			for($i=$data['start'];$i<=$data['end'];$i++){
			if ($data['tiao']){
			  if(strstr($data['tiao'],$i) or $data['tiao']==$i) continue;
		    }
            $qrcode['title']=$data['before'].$i;
            $qrcode['code']='DESK'.$i;
            $qrcode['describe']=$shop['title'].$qrcode['title'].'号桌';

            $get_qrcode=get_qrcode($data['appid'],1);
	        if (!is_array($get_qrcode)) die($get_qrcode);
	        $res =db('WeSceneQrcode')->insert($get_qrcode+$qrcode);
	        $path="uploads/we/qrocdeDesk/".$shop['title'];
            $qrcode_name=$qrcode['title'].'.jpg';
            createFolder($path);
            get_file_from_net($get_qrcode['short_url'],$path,$qrcode_name);
            }
		
			$this->success('更新成功',url('',['appid'=>$data['appid'],'shopid'=>$data['shopid'],'scene'=>$data['scene']]));
			$this->error('更新失败');
		} else {
			$shops=my_select('AdminShop');
			$wes=my_select('we','appid','title');
			$scenes=my_select('WeScene');
			
			$data=my_edit('we/WeSceneQrcode',$id);
			$data['qrcode_type']=1;
			$data['shopid']=input('shopid');
			$data['appid']=input('appid');
			$data['scene']=input('scene');
			$my = new MyConfig();
			$my->title("批量生成二维码")->keyId()
					    ->keySelect('appid','微信',$wes)
					    ->keySelect('shopid','分店',$shops)
					    ->keySelect('scene','场景',$scenes)
					    ->keyText('before', '前缀字母')
					    ->keyText('start', '开始数字')
					    ->keyText('tiao', '跳过数字')
					    ->keyText('end', '结束数字');
					    
					 
			
			$my ->buttonSubmit()->buttonBack()
                ->data($data);
           
            return $my->data($data)->fetch();
		}
	}

	// 场景二维码开始
	public function scan() {
        $shops=my_select('AdminShop');
        $wes=my_select('we','appid','title');
		$scenes=my_select('WeScene');
        $data=my_data('WeSceneScan');
        $group_arr=['months'=>'按月统计','weeks'=>'按周统计','days'=>'按天统计','shopid'=>'分店统计','last_admin'=>'老顾客统计'];
        $cell=array(array('toadmin','对象'), array('shopid','分店'), array('project_type','分类'),array('project_id','项目'),array('um','数量'),array('price','绩效'),array('total_price','总绩效'),array('description','描述'),array('admin','考核'));
	   
	    $my = new MyPage();
	    $my ->title("扫码日志")->buttonNew(url('',['xls'=>1]),'导出');
	    if (isset($data['group'])){
	    	if ($data['group']=='shopid'){
	    		 $my ->keyJoin($data['group'],$group_arr[$data['group']],'id', 'title', 'admin_shop');
            }else{
            	 $my ->keyText($data['group'],$group_arr[$data['group']]);
            }
        $my ->keyText('id', '扫码量')
            ->echart('统计图表','小标题',['数量'=>'id'],$data['group']);
        }else{
		$my ->keyId()
		    ->keyText('nickname', '粉丝')
		    // ->keyJoin('appid', '微信', 'appid', 'title', 'we')
		    ->keyText('qrcode', '二维码')
		    ->keyJoin('shopid', '分店', 'id', 'title', 'admin_shop')
		    ->keyJoin('scene', '场景', 'id', 'title', 'we_scene')
		    ->keyJoin('admin', '服务', 'id', 'name', 'admin_user')
		    ->keyJoin('last_admin', '老顾客', 'id', 'name', 'admin_user')
		    // ->keyMap('qrcode_type', '类型', $this->qrcode_type)
		    ->keyMap('do', '动作', ['0'=>'扫码','1'=>'关注'])
		    ->keyCreateTime()
		   
		    ->actionEdit()
		    ->actionStatus('WeSceneScan');
		}
		$my ->map('qrcode', '名称')->mapCreateTime()
		    ->mapSelect('shopid', '分店',$shops)
		    ->mapSelect('appid', '微信',$wes)
		    ->mapSelect('scene', '场景',$scenes)
		    ->mapSelect('do', '行为',['扫码','关注'])
		    ->mapSelect('qrcode_type','类型',$this->qrcode_type)
		    ->mapGroup('统计', $group_arr,'id','');
		    
		    return $my->data($data)->xls('考核流水表',$cell)->fetch();
	    
    }


    public function scanEdit($id=0,$qrcode_type=0) {
		if ( request()->isPost() ) { 
			$res = model('We/WeSceneQrcode')->editData();
			if ($res>0) $this->success('更新成功','we');
			$this->error('更新失败','we');
		} else {

			$shops=my_select('AdminShop');
			$scenes=my_select('WeScene');
			$data=my_edit('we/WeSceneQrcode',$id);
			$my = new MyConfig();
			$my->title("编辑推广二维码")->keyId()
					    ->keyText('title', '二维码')
					    ->keyText('desc', '简介')
					    ->keySelect('shop_id','分店',$shops);
					   
					 
			if ($qrcode_type==1){
				$my->keyText('scene_str', 'scene_str','可以不填，不填就默认为scene_id')->keySelect('scene','场景',$scenes);
			}else{
				$my->keyText('expire', '有效期');
			}

			// $my ->keySelect('userid','类型','',$users);
			$my ->buttonSubmit()->buttonBack()
                ->data($data);
            return $my->data($data)->fetch();
		}
	}

	
	public function feed() {
		$wes=my_select('we','appid','title');
		$shops=my_select('admin_shop');
		$data=my_data('WeFeed');
		$group_arr=['months'=>'按月统计','weeks'=>'按周统计','days'=>'按天统计','shopid'=>'分店统计','admin'=>'员工统计'];
		$cell=array(array('nickname','粉丝'), array('shopid','分店'), array('product','产品'),array('service','服务'),array('target','对象'),array('admin','服务员'),array('content','留言'));
	   
	    $my = new MyPage();
	    $my ->title("粉丝反馈") ->buttonNew()->buttonNew(url('',['xls'=>1]),'导出');
	    if (isset($data['group'])){
	    	switch ($data['group']) {
	    		case 'shopid':
	    			$my ->keyJoin($data['group'],$group_arr[$data['group']],'id', 'title', 'admin_shop');
	    			break;
	    	    case 'admin':
	    			$my ->keyJoin($data['group'],$group_arr[$data['group']],'id', 'name', 'admin_user');
	    			break;
	    		
	    		default:
	    			$my ->keyText($data['group'],$group_arr[$data['group']]);
	    			break;
	    	}
	    	$my->keyText('id', '反馈数')
	    	->echart('统计图表','小标题',['反馈'=>'id'],$data['group']);
	    }else{
	   
		$my ->keyId()
		    ->keyText('nickname', '粉丝')
		    ->keyJoin('shopid', '分店', 'id', 'title', 'admin_shop')
		    ->keyMap('product', '产品', $this->rank)
		    ->keyMap('service', '服务', $this->rank)
		    ->keyText('target', '对象')
		    ->keyJoin('admin', '服务员', 'id', 'name', 'admin_user')
		    ->keyText('content', '留言')
		    ->keyCreateTime()
		   
		    ->actionEdit()
		    ->actionStatus('WeFeed');
		}
		$my ->mapLike('content|nickname', '名称')
		    ->mapCreateTime()
		    ->mapSelect('shopid', '分店',$shops)
		    ->mapSelect('product', '产品',$this->rank)
		    ->mapSelect('service', '服务',$this->rank)
		    ->mapGroup('统计',['month'=>'按月统计','week'=>'按周统计','day'=>'按天统计','admin'=>'合并服务员','shopid'=>'合并分店'],'id','');
		    
    		
	        return $my->data($data)->xls('粉丝反馈表',$cell)->fetch();
	    
    }


    public function feedEdit($id=0) {
		if ( request()->isPost() ) { 
			$res = model('We/WeFeed')->editData();
			if ($res>0) $this->success('更新成功','feed');
			$this->error('更新失败');
		} else {

			$shops=my_select('AdminShop');
			
			$data=my_edit('we/WeFeed',$id);
			$my = new MyConfig();
			$my->title("编辑反馈")->keyId()
					    
					    ->keySelect('shopid','分店',$shops)
					    ->keySelect('product','产品',$this->rank)
					    ->keySelect('service','服务',$this->rank)
					    ->keyText('target', '对象')
					    ->keyText('content', '留言');
			
			$my ->buttonSubmit()->buttonBack()
                ->data($data);
            return $my->data($data)->fetch();
		}
	}

	// 场景二维码开始
	public function card($appid='') {
		$my = new MyPage();
	    $wes=my_select('we','appid','title');
	    $my ->title("微信卡券")
		  
		// if ($appid) $my   ->buttonNew(url('getCards',['appid'=>$appid]))
		->buttonTrash('WeCard')
	    ->buttonRestore('WeCard')
	    ->buttonEnable('WeCard')
	    ->buttonDisable('WeCard');

		   
		$my ->keyId()
		    ->keyText('title', '券名')
		    ->keyText('sub_title', '副标题')
		    // ->keyText('card_id', 'card_id')
		    ->keyJoin('appid', '微信', 'appid', 'title', 'we')
		    
		    ->keyText('quantity', '库存')
		    // ->keyText('total_quantity', '总数')
		    ->keyText('can_share', '分享')
		    // ->keyText('type', 'type')
		    // ->keyTime('begin_timestamp', '开始')
		    // ->keyTime('end_timestamp', '结束')
		    ->keyText('fixed_term', '有效天数')
		    ->keyText('fixed_begin_term', '领取后生效')
		    
		    // ->keyText('sort', '排序')
		    // ->keyText('mark', '备注')
		    
		    ->keyCreateTime()
		   
		    ->actionEdit()
		    ->actionStatus('WeCard')
		    ->map('title', '名称' )
		    ->map('appid', '微信', 'select',$wes)
		    ->mapStatus();

    
    		$data=my_data('WeCard');
	       return $my->data($data)->fetch();
	    
    }

    public function cardEdit($id=0) {
		if ( request()->isPost() ) { 
			$res = model('We/WeCard')->editData();
			if ($res>0) $this->success('更新成功','card');
			$this->error('更新失败');
		} else {

			$shops=my_select('AdminShop');
			
			$data=my_edit('we/WeCard',$id);
			$my = new MyConfig();
			$my->title("编辑反馈")->keyId()
					    
			->keyText('title', '券名')
		    ->keyText('sub_title', '副标题')
		    
		   
		    ->keyText('sort', '排序')
		    ->keyText('mark', '备注');
			
			$my ->buttonSubmit()->buttonBack()
                ->data($data);
            return $my->data($data)->fetch();
		}
	}


    public function getCards($appid) {
    	
    	
    	$cards=get_cards($appid);
    	
    	foreach ($cards as $key => $card_id) {
    		$card_info=get_card($appid,$card_id);
           
    		//如果
    		$card_type=$card_info['card']['card_type'];
            $card=$card_info['card'][strtolower($card_type)];

            if ($card['base_info']['sku']['quantity']==0) continue;

           
            $data=[];
    		$data['appid']=$appid;
    		$data['card_id']=$card_id;
    		$data['card_type']=$card_type;
    		$data['title']=$card['base_info']['title'];
    		$data['sub_title']=$card['base_info']['sub_title'];
    		$data['quantity']=$card['base_info']['sku']['quantity'];
    		$data['total_quantity']=$card['base_info']['sku']['total_quantity'];
    		if (isset($card['base_info']['can_share']))$data['can_share']=$card['base_info']['can_share'];
    		if (isset($card['base_info']['get_limit']))$data['get_limit']=$card['base_info']['get_limit'];
    		if (isset($card['base_info']['can_give_friend']))$data['can_give_friend']=$card['base_info']['can_give_friend'];
    		if (isset($card['base_info']['date_info']['type']))$data['type']=$card['base_info']['date_info']['type'];
    		if (isset($card['base_info']['date_info']['begin_timestamp']))$data['begin_timestamp']=$card['base_info']['date_info']['begin_timestamp'];
    		if (isset($card['base_info']['date_info']['end_timestamp'])){
    			$data['end_timestamp']=$card['base_info']['date_info']['end_timestamp'];
    			if ($data['end_timestamp']<time()) continue;
    		}

    		if (isset($card['base_info']['date_info']['fixed_term']))$data['fixed_term']=$card['base_info']['date_info']['fixed_term'];
    		if (isset($card['base_info']['date_info']['fixed_begin_term']))$data['fixed_begin_term']=$card['base_info']['date_info']['fixed_begin_term'];
    		
    		
    		$res=model('we/weCard')->updateCard($data);

    	}

    	$this->success('成功','card');



	}

	public function pay() {
		$wes=my_select('we','appid','title');
		$shops=my_select('admin_shop');
		$data=my_data('WePay');
		$group_arr=['months'=>'按月统计','weeks'=>'按周统计','days'=>'按天统计','shopid'=>'分店统计'];
		$cell=array(array('nickname','粉丝'), array('shopid','分店'), array('product','产品'),array('service','服务'),array('target','对象'),array('admin','服务员'),array('content','留言'));
	   
	    $my = new MyPage();
	    $my ->title("微信支付") ->buttonNew()->buttonXls();
	    if (isset($data['group'])){
	    	switch ($data['group']) {
	    		case 'shopid':
	    			$my ->keyJoin($data['group'],$group_arr[$data['group']],'id', 'title', 'admin_shop');
	    			break;
	    	    
	    		default:
	    			$my ->keyText($data['group'],$group_arr[$data['group']]);
	    			break;
	    	}
	    	$my->keyText('um', '数量')
	    	   ->keyText('price', '金额')
	    	   ->echart('统计图表','小标题',['金额'=>'price'],$data['group']);
	    }else{
	   
		$my ->keyId()
		    ->keyJoin('openid', '粉丝', 'openid', 'nickname', 'we_fans')
		    // ->keyJoin('shopid', '分店', 'id', 'title', 'admin_shop')
		    ->keyJoin('appid', '微信', 'appid', 'title', 'we')
		    // ->keyText('out_trade_no', '订单号')
		    ->keyText('product', '产品')
		    // ->keyText('description', '描述')
		    ->keyText('um', '数量')
		    ->keyText('price', '价格')
		    ->keyCreateTime()
		   
		    ->actionEdit()
		    ->actionStatus('WePay');
		}
		$my ->mapLike('nickname', '名称')
		    ->mapCreateTime()
		    ->mapSelect('shopid', '分店',$shops)
		     ->mapGroup('统计',$group_arr,'id','price');
		    
    		
	        return $my->data($data)->xls('粉丝反馈表',$cell)->fetch();
	    
    }

	// 场景二维码开始
	public function message() {
		$my = new MyPage();
	    $wes=my_select('we','appid','title');
	    $my ->title("微信消息")   
		// if ($appid) $my   ->buttonNew(url('getCards',['appid'=>$appid]))
		->buttonTrash('WeMessage')
	    ->buttonRestore('WeMessage')
	    ->buttonEnable('WeMessage')
	    ->buttonDisable('WeMessage');

		   
		$my ->keyId()
		    ->keyText('nickname', '昵称')
		    ->keyText('ToUserName', 'ToUserName')
		    ->keyText('FromUserName', 'FromUserName')
		    ->keyText('MsgType', 'MsgType')
		    ->keyText('MsgId', 'MsgId')
		    ->keyText('Content', 'Content')
		    ->keyText('MediaId', 'MediaId')
		    ->keyText('Event', 'Event')
		    ->keyText('EventKey', 'EventKey')
		   
		    ->keyCreateTime()
		    ->actionEdit()
		    ->actionStatus('WeMessage')
		    ->mapLike('Content', '内容' )
		    ->mapLike('FromUserName', 'FromUserName' )
		    ->mapSelect('appid', '微信',$wes)
		    ->mapStatus();

    	
    		$data=my_data('WeMessage');
	        return $my->data($data)->fetch();
	    
    }


    public function config($id=0) {
		
		    $my = new MyConfig();
			$data = $my->handleConfig();
			$my->title("模块配置")->keyText('FEED_ROB', '反馈机器人')->keyText('FEED_ROB2', '反馈机器人2');
		    $my ->buttonSubmit()->buttonBack();
            return $my->data($data)->fetch();
		
	}




	
}