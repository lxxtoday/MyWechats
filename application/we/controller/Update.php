<?php
namespace app\we\controller;
use think\Controller;
use think\helper\Time;


class Update extends Controller{


    public function index_sorce_amount(){
      ignore_user_abort (true);
      set_time_limit(0);
     
      
      db('OldWechatMember')->chunk(1000, function($olds) {
      $table=db('WeFansCfo');
      foreach ($olds as $value) {
       if ($value['mobile'])$have=$table->where('mobile',$value['mobile'])->where('amount',$value['amount'])->where('score',$value['score'])->find();
       if ($value['openid'])$have=$table->where('openid',$value['openid'])->where('amount',$value['amount'])->where('score',$value['score'])->find();
      
       if ($have){
              //看看是否操作数据库
              if ($have['amount']!=$value['amount'] or $have['score']!=$value['score']){
                $data['amount']=$value['amount'];
                $data['score']=$value['score'];
                $res= $table->whereOr('mobile',$value['mobile'])->whereOr('openid',$value['openid'])->update($data);
                echo $value['nickname'].'更新</br>';
              }else{
                echo $value['nickname'].'跳过</br>';
              }
               
            }else{
              $data['aid']=2;
              $data['openid']=$value['openid'];
              $data['mobile']=$value['mobile'];
              $data['amount']=$value['amount'];
              $data['score']=$value['score'];
              $data['status']=1;
              $data['create_time']=time();
             
              $res=$table->insert($data);
              echo $value['nickname'].'插入</br>';
       }
       ob_flush(); 
       flush();
      
       mylog('处理1000'); 
       }
      });
     
      
    }


    public function update_score_log(){
      ignore_user_abort (true);
      set_time_limit(0);
     
      
      db('we_score_log')->chunk(1000, function($olds) {
      $table=db('WeFansCfoLog');
      foreach ($olds as $value) {
       $map['openid']=$value['openid'];
       $map['create_time']=$value['create_time'];
       $have=$table->where($map)->find();
      
       if ($have){
              
                echo $value['nickname'].'跳过</br>';
             
               
            }else{
              $data['aid']=2;
              $data['openid']=$value['openid'];
              $data['type']=1;
              $data['pay_type']=$value['pay_type'];
              $data['shopid']=$value['shopid'];
              $data['status']=1;
              $data['um']=$value['um'];
              $data['description']=$value['disc'];
              $data['balance']=$value['balance'];
              $data['create_time']=$value['create_time'];
             
              $res=$table->insert($data);
              echo $value['openid'].'插入</br>';
       }
       ob_flush(); 
       flush();
      
       mylog('处理1000'); 
       }
      });
     
      
    }

   
}