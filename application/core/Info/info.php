<?php
return array(
    'name' => 'Core',
    'alias' => '系统公共模块',
    'version' => '2.1.0',
    'is_com' => 0,
    'show_nav' => 0,
    'summary' => '系统核心模块，必不可少，负责核心的处理。',
    'developer' => '嘉兴想天信息科技有限公司',
    'icon' => 'globe',
    'website' => 'http://www.ourstu.com'
);