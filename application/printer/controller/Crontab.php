<?php
namespace app\printer\controller;
use think\Controller;

class Crontab extends Controller{

    public function index($aid){
      $printers=model('printer/Printer')->listByMap(['aid'=>$aid]);
      $pics=db('printer/PrinterPay')->where('aid',$aid)->where('status',2)->count();
      $this->assign('printers',$printers);
      $this->assign('aid',$aid);
      return $this->fetch();
     
    }

}