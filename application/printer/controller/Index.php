<?php
namespace app\printer\controller;
use think\Controller;

class Index extends Controller{


    
    public function index($shopid=0){

     
        $online=cache('printer_online_'.$shopid);
        if ($online==1) {
        dump('已经开启了一个监控，系统自我保护，关闭本次监控,请30秒后刷新重试');
        die; 
        }

       $printers=db('printer')->where(['shopid'=>$shopid])->select();
       foreach ($printers as $key => &$printer) {
        $printer['qrcode']="/".get_cover($printer['qrcode'], 'path');
       }
       $this->assign('printers',$printers);
       $this->assign('shopid',$shopid);
       return $this->fetch();

    }

    public function pay($openid='',$shopid='',$pay=''){
      $fans=model('we/WeFans')->info($openid);
      if(request()->isPost()){
      $pay=[
              'aid'=>$fans['aid'],
              'shopid'=>$shopid,
              'appid'=>$fans['appid'],
              'openid'=>$fans['openid'],
              'product'=>'微信打印',
              'description'=>'微信零钱打印女神美照',
              'price'=>$pay,
              'um'=>$pay*100,
              'url'=>'printer/PrinterFans',
              'data_id'=>$openid
              ];
        session('pay',$pay);
        $this->success('即将进入微信支付',url('we/pay/jssdk'));
      }else{
       
       
        $this->assign('openid',$openid);
        $this->assign('shopid',$shopid);
        $this->assign('fans',$fans);
        return $this->fetch();
      }
      
    }


    public function getPics($shopid=0){
       
       Cache('printer_online_'.$shopid,1,60); //心跳记录
       $pics=model('PrinterPic')->getUnsuccess($shopid);
       
       foreach ($pics as $key => &$pic) {
         $pic['url']='/'.$pic['url'];
       }
       return json($pics);

    }

     public function printStatus($id=0,$status=1,$P_ID){
      
      $map['id']=$id;
      $data['status']=$status;
      $data['P_ID']=$P_ID;
      $data['update_time']=time();
      $db=db('PrinterPic');
      $res= $db->where($map)->update($data);
     

      //发送通知
      $pic=db('PrinterPic')->where($map)->field('openid,printer,aid')->find();

      if ($status==1){
        // $reply=[
        //     'template_id'=>'LsIc21raK3kWuX8j8hgBwJ-1cWn35PIdxgz2KMxgMPQ',
        //     'url'=>'http://baidu.com',
        //     'data'=>[
        //     'first'=>['value'=>'进入打印机'],
        //     'keyword1'=>['value'=>'你好'],
        //     'remark'=>['value'=>'你好']
        //     ]
        //     ];
        // templateMessage($pic['openid'],$reply);
        $reply=['msgtype'=>'text','text'=>['content'=>'进入打印机']];
        custom_message($pic['openid'],$reply);
        // send($pic['openid'],$reply,'customMessage');
      }

      if ($status==2){
        // $reply=[
        //     'template_id'=>'LsIc21raK3kWuX8j8hgBwJ-1cWn35PIdxgz2KMxgMPQ',
        //     'url'=>'http://baidu.com',
        //     'data'=>[
        //     'first'=>['value'=>'成功打印'],
        //     'keyword1'=>['value'=>'你好'],
        //     'remark'=>['value'=>'你好']
        //     ]
        //     ];
        // templateMessage($pic['openid'],$reply);
        $reply=['msgtype'=>'text','text'=>['content'=>'打印成功']];
        custom_message($pic['openid'],$reply);
        // send($pic['openid'],$reply,'customMessage');

      }

      
    
     

    }

}