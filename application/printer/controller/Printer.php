<?php
namespace app\admin\controller;

use app\admin\my\MyPage;
use app\admin\my\MyConfig;

class Printer extends Base {
   
	private $we_type = [ '订阅号', '认证订阅号', '服务号','认证服务号' ];
    private $sex = ['未知','男','女'];
    private $qrcode_type=['临时二维码','数值型永久二维码','字符型永久二维码'];

    public function printer() {
		$my = new MyPage();
	    $my ->title("打印机管理")
		    ->buttonNew("printerEdit")
		    ->keyId()
		    ->keyLink('title', '名称','printer/index/index?shopid={$shopid}','new')
		    ->keyJoin('appid', '微信', 'appid', 'title', 'We')
		    ->keyJoin('shopid', '分店', 'id', 'title', 'AdminShop')
		    
		    ->keyText('appid', '免费')
		    ->keyText('word_ad', '文字广告')
		    ->keyText('online', '在线')
		    ->keyUpdateTime()
		    ->keyMap('qrcode_ad','二维码广告',['关闭','开启'])

		    ->actionEdit()
		    ->actionStatus('printer')
		    ->map('title', '打印机');
		    $data=my_data('Printer');
		    foreach ($data['rows'] as $key => &$value) {
		     $value['online']=(cache('printer_online_'.$value['shopid']))?'在线' :'-';
		    }
    	    return $my->data($data)->fetch();
	    
    }



    public function printerEdit($id=0) {
		

		if ( request()->isPost() ) { 
			$res = model('printer/printer')->editData();
			if ($res>0) $this->success('更新成功','printer');
			$this->error('更新失败');
		} else {
			$shops=my_select('AdminShop');
			$Wes=my_select('We','appid','title');
			$data=my_edit('printer/printer',$id);

			$my = new MyConfig();
			return  $my->title("编辑打印机")->keyId()
					    ->keyText('title', '名称')
					    ->keySelect('shopid','分店',$shops)
					    ->keySelect('appid','微信',$Wes)
					    ->keyImage('qrcode', '二维码')
					    ->keyText('free', '免费')
					   
					    ->keyBool('qrcode_ad', '二维码广告','这个二维码会出现在顾客的照片上')
					    ->keyText('word_ad', '文字广告','这个文字广告会出现在顾客的照片上,留空则不显示')
					    ->keyText('printing_ad', '打印中广告')
					    ->keyText('success_ad', '成功广告')
					    ->keyText('pay_ad', '支付广告')

					    ->keyBool('pay','微信支付','开通微信支付顾客将可以自助打印，满100后提现')

					    ->group( '基本', 'fa-cog', 'id,title,shopid,appid,qrcode')
					    ->group( '广告', 'fa-wechat', 'qrcode_ad,word_ad,printing_ad,success_ad,pay_ad')
					    ->group( '设置', 'fa-wechat', 'free,pay')
					    ->buttonSubmit()->buttonBack()
                        ->data($data)->fetch();
		}
	}

	 public function fans() {
		$my = new MyPage();
	   
	    $my ->title("打印会员管理")
		    ->buttonNew("printerEdit")
		    ->keyId()
		    ->keyText('nickname', '昵称')
		    ->keyText('balance', '余额')
		    ->keyText('times', '打印次数')
		    ->keyText('pay', '支付次数')
		   
		    ->actionEdit()
		    ->actionStatus('PrinterFans')
		    ->map('nickname', '名称')
		   
		    ->mapStatus();

    	
    		$data=my_data('PrinterFans');
	        return $my->data($data)->fetch();
	    
    }



    public function fansEdit($id=0) {
		
		if ( request()->isPost() ) { 
			$res = model('printer/PrinterFans')->editData();
			if ($res>0) $this->success('更新成功','fans');
			$this->error('更新失败');
		} else {
			$data=my_edit('printer/PrinterFans',$id);
			$my = new MyConfig();
			return  $my->title("编辑会员")->keyId()
					    ->keyText('nickname', '昵称')
					    ->keyText('balance', '余额')
					    ->buttonSubmit()->buttonBack()
                        ->data($data)->fetch();
		}
	}


	public function pic() {
		
		$wes=my_select('we','appid','title');
		$shops=my_select('admin_shop');
		$group_arr=['months'=>'按月统计','weeks'=>'按周统计','days'=>'按天统计','shopid'=>'分店统计','style'=>'照片类型'];
		$data=my_data('PrinterPic');
		$my = new MyPage();
	    $my ->title("打印记录") ->buttonNew("picEdit");
		if (isset($data['group'])){
			switch ($data['group']) {
				case 'shopid':
					$my ->keyJoin($data['group'],$group_arr[$data['group']],'id', 'title', 'admin_shop');
					break;
				case 'shopid':
					$my ->keyMap($data['group'],$group_arr[$data['group']],[0=>'竖片',1=>'横片']);
					break;
				
				default:
					 $my ->keyText($data['group'],$group_arr[$data['group']]);
					break;
			}
	    	
        $my ->keyText('id', '照片数量')
            ->echart('统计图表','小标题',['照片数量'=>'id'],$data['group']);
	    }else{   
		$my ->keyId()
		    ->keyText('nickname', '昵称')
		    ->keyJoin('shopid', '分店', 'id', 'title', 'admin_shop')
		    ->keyJoin('printer', '打印机', 'id', 'title', 'printer')
		    
		    // ->keyText('url', '图片')
		    ->keyText('width', 'width')
		    ->keyText('height', 'height')
		    ->keyText('type', 'type')
		    ->keyText('P_ID', 'P_ID')
		    ->keyMap('style', '类型',[0=>'竖片',1=>'横片'])
		    ->actionEdit()
		    ->actionStatus('PrinterPic');
		}
		$my ->map('title', '名称')
		    ->mapSelect('appid', '微信',  $wes)
		    ->mapSelect('shopid', '分店',$shops)
		    ->mapGroup('统计',$group_arr,'id','');
        return $my->data($data)->fetch();
	   
    }



	public function pay() {
		$my = new MyPage();
	   
	    $my ->title("打印机管理")
		   
		    ->buttonNew("payEdit")
		   
		    ->keyId()
		    ->keyText('nickname', '昵称')
		    ->keyJoin('shopid', '分店', 'id', 'title', 'admin_shop')
		    ->keyJoin('appid', '微信', 'appid', 'title', 'we')
		    
		    ->keyText('um', '充值')
		    ->keyText('price', '充值金额')
		    ->keyText('balance', '余额')
		   
		    ->keyMap('pay_type','方式',['指令','微信支付'])


		   
		    ->actionEdit()
		    ->actionStatus('PrinterPay')
		    ->map('nickname', '昵称')
		    ->mapStatus();

    
    		 $data=my_data('PrinterPay');
	          return $my->data($data)->fetch();
	  
    }


}