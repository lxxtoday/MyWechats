<?php
// +----------------------------------------------------------------------
// | my
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://isofttime.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: $this <498944516@qq.com>
// +----------------------------------------------------------------------
namespace app\printer\model;

use think\Model;
use app\we\sdk\Thinkwechat;


class PrinterFans extends Model
{
    
    // 根据openid 获取用户
    public function info($key, $field = true)
    {
        
        if (!$key) return false;
        if (is_numeric($key)){
            $map['id'] = $key;
        }else{
            $map['openid'] = $key;
        }

        $fans=$this->field($field)->where($map)->find();

        return $fans;
    }

    

    // 更新用户
    public function checkFans($openid,$printer)
    {
       
        if (!$printer['free'])$printer['free']=1;
        $cur_date = strtotime(date('Y-m-d',time()));
        $map['openid']=$openid;
        $fans = $this->where($map)->find();
       
        if (!$fans){
            $fans = model('we/WeFans')->info($openid);
       
            $data['openid']=$openid;
            $data['nickname']=$fans['nickname'];
            $data['balance']=$printer['free'];
            $data['aid']=$fans['aid'];
            $data['printer']=$printer['id'];
            $data['update_time']=time();
          
            $this->allowField(true)->data($data)->save();
          

        }elseif($fans['update_time']<$cur_date){
            //自动赠送一张
            $this->where($map)->setInc('balance',$printer['free']);
            $this->where($map)->setField('update_time',time());
          

        }
        

        $balance=$this->where($map)->value('balance');  //TODO::可以增加黑名单功能，被限制的用户永远返回0
        
        if ($balance>=1){
           $res= $this->where($map)->setInc('times');
           $res= $this->where($map)->setDec('balance');
        }

         return $balance-1; 
        
    }

     public function editData(){

      $data=input("post.");
      
      if ($data['id']){
         $res = $this->allowField(true)->where('id',$data['id'])->update($data);
      }else{
         $res = $this->allowField(true)->data($data)->save();
      }
      
      return $res;
    }

    public function pay($openid,$um){
      $map['openid']=$openid;
      
      $res=$this->where($map)->setInc('balance',$um);
      $this->where($map)->setField('update_time',time());

      //记录日志
      
      $message=['msgtype'=>'text','text'=>['content'=>'管理员为你充值微信打印'.$um.'元，请查收']];
      if ($res>0) custom_message($openid,$message);

       // $reply=[
       //      'template_id'=>'LsIc21raK3kWuX8j8hgBwJ-1cWn35PIdxgz2KMxgMPQ',
       //      'url'=>'http://baidu.com',
       //      'data'=>[
       //      'first'=>['value'=>'你好'],
       //      'keyword1'=>['value'=>'你好'],
       //      'remark'=>['value'=>'你好']
       //      ]
       //      ];
       // templateMessage('od6detwtpzfxWSF4pQJPxiufz3lw',$reply);
    
      return $res;
    }

    public function paySuccess(){
       //标记打赏了多少钱
       
       $pay=session('pay');
       $fans=model('we/WeFans')->info($pay['data_id']);
       $data['nickname']='微信支付';
       $data['before']=$fans['id'];
       $data['back']=$pay['um'];
       $data['fans']['admin_info']['shopid']=$pay['shopid'];
       mylog($data);
       $res=$this->payByOrder($data);
       mylog($res);

    }

    public function payByOrder($data){
    
       $fans = model('we/WeFans')->info($data['before']);
       if (!$fans) return ['message'=>'粉丝不存在，请核对粉丝ID'];
       if (!is_numeric($data['back'])) return ['message'=>'充值数量输入不合法'];
       $map['openid']=$fans['openid'];
       $res=$this->where($map)->setInc('balance',$data['back']);
       $this->where($map)->setField('update_time',time());
       //记录
       if($res){
         $log['aid']=$fans['aid'];
         $log['appid']=$fans['appid'];
         $log['shopid']=$data['fans']['admin_info']['shopid'];
         $log['openid']=$fans['openid'];
         $log['um']=$data['back'];
         $printer_fans=$this->info($fans['openid']);
         $log['balance']=$printer_fans['balance'];
         $log_res=db('PrinterPay')->insert($log);
         $message=['msgtype'=>'text','text'=>['content'=>'为你充值微信打印'.$data['back'].'元，请查收']];
         if ($res>0) custom_message($fans['openid'],$message);
         return ['message'=>'您为'.$fans['nickname'].'充值照片'.$data['back'].'张'];
       }else{
         return ['message'=>'该会员还未使用过打印服务，不能充值'];
       }
       
    }




   
}