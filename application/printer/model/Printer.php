<?php
// +----------------------------------------------------------------------
// | my
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://baiyf.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: NickBai <1902822973@qq.com>
// +----------------------------------------------------------------------
namespace app\Printer\model;

use think\Model;

class Printer extends Model
{

    protected $table = "my_printer";
    protected $autoWriteTimestamp = true;

    /**
     * 获取打印机，并且缓存
     */
    public function info($printer_id){

      $options = $this->where('id',$appid)-> cache(true,60)->find();
      
      return $options;
    }

    public function infoByMap($map,$field='*'){

      $printers = $this->field($field)->where($map)-> cache(true,60)->find();
      
      return $options;
    }

    public function listByMap($map,$field='*'){
      $map['status']=['EGT',0];
      $printers = $this->field($field)->where($map)-> cache(true,60)->select();
      
      return $printers;
    }

    public function addTotal($id){

      $this->where('id',$id)->setInc('total');
      $this->where('id',$id)->setField('update_time',time());
      
      
    }

    /**
     * 检查符合要求的打印机
     * 
     */
    public function checkPrinter($appid,$openid){

        $printers=$this->where('appid',$appid)->select();  
        if (!$printers) return false;
        if (count($printers)==1) return  $printers[0];
        if (count($printers)>1) $scan=model('we/WeSceneScan')->getLastScan($openid);  //获取这个用户最后扫描的二维码
        foreach ($printers as $key => $printer) {
        	if ($scan['shopid']==$printer['shopid']) return $printer;
        }
        
    }

     public function editData(){

      $data=input("post.");
      $data['aid']=session('aid');
      if(isset($data['file'])) unset($data['file']);
      
      if ($data['id']){
         $res = $this->allowField(true)->where('id',$data['id'])->update($data);
      }else{
         $res = $this->allowField(true)->data($data)->save();
      }
      
      return $res;
    }


    
}