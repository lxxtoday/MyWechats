<?php
// +----------------------------------------------------------------------
// | my
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://baiyf.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: $this <498944516@qq.com>
// +----------------------------------------------------------------------
namespace app\printer\model;

use think\Model;

class WeApp extends Model
{

    protected $status=['待打印','打印机中','打印成功'];
    /**
     * 获取机器人
     */
    public function index($data=''){
      
      

      $this->data=$data;

       switch ($data['keyword']) {
         
          case 'P':
            $reply= $this->P();
            break;
          
          default:
            # code...
            break;
        } 



       return $reply;
      
    }

   
    public function P() {
      
      if ($this->data['before']=='image' and !$this->data['back']) return model('printer/PrinterPic')->addPic($this->data);
        //下面的命令需要验证权限
      $admin=model('we/WeFans')->checkAdmin($this->data['openid']);
      if($admin['status']==0) return $admin['text'];

      if (!$this->data['before'] and !$this->data['back']) return $this->order();
      
      if ($this->data['before']=='列表' and !$this->data['back']) return model('printer/PrinterPic')->weList($this->data);
      if ($this->data['before']=='再打' and  $this->data['back']) return model('printer/PrinterPic')->again($this->data);
      if ($this->data['before'] and  $this->data['back']) return model('printer/PrinterFans')->payByOrder($this->data);
      
     

    }

    public function order() {
         $printers=db('printer')->where('shopid',$this->data['fans']['admin_info']['shopid'])->select();
          
         $message='P-打印机(Printer)诊断工具';
         $online='';
        
        foreach ($printers as $key => &$printer) {
            if (cache('printer_online_'.$printer['shopid'])==1){
             $online='-在线/:rose';
            }else{
              $online="-离线/:fade"; 
              $shopid=$printer['shopid'];
            }
            $message.="\n".$printer['title'].$online;
         }

         if (isset($shopid)){
           $shop=model('admin/AdminShop')->info($printer['shopid'],'phone');
           $message.="\n请在打印机电脑打开".$_SERVER['HTTP_HOST']."/printer/?shopid=".$printer['shopid']; 
         }

         $message.="\n-------------";
         $message.="\n/:hug1.充值：\n1P2 (给1号粉丝充值2张)";
         $message.="\n/:hug2.查询：\n列表P (查询当前打印列表)";
         $message.="\n/:hug3.再打：\n再打P10（再打10号照片）";
         return $message;
     }

    
}