<?php
namespace app\admin\model;

use think\Model;

// 文件
class File extends Model {
    // protected $_auto = ['']; // 自动完成 - 新增更新
    protected $_insert = ['create_time']; // 自动完成 - 新增
    // protected $_update = ['']; // 自动完成 - 更新

    protected $_map = ['type' => 'mime'];

    // 设置创建时间
    protected function setCreateTimeAttr() {
    	return time();
    }

    public function download($root, $id, $callback=null, $args=null) {
        $file = $this->find($id);
        if(!$file){
            $this->error = L('_NO_THIS_FILE_IS_NOT_THERE_WITH_EXCLAMATION_');
            return false;
        }

        if($file['driver'] == 'local'){
            $file['rootpath'] = $root;
            return $this->downLocalFile($file, $callback, $args);
        }else{
            redirect($file['savepath']);
        }

    }
    public function isFile($file) {
        if ( empty($file['md5']) ) throw new \Exception('缺少参数:md5');

        $map = array(
        	'md5' => $file['md5'],
        	'sha1'=>$file['sha1'],
        );
        return $this->field(true)->where($map)->find();
    }
	// 下载本地文件
    private function downLocalFile($file, $callback=null, $args=null) {
        $path = $file['rootpath'].$file['savepath'].$file['savename'];
        if(is_file($path)){
            is_callable($callback) && call_user_func($callback, $args);

            // 执行下载 大文件断点续传
            header("Content-Description: File Transfer");
            header('Content-type: ' . $file['type']);
            header('Content-Length:' . $file['size']);
            if (preg_match('/MSIE/', $_SERVER['HTTP_USER_AGENT']) ) { //for IE
                header('Content-Disposition: attachment; filename="' . rawurlencode($file['name']) . '"');
            } else {
                header('Content-Disposition: attachment; filename="' . $file['name'] . '"');
            }
            readfile($path);
            exit;
        } else {
            $this->error = L('_FILE_HAS_BEEN_DELETED_WITH_EXCLAMATION_');
            return false;
        }
    }
	// 下载ftp文件
	private function downFtpFile($file, $callback=null, $args=null) {
		is_callable($callback) && call_user_func($callback, $args);

		$host = C('DOWNLOAD_HOST.host');
		$root = explode('/', $file['rootpath']);
		$file['savepath'] = $root[3].'/'.$file['savepath'];

		$data = array($file['savepath'], $file['savename'], $file['name'], $file['mime']);
		$data = json_encode($data);
		$key = think_encrypt($data, C('DATA_AUTH_KEY'), 600);

		header("Location:http://{$host}/onethink.php?key={$key}");
	}
	// 清除数据库存在但本地不存在的数据
	public function removeTrash($data) {
		$this->where(array('id'=>$data['id'],))->delete();
	}
}
// // 单文件上传
// $file = request()->file($name);
// $info = $file->move(ROOT_PATH . 'public' . DS . 'uploads');
// if ( $info ) {
// 	echo $info->getExtension();
// 	echo $info->getSaveName();
// 	echo $info->getFilename(); 
// } else {
// 	echo ->getError();
// }

// // 多文件上传
// $files = request()->file($name);
// foreach ( $files as $file ) {
// 	$info = $file->move(ROOT_PATH . 'public' . DS . 'uploads');
// 	if ( $info ) {
// 		echo $info->getExtension();
// 		echo $info->getSaveName();
// 		echo $info->getFilename();
// 	} else {
// 		echo $file->getError();
// 	}
// }