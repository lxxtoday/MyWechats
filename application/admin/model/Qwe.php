<?php
// +----------------------------------------------------------------------
// | my
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://baiyf.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: NickBai <1902822973@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\model;

use think\Model;

class Qwe extends Model
{

    protected $table = "my_qwe";

    /**
     * 获取微信配置信息，需要缓存
     */
    public function info($appid){
       if (is_numeric($appid)){
        $map['id']=$appid;
      }else{
        $map['appid']=$appid;
      }

      $options = $this->where($map)->find();
      
      return $options;
    }


    public function editData(){

      $data=input("post.");
      if ( isset($data['file']) ) unset($data['file']);
      $data['aid']=session('aid');
      if ($data['id']){
         $res = $this->allowField(true)->where('id',$data['id'])->update($data);
      }else{
         $res = $this->allowField(true)->data($data)->save();
      }
      
      return $res;
    }

  
    

    
}