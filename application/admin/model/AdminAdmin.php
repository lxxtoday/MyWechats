<?php
// +----------------------------------------------------------------------
// | my
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://baiyf.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: NickBai <1902822973@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\model;

use think\Model;

class AdminAdmin extends Model
{

    protected $table = "my_admin_admin";
    protected $insert = ['status'=>1];  
    protected $autoWriteTimestamp = true;

     public function info($key, $field = true)
    {
        
        if (!$key) return false;
        if (is_numeric($key)){
            $map['id'] = $key;
        }else{
            $map['title'] = $key;
        }

        $data=$this->field($field)->where($map)->find();

        return $data;
    }


    public function editData(){

      $data=input("post.");
      $data['aid']=session('aid');
      if (isset($data['file'])) unset($data['file']);
     
      if ($data['id']){
         $res = $this->allowField(true)->where('id',$data['id'])->update($data);
      }else{
         $res = $this->allowField(true)->data($data)->save();
      }
      
      return $res;
    }

  
    

    
}