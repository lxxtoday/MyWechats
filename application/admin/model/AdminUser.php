<?php
// +----------------------------------------------------------------------
// | my
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://baiyf.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: NickBai <1902822973@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\model;

use think\Model;

class AdminUser extends Model
{
    protected $table = 'my_admin_user';
    protected $insert = ['status'=>1];  
    protected $autoWriteTimestamp = true;

    public function info($key, $field = true){
        
        if (!$key) return false;
        if (is_numeric($key)){
            $map['id|mobile'] = $key;  //todo 电话单独判断
        }else{
            $map['title'] = $key;
        }

        $data=$this->field($field)->where($map)->find();

        return $data;
    }

    public function infoByUserid($userid, $field = true){
        
        if (!$key) return false;
        $map['userid'] = $userid;
        $map['aid'] = session('aid');
        $data=$this->field($field)->where($map)->find();

        return $data;
    }

    /**
     * 根据搜索条件获取用户列表信息
     * @param $where
     * @param $offset
     * @param $limit
     */
    public function getUsersByWhere($where, $offset, $limit)
    {
        return $this->field('my_admin_user.*,rolename')
            ->join('my_admin_role', 'my_admin_user.role_id = my_admin_role.id')
            ->where($where)->limit($offset, $limit)->order('id desc')->select();
    }


    /**
     * 根据搜索条件获取所有的用户数量
     * @param $where
     */
    public function getAllUsers($where)
    {
        return $this->where($where)->count();
    }


    public function editData(){

      $data=input("post.");
      if (!isset($data['aid']))$data['aid']=session('aid');
      if ($data['id']){
         $res = $this->allowField(true)->where('id',$data['id'])->update($data);
      }else{
         $data['password']=md5(md5($data['mobile']) . config('data_auth_key'));
         $res = $this->allowField(true)->data($data)->save();
      }
      return $res;
    }


    /**
     * 插入管理员信息
     * @param $param
     */
    public function insertUser($param)
    {
        try{

            $result =  $this->validate('UserValidate')->save($param);
            if(false === $result){
                // 验证失败 输出错误信息
                return ['code' => -1, 'data' => '', 'msg' => $this->getError()];
            }else{

                return ['code' => 1, 'data' => '', 'msg' => '添加用户成功'];
            }
        }catch( PDOException $e){

            return ['code' => -2, 'data' => '', 'msg' => $e->getMessage()];
        }
    }

    /**
     * 编辑管理员信息
     * @param $param
     */
    public function editUser($param)
    {
        try{

            $result =  $this->validate('UserValidate')->save($param, ['id' => $param['id']]);

            if(false === $result){
                // 验证失败 输出错误信息
                return ['code' => 0, 'data' => '', 'msg' => $this->getError()];
            }else{

                return ['code' => 1, 'data' => '', 'msg' => '编辑用户成功'];
            }
        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }

    /**
     * 根据管理员id获取角色信息
     * @param $id
     */
    public function getOneUser($id)
    {
        return $this->where('id', $id)->find();
    }



    /**
     * 删除管理员
     * @param $id
     */
    public function delUser($id)
    {
        try{

            $this->where('id', $id)->delete();
            return ['code' => 1, 'data' => '', 'msg' => '删除管理员成功'];

        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }



    public function cfoAmount($admin,$um,$message){
           $map['id']=$admin['id'];
           $res= $this->where($map)->setInc('amount',$um);
        if ($res){
           $admin=$this->where($map)->find();
           $log['aid']=$admin['aid'];
           $log['shopid']=$admin['shopid'];
           $log['type']=0;
           $log['pay_type']=0;
           $log['user_id']=$admin['id'];
           $log['admin']=1;
           $log['um']=$um;
           $log['balance']=$admin['amount'];
           $log['description']='老顾客';
           $log_res=model('admin/AdminUserCfo')->editData($log); 
           // TODO::通知
       }
    }
}