<?php
// +----------------------------------------------------------------------
// | my
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://baiyf.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: NickBai <1902822973@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\model;

use think\Model;

class AdminUserCfo extends Model
{

    protected $insert = ['status'=>1];  
    /**
     * 获取微信配置信息，需要缓存
     */
    public function info($openid,$field=true){
      
      $map['openid']=$openid;
      $cfo = $this->field($field)->where($map)->find();
     
      return  $cfo;
    }

     

    public function editData($data){
      if (isset($data['id'])){
         $res = $this->allowField(true)->where('id',$data['id'])->update($data);
      }else{
         $res = $this->allowField(true)->data($data)->save();
      }
    
      return $res;
    }
    
}