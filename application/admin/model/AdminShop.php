<?php
// +----------------------------------------------------------------------
// | my
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://baiyf.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: NickBai <1902822973@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\model;

use think\Model;

class AdminShop extends Model
{

    
    protected $insert = ['status'=>1];  
  

    public function info($key, $field = true){
        
        if (!$key) return false;
        if (is_numeric($key)){
            $map['id'] = $key;
        }else{
            $map['title'] = $key;
        }

        $data=$this->field($field)->where($map)->cache(3600)->find();

        return $data;
    }

     public function getField($key, $field = 'title'){
        
        $shop=$this->info($key, $field);

        return $shop[$field];
    }


    public function editData(){

      $data=input("post.");
      $data['aid']=session('aid');
      if ($data['id']){
         $res = $this->allowField(true)->where('id',$data['id'])->update($data);
      }else{
         $res = $this->allowField(true)->data($data)->save();
      }
      
      return $res;
    }

    public function listByMap($map=[], $field = true){
      if(!$map){
        $map['aid']=session('aid');
      }
        $map['status']=['EGT',0];
        $list=$this->field($field)->where($map)->select();
        return $list;
    }

    public function listForSelect(){
        $list=$this->listByMap();
        $list =array_column($list, 'title', 'id');
        return $list;
    }

  
    

    
}