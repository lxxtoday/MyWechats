<?php
/**
 * 生成操作按钮
 * @param array $operate 操作按钮数组
 */
function showOperate($operate = [])
{
    if(empty($operate)){
        return '';
    }
    $option = <<<EOT
<div class="btn-group">
    <button class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
        操作 <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
EOT;

    foreach($operate as $key=>$vo){

        $option .= '<li><a href="'.$vo.'">'.$key.'</a></li>';
    }
    $option .= '</ul></div>';

    return $option;
}

/**
 * 将字符解析成数组
 * @param $str
 */
function parseParams($str)
{
    $arrParams = [];
    parse_str(html_entity_decode(urldecode($str)), $arrParams);
    return $arrParams;
}

/**
 * 子孙树 用于菜单整理
 * @param $param
 * @param int $pid
 */
function subTree($param, $pid = 0)
{
    static $res = [];
    foreach($param as $key=>$vo){

        if( $pid == $vo['pid'] ){
            $res[] = $vo;
            subTree($param, $vo['id']);
        }
    }

    return $res;
}

/**
 * 整理菜单住方法
 * @param $param
 * @return array
 */
function prepareMenu($param)
{
    
    $parent = []; //父类
    $child = [];  //子类

    foreach($param as $key=>$vo){
        $vo['active']=null;
        if (request()->controller()==$vo['control_name'] and request()->action()==$vo['action_name']){
            $vo['active']='active';
        }
        if($vo['pid'] == 0){
            $vo['href'] = '#';
            $parent[] = $vo;

        }else{
            $vo['href'] = url($vo['control_name'] .'/'. $vo['action_name']); 
            $child[] = $vo;   
        }
    }

    foreach($parent as $key=>$vo){
       
        foreach($child as $k=>$v){

            if($v['pid'] == $vo['id']){
                if (isset($v['active']) ){
                    $parent[$key]['active']=$v['group']?$v['group']:'active';  
                }
           
                if ($v['group']){
                    $parent[$key]['child'][$v['group']][] = $v;
                 }else{
                    $parent[$key]['child'][] = $v;
                }
                
            }
        }
    }
    
   
    return $parent;
}

/**
 * 解析备份sql文件
 * @param $file
 */
function analysisSql($file)
{
    // sql文件包含的sql语句数组
    $sqls = array ();
    $f = fopen ( $file, "rb" );
    // 创建表缓冲变量
    $create = '';
    while ( ! feof ( $f ) ) {
        // 读取每一行sql
        $line = fgets ( $f );
        // 如果包含空白行，则跳过
        if (trim ( $line ) == '') {
            continue;
        }
        // 如果结尾包含';'(即为一个完整的sql语句，这里是插入语句)，并且不包含'ENGINE='(即创建表的最后一句)，
        if (! preg_match ( '/;/', $line, $match ) || preg_match ( '/ENGINE=/', $line, $match )) {
            // 将本次sql语句与创建表sql连接存起来
            $create .= $line;
            // 如果包含了创建表的最后一句
            if (preg_match ( '/ENGINE=/', $create, $match )) {
                // 则将其合并到sql数组
                $sqls [] = $create;
                // 清空当前，准备下一个表的创建
                $create = '';
            }
            // 跳过本次
            continue;
        }

        $sqls [] = $line;
    }
    fclose ( $f );

    return $sqls;
}



// --- builder
function real_strip_tags($str, $allowable_tags="")
{
    // $str = html_entity_decode($str, ENT_QUOTES, 'UTF-8');
    return strip_tags($str, $allowable_tags);
}
// 过滤 输出没有html的干净的文本
function op_t($text, $addslanshes=false)
{
    $text = nl2br($text);
    $text = real_strip_tags($text);
    if ( $addslanshes ) $text = addslashes($text);
    $text = trim($text);
    return $text;
}
// 过滤
function text($text, $addslanshes=false)
{
    return op_t($text, $addslanshes);
}
// 过滤 不安全的html标签，输出安全的html
function op_h($text, $type='html')
{
    $text_tags = ''; // 无标签格式
    $link_tags = '<a>'; // 只保留链接
  	$image_tags = '<img>'; // 只保留图片
    $font_tags = '<i><b><u><s><em><strong><font><big><small><sup><sub><bdo><h1><h2><h3><h4><h5><h6>'; // 只存在字体样式
    $base_tags = $font_tags . '<p><br><hr><a><img><map><area><pre><code><q><blockquote><acronym><cite><ins><del><center><strike>'; // 标题摘要基本格式
    $form_tags = $base_tags . '<form><input><textarea><button><select><optgroup><option><label><fieldset><legend>'; // 兼容Form格式
    $html_tags = $base_tags . '<ul><ol><li><dl><dd><dt><table><caption><td><th><tr><thead><tbody><tfoot><col><colgroup><div><span><object><embed><param>'; // 内容等允许HTML的格式
    $all_tags = $form_tags . $html_tags . '<!DOCTYPE><meta><html><head><title><body><base><basefont><script><noscript><applet><object><param><style><frame><frameset><noframes><iframe>'; // 专题等全HTML格式
    $text = real_strip_tags($text, ${$type . '_tags'}); // 过滤标签

    if ( $type != 'all' ) { // 过滤攻击代码
        while ( preg_match('/(<[^><]+)(ondblclick|onclick|onload|onerror|unload|onmouseover|onmouseup|onmouseout|onmousedown|onkeydown|onkeypress|onkeyup|onblur|onchange|onfocus|action|background[^-]|codebase|dynsrc|lowsrc)([^><]*)/i', $text, $mat) ) { // 过滤危险的属性，如：过滤on事件lang js
            $text = str_ireplace($mat[0], $mat[1] . $mat[3], $text);
        }
        while ( preg_match('/(<[^><]+)(window\.|javascript:|js:|about:|file:|document\.|vbs:|cookie)([^><]*)/i', $text, $mat) ) {
            $text = str_ireplace($mat[0], $mat[1] . $mat[3], $text);
        }
    }
    return $text;
}
// 过滤
function html($text)
{
    return op_h($text);
}

// 字符串截取，支持中文和其他编码
function msubstr($str, $start=0, $length, $charset="utf-8", $suffix=true)
{
    if ( function_exists("mb_substr") ) {
        $slice = mb_substr($str, $start, $length, $charset);
    } elseif ( function_exists('iconv_substr') ) {
        $slice = iconv_substr($str, $start, $length, $charset);
        if ( false === $slice ) $slice = '';
    } else {
        $re['utf-8'] = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|[\xe0-\xef][\x80-\xbf]{2}|[\xf0-\xff][\x80-\xbf]{3}/";
        $re['gb2312'] = "/[\x01-\x7f]|[\xb0-\xf7][\xa0-\xfe]/";
        $re['gbk'] = "/[\x01-\x7f]|[\x81-\xfe][\x40-\xfe]/";
        $re['big5'] = "/[\x01-\x7f]|[\x81-\xfe]([\x40-\x7e]|\xa1-\xfe])/";

        preg_match_all($re[$charset], $str, $match);
        $slice = join("", array_slice($match[0], $start, $length));
    }

    return $suffix ? $slice.'...' : $slice;
}
// 根据条件字段获取指定表的数据
function get_table_field($value=null, $condition='id', $field=null, $table=null, $db='')
{
  
    if ( empty($value) || empty($table) ) return false;

    $map[$condition] = $value;
    if ( $db == '' ) {
    	$info = db($table)->where($map);
    } else {
    	$info = db($table, 'database.'.$db)->where($map);
    }
   
    if ( empty($field) ) {
        $info = $info->field(true)->find();
    } else {
        $info = $info->value($field);
    }
    
    return $info;
}