<?php
namespace app\admin\my;

class MyPage extends My {
	private $_table = [];
	private $_keyList = [];
	private $_buttonList = [];
	private $_chart = []; // 图表
	private $_page = [];
	private $_data = [];
	private $_db = '';
	private $_xls =[];
	private $_count =[];
	private $_sum =[];
	
	private $_statusUrl = "setStatus"; // 状态修改连接
	private $_deleteTrueUrl = "delete"; // 彻底删除连接

	private $_search = [];
	private $_select = [];
	private $_form = [];
	private $_join = [];

	// -- 状态修改链接
	public function setDb($db) {
		$this->_db = $db;
		return $this;
	}

	public function xls($title='导出自轻时光',$cell) {
		$this->_xls =[
        'title'=>$title,
		'cell'=>$cell
		];
		return $this;
	}

	public function title($title) {
		$this->_table['title'] = $title;
		return $this;
	}

	public function music($path) {
		$this->_table['music'] = $path;
		return $this;
	}

	public function suggest($suggest) {
		$this->_table['suggest'] = $suggest;
		return $this;
	}
	public function tips($content) { // --未使用
		$this->_table['tips'] = $content;
		return $this;
	}
	public function url($url='') {
		$this->_table['url'] = $url;
		return $this;
	}
	public function height($length=1000) {
		$this->_table['height'] = $length;
		return $this;
	}
	

	// -- 状态修改链接
	public function setStatusUrl($url) {
		if ( !$url ) $url="setStatus";
		$this->_statusUrl = $url;
		dump($this->_statusUrl );
		return $this;
	}
	// -- 彻底删除链接
	public function setDeleteTrueUrl($url) {
		if ( $url ) $url = "delete";
		$this->_deleteTrueUrl = $url;
		return $this;
	}

	// --- 按钮
		public function button($title, $attr)
	    {
	        $this->_buttonList[] = array('title' => $title, 'attr' => $attr);
	        return $this;
	    }

		public function ajaxButton($url, $params, $title, $attr=[]) { // ajax按钮
			$attr['class'] = 'btn ajax-post btn-default';
			$attr['url'] = $this->addUrlParam($url, $params);
			$attr['target-form'] = 'ids';
			return $this->button($title, $attr);
		}
		public function buttonModal($url, $params, $title, $attr=[]) { // 模态弹窗按钮
			// $attr data-title 模态框标题 target-form 要传输的数据
			$attr['modal-url'] = $this->addUrlParam($url, $params);
			$attr['data-role'] = 'modal_popup';
			return $this->button($title, $attr);
		}

		 public function buttonNew($href='', $title = '新增', $attr = array())
	    {
	        if ( $href=='' ) { // 默认链接
				$action = request()->action();
				$href = $action."Edit";
			}
	        $attr['href'] = $href;
	        return $this->button($title, $attr);
	    }

	    public function buttonXls(){
            $attr['href'] = url('',['xls'=>1]);
            return $this->button('导出', $attr); 
	    }
	    
		public function buttonEdit($url='', $param=[], $title='编辑', $attr=[]) { // 编辑按钮 - now no use
			if ( $url=='' ) { // 默认链接
				$action = request()->action();
				$url = $action."Edit";
			}

			$attr['url'] = url($url, $param);
			$attr['icon'] = 'glyphicon-edit icon-pencil-square-o';
			$attr['target'] = '_blank';
			return $this->button($title, $attr);
		}
	// -- 状态设置
		public function buttonSetStatus($table, $url, $status, $title, $attr) {
			if ( $url ) {
			} else if ( $this->_statusUrl ) {
				$url = $this->_statusUrl;
			} else {
				$url = "setStatus";
			}

			$attr['class'] = isset($attr['class']) ? $attr['class'] : 'btn ajax-post btn-default';
			$attr['url'] = $this->addUrlParam($url, [
				'table' => $table,
				'status' => $status,
			]);
			$attr['target-form'] = 'ids';
			return $this->button($title, $attr);
		}
		public function buttonDisable($table, $url=null, $title='禁用', $attr=[]) { // 禁用按钮 0
			$attr['class'] = 'btn ajax-post';
			$attr['icon'] = 'glyphicon-remove icon-check-square';
			$attr['ajax'] = true;
			return $this->buttonSetStatus($table, $url, 0, $title, $attr);
		}
		public function buttonEnable($table, $url=null, $title='启用', $attr=[]) { // 启用按钮 1
			$attr['class'] = 'btn ajax-post';
			$attr['icon'] = 'glyphicon-ok icon-check-square-o';
			$attr['ajax'] = true;
			return $this->buttonSetStatus($table, $url, 1, $title, $attr);
		}
		public function buttonTrash($table, $url=null, $title='删除', $attr=[]) { // 软删除按钮 -1
			$attr['class'] = 'btn ajax-post btn-danger confirm';
			$attr['icon'] = 'glyphicon-trash icon-trash-o';
			$attr['confirm'] = true;
			$attr['ajax'] = true;
			return $this->buttonSetStatus($table, $url, -1, $title, $attr);
		}
		public function buttonRestore($table, $url=null, $title='还原', $attr=[]) { // 还原按钮 1
			$attr['class'] = 'btn ajax-post';
			$attr['icon'] = 'glyphicon-share-alt icon-check-square-o';
			$attr['ajax'] = true;
			return $this->buttonSetStatus($table, $url, 1, $title, $attr);
		}
	// -- 其他按钮
		public function buttonClear($model=null) { // 清空回收站按钮 - 删除软删除全部
			return $this->button('清空回收站', [
				'class' => 'btn ajax-post btn-default tox-confirm',
				'data-confirm' => '确定清空回收站？',
				'url' => url('', ['model' => $model]),
				'target-form' => 'ids',
				'hide-data' => 'true'
			]);
		}
		public function buttonDelete($url=null) { // 彻底删除按钮
			if ( !$url ) $url = $this->_deleteTrueUrl;
			$attr['class'] = 'btn ajax-post btn-default tox-confirm';
			$attr['data-confirm'] = '确定删除所有？';
			$attr['url'] = $url;
			$attr['target-form'] = 'ids';
			return $this->button('删除所有', $attr);
		}
		public function buttonSort($href, $title='排序', $attr=[]) { // 排序按钮
			$attr['href'] = $href;
			return $this->button($title, $attr);
		}

	// --- key
		public function key($name, $title, $type, $options=[], $width='300px') {
			$this->_keyList[] = [
				'name' => $name,
				'title' => $title,
				'type' => $type,
				'opt' => $options,
				'width' => $width
			];
			return $this;
		}
	// -- text
		public function keyText($name, $title, $options=[]) {
			return $this->key($name, text($title), 'text', $options);
		}
		public function keyTruncText($name, $title, $length) {
			return $this->key($name, $title, 'trunktext', $length);
		}
		public function keyId($name='id', $title='ID') {
			return $this->keyText($name, $title);
		}
		public function keyTitle($name='title', $title='标题') {
			return $this->keyText($name, $title);
		}
	// -- html
		public function keyHtml($name, $title, $width='150px') {
			return $this->key($name, op_h($title), 'html', null, $width);
		}
		
		public function keyLink($name, $title, $url, $target="_self", $attr=[], $options=[]) {
			$options['target'] = $target;
			$options['attr'] = $attr;
			if ( is_string($url) ) $url = $this->createDefaultGetUrlFunction($url);
			$options['url'] = $url;
			return $this->key($name, $title, 'link', $options);
		}

		public function keyModal($name, $title, $url, $attr=[], $options=[]) {
			if ( !isset($attr['data-type']) ) $attr['data-type'] = 1; // 类型 1 page 2 ipage

			return $this->keyLink($name, $title, $url, "modal_popup", $attr, $options);
		}
	// -- 映射
		public function keyMap($name, $title, $map) {
			return $this->key($name, $title, 'map', $map);
		}
		public function keyYesNo($name, $title) {
			$map = [
				0 => '否',
				1 => '是'
			];
			return $this->keymap($name, $title, $map);
		}
		public function keyBool($name, $title) {
			return $this->keyYesNo($name, $title);
		}
		public function keyStatus($name='status', $title='状态') {
			$map = [
				-1 => '删除',
				0 => '禁用',
				1 => '启用'
			];
			return $this->key($name, $title, 'status', $map);
		}
	// -- 图标
		public function keyIcon($name='icon', $title='图标') {
			return $this->key($name, $title, 'icon');
		}
		public function keyImage($name, $title) {
			return $this->key($name, $title, 'image');
		}
	// -- 时间
		public function keyTime($name, $title) {
			return $this->key($name, $title, 'time');
		}

		public function keyDate($name, $title) {
			return $this->key($name, $title, 'date');
		}


		public function keyCreateTime($name='create_time', $title='创建时间') {
			return $this->keyTime($name, $title);
		}
		public function keyUpdateTime($name='update_time', $title='更新时间') {
			return $this->keyTime($name, $title);
		}
	// -- 关联表字段显示+URL连接
		public function keyJoin($name, $title, $mate, $return, $model, $url='') {
			$map = [
				'name' => $name,
				'mate' => $mate,
				'return' => $return,
				'model' => $model,
				'url' => $url
			];
			return $this->key($name, $title, 'Join', $map);
		}

	// --- 关联
		public function join($name, $return, $table, $field='id') {
			// select 'return' from 'table' where 'name' = 'value'
			$join = [
				'name' => $name,
				'return' => $return, // 返回的数据
				'table' => $table,
				'field' => $field,
			];

			$this->_join[] = $join;

			return $this;
		}
	
	
		public function echart($title='', $subtitle='',$legend=[], $x=[], $y=[],  $series=[],$options=[]) {
			if ( !isset($options['tooltip']) ) $options['tooltip'] = [];
			$options['tooltip'] = json_encode($options['tooltip']);

			$this->_chart = [
				'title' => $title, // 标题
				'subtitle' => $subtitle, // 子标题
				'series' => $series,
				'x' => $x, // x轴
				'y' =>$y, // y轴
				'legend' => $legend,
				'options' => $options,
			];


			return $this;
		}
	
	// --- 操作
		public function action($getUrl, $text, $title='操作', $attr=[]) {
			if ( is_string($getUrl) ) $getUrl = $this->createDefaultGetUrlFunction($getUrl);

			$doActionKey = null;
			foreach ( $this->_keyList as $key ) {
				if ( $key['name'] === 'DOACTIONS' ) {
					$doActionKey = $key;
					break;
				}
			}
			if ( !$doActionKey ) $this->key('DOACTIONS', $title, 'doaction', []);

			$doActionKey = null;
			foreach ( $this->_keyList as &$key ) {
				if ( $key['name'] == 'DOACTIONS' ) {
					$doActionKey = &$key;
					break;
				}
			}

			$doActionKey['opt']['actions'][] = [
				'text' => $text,
				'get_url' => $getUrl,
				'opt' => $attr
			];
			return $this;
		}
		public function actionModal($getUrl, $text, $title='操作', $attr=[]) {
			$attr['data-role'] = 'modal_popup';
			if ( !isset($attr['data-type']) ) $attr['data-type'] = 1; // 类型 1 page 2 ipage

			return $this->action($getUrl, $text, $title, $attr);
		}
	// -- 编辑&状态
		public function actionEdit($url='', $text='编辑', $title='操作', $modal=false, $attr=[]) {
			if ( $url=='' ) {
				$action = request()->action();
				$url = $action."Edit?id=###";
			}

			if ( $modal ) {
				if ( !isset($attr['data-full']) ) $attr['data-full'] = true;

				return $this->actionModal($url, $text, $title, $attr);
			} else {
				return $this->action($url, $text, $title, $attr);
			}
		}
		public function actionStatus($db='') { // -- 设置状态
			$this->_db = $db;
			
			$status = [
				'enable' => ['number' => '1', 'name' => '启用', 'show' => '0'], // 启用
				'disable' => ['number' => '0', 'name' => '禁用', 'show' => '1'], // 禁用
				'delete' => ['number' => '-1', 'name' => '删除', 'show' => '0,1,2'], // 删除
				'revert' => ['number' => '1', 'name' => '还原', 'show' => '-1'], // 还原
				'verify' => ['number' => '1', 'name' => '审核', 'show' => '2'], // 审核
			];
			$that = $this;

			foreach ( $status as $value ) {
				$getUrl = function ($item) use ($that, $status, $value) {
					$value['show'] = explode(',', $value['show']);
				if (isset($item['status'])){
					if ( in_array($item['status'], $value['show']) ) {
						$pattern = str_replace('###', $item['id'], $that->_statusUrl."?id=###"); // id替换
						return $that->addUrlParam($pattern, ['status' => $value['number'], 'table'=>$that->_db]);
					} else {
						return '';
					}
				}
				};
				$this->action($getUrl, $value['name'],'操作',['class' => 'ajax-get']);
			}

			return $this;
		}
		public function actionDelete($title="真删除") { // -- 硬删除
			$that = $this;

			$getUrl = function ($item) use ($that) {
				$pattern = str_replace('###', $item['id'], $that->_deleteTrueUrl."?id=###"); // id替换
				return  $that->addUrlParam($pattern, ['table'=>$that->_db]);
			};

			return $this->action($getUrl, $title,'操作', ['class' => 'ajax-get']);
		}
	
	// --- 过滤
		public function map($name, $title, $type='text', $options=[], $extend=null) {
			switch ( $type ) {
				case 'select':
					return $this->mapSelect($name, $title, $options, $extend);
				break;
				case 'select2':
					return $this->mapSelect2($name, $title, $options, $extend);
				break;
				default:
					return $this->mapText($name, $title, $extend);
			}
		}
		public function mapText($name, $title, $extend=null) {
			$this->_search[] = [
				'name' => $name,
				'title' => $title,
				'type' => 'text',
				'extend' => $extend
			];

			return $this;
		}

		public function mapLine() {
			$this->_search[] = [
				
				'type' => 'line',
				
			];

			return $this;
		}


		public function mapHidden($name, $title, $extend=null) {
			$this->_search[] = [
				'name' => $name,
				'title' => $title,
				'type' => 'text',
				'extend' => $extend
			];

			return $this;
		}
		public function mapSelect($name, $title, $options=[],$extend=null) {
			$this->_search[] = [
				'name' => $name,
				'title' => $title,
				'type' => 'select',
				'options' => $options,
				'extend' => $extend
			];

			return $this;
		}

    //分组统计，按年月周日统计，按指定字段分组，sum ,count指定字段
    //如果有mapgroup则销毁表单
		public function mapGroup( $title, $options=[],$count='',$sum='') {
			
			return $this->mapSelect('group', $title, $options, ['group'=>true,'count'=>$count,'sum'=>$sum]);
		}

		public function mapTotal($name, $title, $count='',$sum='') {
			$this->_search[] = [
				'name' => $name,
				'title' => $title,
				'type' => 'total',
				'count' => $count,
				'sum' => $sum
			];
			return $this;
		}

		

		public function mapSelect2($name, $title, $options=[], $extend=null) {
			$this->_search[] = [
				'name' => $name,
				'title' => $title,
				'type' => 'select2',
				'options' => $options,
				'extend' => $extend
			];

			return $this;
		}
		public function mapIn($name, $title, $options=[]) {
			return $this->mapSelect2($name, $title, $options, ['in' => true]);
		}
		public function mapLike($name, $title, $extend=null) {
			return $this->mapText($name, $title, ['like' => true]);
		}
		public function mapDo($name, $title, $extend=null) {
			return $this->mapText($name, $title, ['do' => true]);
		}
		
		public function mapStatus($name='status', $title='状态', $options=[
			'-1'=>'已删除',
			'0'=>'禁用',
			'1'=>'启用'
		]) {
			return $this->mapIn($name, $title, $options);
		}
		
		public function mapTime($name="time", $title="时间", $type='time') { // 时间搜索
			$this->_search[] = [
				'name' => $name,
				'title' => $title,
				'type' => $type,
			];
			return $this;
		}
		public function mapDate($name="date", $title="日期") { // 日期搜索
			$this->mapTime($name, $title, 'date');
		}
		public function mapDateTime($name="datetime", $title="日期时间") { // 日期时间搜索
			$this->mapTime($name, $title, 'datetime');
		}
		public function mapCreateTime($name="create_time", $title="创建时间") { // 创建时间搜索
			$this->mapDateTime($name, $title);
			return $this;
		}
		public function mapUpdateTime($name="update_time", $title="编辑时间") { // 编辑时间搜索
			$this->mapDateTime($name, $title);
		}
	// -- 数据
		private function convertKey($from, $to, $convertFunction) {
			foreach ( $this->_keyList as &$key ) {
				if ( $key['type'] == $from ) {
					$key['type'] = $to;

					foreach ( $this->_data as &$data ) {
						$value = &$data[$key['name']];
						$value = $convertFunction($value, $key, $data);
						unset($value);
					}
					unset($data);
				}
			}
			unset($key);
		}

		
	    public function page($page)
	    {
	        $this->_page = $page;
	        return $this;
	    }

		public function data($list,$count='',$sum='')
	    {
	        if ($count)$this->_count = $count;
	        if ($sum){
	        	$sum_arr=explode(',',$sum);
	            $this->_sum = $sum_arr;
	        }
	        
	        $this->_data = $list['rows'];
	        $this->_page = ['page_str'=>$list['page_str'],'page'=>$list['page']];
	        return $this;
	    }

	
		public function fetch($template=false, $vars=[], $replace=[], $config=[]) {

			foreach ( $this->_join as $join ) {
				$return_array = explode(",", $join['return']);
				foreach ( $return_array as $val ) {
					$temp[] = $val . " as " . $join['table'] . "_" . $val;
				}
				$return = implode(",", $temp);
				
				foreach ( $this->_data as $key => $value ) {
					$temp = db($join['table'])->field($return)->where([
						$join['field'] => $value[$join['name']],
					])->find();
					$this->_data[$key] = array_merge($value, $temp);
				}
			}

			$this->convertKey('map', 'text', function ($value, $key) { // 映射
				if ( isset($value) ) {
					return $key['opt'][$value];
				} else {
					return '-';
				}
			});
			$this->convertKey('time', 'text', function ($value) { // 时间
				if ( $value != 0 ) {
					return time_format($value);
				} else {
					return '-';
				}
			});

			$this->convertKey('date', 'text', function ($value) { // 时间
				if ( $value != 0 ) {
					return time_format($value,'Y-m-d');
				} else {
					return '-';
				}
			});

			$this->convertKey('trunktext', 'text', function ($value, $key) { // trunktext
				$length = $key['opt'];
				return msubstr($value, 0, $length);
			});

			$this->convertKey('text', 'html', function ($value) { // text
				return $value;
			});
			$this->convertKey('link', 'html', function ($value, $key, $item) { // link
				// $value = htmlspecialchars($value);
				$url = $key['opt']['url'];
				$url = $url($item);

				if ( isset($key['opt']['attr']) ) {
					$content = [];
					foreach ( $key['opt']['attr'] as $ke => $value ) {
						$value = htmlspecialchars($value);
						$content[] = "$ke=\"$value\"";
					}
					$content = implode(' ', $content);
				}

				if ( $key['opt']['target'] == "modal_popup" ) { // 模态弹窗
					return "<a href=\"javascript: void(0);\" modal-url=\"$url\" " . $content . " data-role=\"modal_popup\">$value</a>";
				} else {
					return "<a href=\"$url\" target=\"" . $key['opt']['target'] . "\" class=\"J_menuItem\">$value</a>";
				}
			});

			$this->convertKey('icon', 'html', function ($value, $key, $item) { // icon
				$value = htmlspecialchars($value);
				if ( $value == '' ) {
					$html = '-';
				} else {
					$html = "<i class=\"$value\"></i> $value";
				}
				return $html;
			});
			$this->convertKey('image', 'html', function ($value, $key, $item) { // image
				$value = htmlspecialchars($value);
				if ( intval($value) ) { // value是图片标识 - todo behero
					// $sc_src = get_cover($value, 'path');

					// $src = getThumbImageById($value, 80, 80);
					// $sc_src = $sc_src=='' ? $src : $sc_src;
				} else { // value是图片路径
					$sc_src = $value;
				}
				$html = "<div class='popup-gallery'><a title=\"查看大图\" href=\"$sc_src\"><img src=\"$sc_src\"/ style=\"height: 40px\"></a></div>";
				return $html;
			});
			$this->convertKey('doaction', 'html', function ($value, $key, $item) { // doaction
				$actions = $key['opt']['actions'];
				$result = [];
				foreach ( $actions as $action ) {
					$getUrl = $action['get_url'];
					$linkText = $action['text'];
					$url = $getUrl($item);

					if ( !empty($url) ) {
						if ( isset($action['opt']) ) {
							$content = [];
							foreach ( $action['opt'] as $key => $value ) {
								$value = htmlspecialchars($value);
								$content[] = "$key=\"$value\"";
							}
							$content = implode(' ', $content);
							if ( isset($action['opt']['data-role']) && $action['opt']['data-role'] == "modal_popup" ) { // 模态弹窗
								$result[] = "<a href=\"javascript: void(0);\" modal-url=\"$url\" " . $content . ">$linkText</a>";
							} else {
								$result[] = "<a href=\"$url\" " . $content . ">$linkText</a>";
							}
						} else {
							$result[] = "<a href=\"$url\">$linkText</a>";
						}
					}
				}
				return implode(' ', $result);
			});
			$this->convertKey('Join', 'html', function ($value, $key) { // Join
				if ( $value != '' ) {
					$val = get_table_field($value, $key['opt']['mate'], $key['opt']['return'], $key['opt']['model']);

					if ( !$key['opt']['url'] ) {
						return $val;
					} else {
						$urld = url($key['opt']['url'], [$key['opt']['name'] => $value]);
						return "<a href=\"$urld\" target=\"_blank\">$val</a>";
					}
				} else {
					return '-';
				}
			});

			$statusUrl = $this->_statusUrl;
			$that = &$this;
			
			$this->convertKey('status', 'html', function ($value, $key, $item) use ($statusUrl, $that) { // 状态
				$map = $key['opt'];
				$text = $map[$value];
				
				if ( !$statusUrl ) return $text;

				$switchStatus = $value == 1 ? 0 : 1;
				$url = $that->addUrlParam($statusUrl, [
					'status' => $switchStatus,
					'id' => $item['id'],
					'table' => $that->_db
				]);
				
				return "<a href=\"{$url}\" class=\"ajax-get\">$text</a>";
			});

			$this->convertKey('html', 'html', function ($value) { // 如果html为空
				if ( $value === '' ) return '-';
				if ( $value === NULL ) return '-';
				return $value;
			});


			foreach ( $this->_buttonList as &$button ) { // 编译buttonList中的属性
				$button['tag'] = isset($button['attr']['href']) ? 'a' : 'button';
				$this->addDefaultCssClass($button);
				$button['attr'] = $this->compileHtmlAttr($button['attr']);
			}

			//处理合计
            if ($this->_count or $this->_sum){
            	$total=count($this->_data);
            	if ($this->_sum){
	            	foreach ($this->_sum as $k => $v) {
	            	   	 $$v=0;
	            	}
	            	foreach ($this->_data as $key => $value) {
	            	  foreach ($this->_sum as $k => $v) {
	            	   	 $$v=$$v+$value[$v];
	            	  } 
	            	}
	            	foreach ($this->_sum as $k => $v) {
	            		$this->_data[$total][$v]=$$v;
	            	}
	            }

            	if ($this->_count) $this->_data[$total][$this->_count]=$total;
            	$this->_data[$total]['id']='合计'.$this->_data[$total]['id'];

            	//标记要，处理掉最后一行合计
            	$is_boss=1;
            }

			$this->assign('buttonList', $this->_buttonList); // 按钮
			$this->assign('keyList', $this->_keyList); // 字段

			$this->assign('table', $this->_table); // 表格信息
			$this->assign('searches', $this->_search); // 搜索列表
			
            $this->assign('list', $this->_data);
            $this->assign('page', $this->_page);

            //处理导出
            if (input('xls')!==null){
            	
	            $xls['name']=$this->_xls['title'];
	            $xls['cell']=$this->_xls['cell'];
	            $xls['data']=$this->_data;
		        //处理表头
		        foreach ($xls['cell'] as $k => $v) {
			          if (!in_array($v[0],array_keys($xls['data'][0]))) unset($xls['cell'][$k]);
			    }
			    exportExcel($xls['name'],array_values($xls['cell']),$xls['data']);
		    }

		    //处理图表
            if ($this->_chart){
            	if (isset($is_boss)) array_pop($this->_data);
            	
            	foreach ($this->_data as $key => $value) {
            	   $chat_x_data[]=$value[$this->_chart['x']];
            	   foreach ($this->_chart['legend'] as $k => $v) {
            	     $myseries[$k][]=$value[$v];
            	   }
            	}
                if(isset($myseries)){
	            	$this->_chart['legend']= json_encode(array_keys($this->_chart['legend']));
	            	foreach ($myseries as $serie_k => $serie_v) {
	            		$series[]=[
	            		'name'=>$serie_k,
	            		'type'=>'bar',
	            		'data'=>$serie_v,
	            		];
	            	}
	            	
	            	$this->_chart['x_data']=json_encode($chat_x_data);
	            	$this->_chart['series']=json_encode($series);
	            	$this->assign('chart', $this->_chart); // 图表
                }
		    }




			return parent::fetch("page");
		}


	    // --- 其他
		// -- 添加默认样式 - 按钮
		private function addDefaultCssClass(&$button) {
			if ( !isset($button['attr']['class']) ) {
				$button['attr']['class'] = ' btn btn-sm btn-default';
			} else { // isset($button['attr']['class']
				$button['attr']['class'] .= 'btn btn-sm btn-default';
			}
		}
		// -- 生成链接 id=### other_id={$other_id}
		private function createDefaultGetUrlFunction($pattern) {
			$explode = explode('|', $pattern);
			$pattern = $explode[0];
			$fun = empty($explode[1]) ? 'url' : $explode[1];

			return function ($item) use ($pattern, $fun) {
				$pattern = str_replace('###', $item['id'], $pattern); // id替换

				$view = new \think\View(); // 其他变量替换
				$view->assign($item);

				$pattern = $view->display($pattern);

				return $fun($pattern);
			};
		}
		// -- 附加查询参数
		public function addUrlParam($url, $params) {
			if ( strpos($url, '?') === false ) {
				$seperator = '?';
			} else {
				$seperator = '&';
			}

			$params = http_build_query($params);
			return $url . $seperator . $params;
		}
	// 自动处理清空回收站
	public function clearTrash($model='') {
		if ( IS_POST ) {
			if ( $model != '' ) {
				$aIds = I('post.ids', []);
				if ( !empty($aIds) ) {
					$map['id'] = ['in', $aIds];
				} else {
					$map['status'] = -1;
				}

				$result = model($model)->where($map)->delete();
				if ( $result ) $this->success('_SUCCESS_TRASH_CLEARED_');
				$this->error('_TRASH_ALREADY_EMPTY_');
			} else {
				$this->error('_TRASH_SELECT_');
			}
		}
	}
}