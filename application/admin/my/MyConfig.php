<?php
namespace app\admin\my;

class MyConfig extends My {
    private $_title; // 标题
    private $_suggest; // 副标题
    private $_filter = array(); // 过滤器
    private $_callback = null; // 回调
    private $_data = array(); // 数据
    private $_savePostUrl = array(); // 提交地址
    private $_group = array(); // 分组
    private $_keyList = array(); // 键
    private $_buttonList = array(); // 按钮

    public function title($title) {
        $this->_title = $title;
        $this->meta_title = $title;
        return $this;
    }
    public function suggest($suggest) {
        $this->_suggest = $suggest;
        return $this;
    }
    public function filter($filter) {
        $filter = is_array($filter) ? $filter : explode(',', $filter);
        $this->_filter = $filter;
        return $this;
    }
    public function callback($callback) {
        $this->_callback = $callback;
        return $this;
    }
    public function data($data) {
        $this->_data = $data;
        return $this;
    }
    public function savePostUrl($url) {
        if ( $url ) $this->_savePostUrl = $url;
    }
    public function group($title, $icon, $fields) {
    	$v = array(
    		'title' => $title,
    		'icon' => $icon,
    		'items' => is_array($fields) ? $fields : explode(',', $fields)
    	);
        $this->_group[$title] = $v;
        return $this;
    }
    public function groups($list=array()) {
        foreach ( $list as $key => $v ) {
            $v['items'] = is_array($v['items']) ? $v['items'] : explode(',', $v['items']);
            $this->_group[$key] = $v;
        }
        return $this;
    }
    // -- 键
    public function key($name, $title, $subtitle='', $type, $inline=10, $opt=null) {
        $this->_keyList[] = array(
        	'name' => $name,
        	'title' => $title,
	        'subtitle' => $subtitle,
	        'type' => $type,
	        'inline' => $inline,
	        'opt' => $opt,
        );
        return $this;
    }
    // -- 隐藏字段 hidden
    public function keyHidden($name, $title, $subtitle='', $inline=false) {
        return $this->key($name, $title, $subtitle, 'hidden', $inline);
    }
    // -- 密码
    public function keyPassword($name='password', $title='密码', $subtitle='', $inline=false) {
		return $this->key($name, $title, $subtitle, 'password', $inline);
    }
	// -- 只读文本 readonly
    public function keyReadOnly($name, $title, $subtitle='', $inline=false) {
        return $this->key($name, $title, $subtitle, 'readonly', $inline);
    }
    // -- 标识
    public function keyId($name='id', $title='标识', $subtitle='', $inline=false) {
        return $this->keyReadOnly($name, $title, $subtitle, $inline);
    }
	// -- 文本输入 text
    public function keyText($name, $title, $subtitle='', $inline=false) {
        return $this->key($name, $title, $subtitle, 'text', $inline);
    }
    // -- 标题
    public function keyTitle($name='title', $title='标题', $subtitle='', $inline=false) {
        return $this->keyText($name, $title, $subtitle, $inline);
    }

    // -- 标签
    public function keyLabel($name, $title, $subtitle='', $inline=false) {
        return $this->key($name, $title, $subtitle, 'label', $inline);
    }
	// - 图标-------------------chosen-icon
    public function keyIcon($name='icon', $title='图标', $subtitle='', $inline=false) {
        return $this->key($name, $title, $subtitle, 'icon', $inline);
    }
	// -- 颜色选择器
    public function keyColor($name='color', $title='颜色', $subtitle='', $inline=false) {
        return $this->key($name, $title, $subtitle, 'colorPicker', $inline);
    }
	// -- 文本域 textarea
    public function keyTextArea($name, $title, $subtitle='', $inline=false) {
        return $this->key($name, $title, $subtitle, 'textarea', $inline);
    }
	// - 整型 integer
    public function keyInteger($name, $title, $subtitle='', $inline=false) {
        return $this->key($name, $title, $subtitle, 'integer', $inline);
    }

    public function keyFile($name, $title, $subtitle='', $inline=false) {
        return $this->key($name, $title, $subtitle, 'file', $inline);
    }
    public function keyMultiFile($name, $title, $subtitle='', $inline=false) {
        return $this->key($name, $title, $subtitle, 'multiFile', $inline);
    }
    public function keyImage($name, $title, $subtitle='', $inline=false) {
        return $this->key($name, $title, $subtitle, 'image', $inline);
    }
    public function keyImages($name, $title, $subtitle='', $inline=false, $limit='') {
        return $this->key($name, $title, $subtitle, 'multiImage', $inline, $limit);
    }
    public function keyEditor($name, $title, $subtitle='', $inline=false, $config='', $style=array('width' => '100%', 'height' => '200px')) {
        $toolbars = "toolbars:[[" . $config . "]]";
        if ( empty($config) ) $toolbars = "toolbars:[['source','|','bold','italic','underline','fontsize','forecolor','justifyleft','fontfamily','|','map','emotion','insertimage','insertcode']]";
        if ( $config == 'all' ) $toolbars = 'all';
        $key = array('name' => $name, 'title' => $title, 'subtitle' => $subtitle, 'type' => 'editor', 'config' => $toolbars, 'style' => $style);
        $this->_keyList[] = $key;
        return $this;
    }


    public function keyRadio($name, $title, $subtitle='', $inline=false, $options=[]) {
        return $this->key($name, $title, $subtitle, 'radio', $inline, $options);
    }
    public function keyRadioH($name, $title, $subtitle='', $inline=false, $options=[]) {
        return $this->key($name, $title, $subtitle, 'radio', $inline, $options);
    }
    public function keyRadioV($name, $title, $subtitle='', $inline=false, $options) {
        return $this->key($name, $title, $subtitle, 'radiov', $inline, $options);
    }
    public function keyCheckBox($name, $title, $subtitle='', $options=[],$inline=false) {
        return $this->key($name, $title, $subtitle, 'checkbox', $inline, $options);
    }
    public function keyCheckBoxH($name, $title, $subtitle='', $inline=false, $options=[]) {
        return $this->key($name, $title, $subtitle, 'checkbox', $inline, $options);
    }
    public function keyCheckBoxV($name, $title, $subtitle='', $inline=false, $options=[]) {
        return $this->key($name, $title, $subtitle, 'checkboxv', $inline, $options);
    }
    public function keySelect($name, $title, $options=[], $subtitle='', $inline=false) {
        return $this->key($name, $title, $subtitle, 'select', $inline, $options);
    }

    public function keyBool($name, $title, $subtitle='', $inline=false) {
        $map = array(
        	1 => '是',
        	0 => '否'
        );
        return $this->keyRadio($name, $title, $subtitle, $inline, $map);
    }
    public function keySwitch($name, $title, $subtitle='', $inline=false) {
        $map = array(
        	1 => '开',
        	0 => '关'
        );
        return $this->keyRadio($name, $title, $subtitle, $inline, $map);
    }
    public function keyStatus($name='status', $title='状态', $subtitle='', $inline=false) {
        $map = array(
            -1 => '删除',
            0 =>'禁用',
            1 => '启用'
        );
        return $this->keySelect($name, $title, $map, $subtitle, $inline);
    }

    public function keyTime($name, $title, $subtitle='', $inline=false, $type='datetime') {
        return $this->key($name, $title, $subtitle, $type, $inline);
    }
    public function keyDate($name, $title, $subtitle='', $inline=false) {
        return $this->key($name, $title, $subtitle, 'date', $inline);
    }
    public function keyDateTime($name, $title, $subtitle='', $inline=false) {
        return $this->key($name, $title, $subtitle, 'datetime', $inline);
    }

    public function keyCreateTime($name='create_time', $title='创建时间', $subtitle='', $inline=false) {
        return $this->keyDateTime($name, $title, $subtitle, $inline);
    }
    public function keyUpdateTime($name='update_time', $title='修改时间', $subtitle='', $inline=false) {
        return $this->keyDateTime($name, $title, $subtitle, $inline);
    }


    public function button($title, $attr = array()) {
        $this->_buttonList[] = array('title' => $title, 'attr' => $attr);
        return $this;
    }
    // 确定
    public function buttonSubmit($url='', $title='确定') {
        if ( $url=='' ) $url = url(request()->action(), $_GET);
        $this->savePostUrl($url);

        $attr = array();
        $attr['class'] = "btn submit-btn ajax-post btn-success";
        $attr['id'] = 'submit';
        $attr['type'] = 'submit';
        $attr['target-form'] = 'form-horizontal';
        return $this->button($title, $attr);
    }
	// 返回
    public function buttonBack($title='返回') {
        $attr = array();
        $attr['onclick'] = 'javascript: history.back(-1); return false;';
        $attr['class'] = 'btn btn-return';
        return $this->button($title, $attr);
    }
    // 按钮-链接
    public function buttonLink($title='按钮', $attr) {
        $attr['onclick'] = 'javascript: location.href=\'' . $attr['href'] . '\'; return false;';
        return $this->button($title, $attr);
    }


    public function fetch($template='', $vars=array(), $replace=array(), $config=array()) {
        foreach ( $this->_keyList as &$e ) {
            if ( $e['type'] == 'multiInput' ) $e['name'] = explode('|', $e['name']);

            if ( is_array($e['name']) ) {
                $i = 0;
                $n = count($e['name']);
                while ( $n > 0 ) {
                    $e['value'][$i] = $this->_data[$e['name'][$i]];
                    $i++;
                    $n--;
                }
            } else {
            	if ( isset($this->_data[$e['name']]) ) {
					$e['value'] = $this->_data[$e['name']];
            	} else {
                	$e['value'] = '';
            	}
            }
        }

        // 编译按钮的html属性
        foreach ( $this->_buttonList as &$button ) {
            $button['attr'] = $this->compileHtmlAttr($button['attr']);
        }

        $this->assign('groups', $this->_group);
        $this->assign('title', $this->_title);
        $this->assign('suggest', $this->_suggest);
        $this->assign('keyList', $this->_keyList);
        $this->assign('buttonList', $this->_buttonList);
        $this->assign('savePostUrl', $this->_savePostUrl);

        return parent::fetch('config');
    }

    public function keyMultiSelect($name, $title, $subtitle='', $options, $type='chosen', $inline=false) {
        if ( key($options) === 0 ) { // 索引数组
            if ( !is_array(reset($options)) ) { // 二级不是数组
                foreach ( $options as $key => &$val ) {
                    $val = array($val, $val);
                }
                unset($key, $val);
            }
        } else { // 关联数组
            foreach ( $options as $key => &$val ) {
                foreach ( $val as $k => &$v ) {
                    if ( !is_array($v) ) $v = array($v, $v);
                }
                unset($k, $v);
            }
            unset($key, $val);
        }

        return $this->key($name, $title, $subtitle, $type, $inline, $options);
    }
    public function keyChosen($name, $title, $subtitle='', $options, $inline=false) {
        return $this->keyMultiSelect($name, $title, $subtitle, $options, 'chosen', $inline);
    }
    public function keySelect2($name, $title, $subtitle='', $options, $inline=false) {
        return $this->keyMultiSelect($name, $title, $subtitle, $options, 'select2', $inline);
    }

	// keyMultiInput 输入组组件
    public function keyMultiInput($name, $title, $subtitle, $config, $style=null) {
        empty($style) && $style = 'width: 400px;';
        $key = array(
        	'name' => $name,
        	'title' => $title,
	        'subtitle' => $subtitle,
	        'type' => 'multiInput',
	        'config' => $config,
	        'style' => $style
        );
        $this->_keyList[] = $key;
        return $this;
    }
	// 自动处理配置存储事件，配置项必须全大写
    public function handleConfig() {
       if ( request()->isPost()) { 
            $success = false;
            $configModel = model('admin/Config');

            foreach ( input('') as $k => $v) {
                if ( in_array($k, $this->_filter) ) {
                    $success = 1;
                    continue;
                }
                $config['name'] = '_' . strtoupper(request()->controller()) . '_' . strtoupper($k);
                $config['aid'] = session('aid');
                $config['type'] = 0;
                $config['title'] = '';
                $config['group'] = 0;
                $config['extra'] = '';
                $config['remark'] = '';
                $config['create_time'] = time();
                $config['update_time'] = time();
                $config['status'] = 1;
                $config['value'] = is_array($v) ? implode(',', $v) : $v;
                $config['sort'] = 0;
                $map['aid']=session('aid');
                $map['name']=$config['name'];
                $have=$configModel->where($map)->find();
                if (!$have){
                    $res=$configModel->save($config);
                 }else{
                    $res=$configModel->where($map)->update($config);
                }
                $tag = 'conf_' . strtoupper(request()->controller()) . '_' . strtoupper($k).'_' .session('aid');
                cache($tag, null);

            }

            if ( $res ) $this->success('成功了');
            $this->error('失败了');
                
        } else {
            $configs = db('Config')->where(array('name' => array('like', '_' . strtoupper(request()->controller()) . '_' . '%')))->limit(999)->select();
            $data = array();
            foreach ( $configs as $k => $v ) {
                $key = str_replace('_' . strtoupper(request()->controller()) . '_', '', strtoupper($v['name']));
                $data[$key] = $v['value'];
            }
            return $data;
        }
    }

    private function readUserGroups() {
        $list = model('AuthGroup')->where(array('status' => 1))->order('id asc')->select();
        $result = array();
        foreach ( $list as $group ) {
            $result[$group['id']] = $group['title'];
        }
        return $result;
    }
	// 解析看板数组
    public function parseNestableArray($data, $item=array(), $default=array()) {
        if ( empty($data) ) {
            $head = reset($default);
            if ( !array_key_exists("items", $head) ) {
                $temp = array();
                foreach ( $default as $k => $v ) {
                    $temp[] = array('data-id' => $k, 'title' => $k, 'items' => $v);
                }
                $default = $temp;
            }
            $result = $default;
        } else {
            $data = json_decode($data, true);
            $item_d = getSubByKey($item, 'data-id');
            $all = array();
            foreach ( $data as $key => $v ) {
                $data_id = getSubByKey($v['items'], 'data-id');
                $data_d[$key] = $v;
                unset($data_d[$key]['items']);
                $data_d[$key]['items'] = $data_id ? $data_id : array();
                $all = array_merge($all, $data_id);
            }
            unset($v);
            foreach ( $item_d as $val ) {
                if ( !in_array($val, $all) ) $data_d[0]['items'][] = $val;
            }
            unset($val);
            foreach ( $all as $v ) {
                if ( !in_array($v, $item_d) ) {
                    foreach ( $data_d as $key => $val ) {
                        $key_search = array_search($v, $val['items']);
                        if ( !is_bool($key_search) ) unset($data_d[$key]['items'][$key_search]);
                    }
                    unset($val);
                }
            }
            unset($v);
            $item_t = array();
            foreach ( $item as $val ) {
                $item_t[$val['data-id']] = $val['title'];
            }
            unset($v);
            foreach ( $data_d as &$v ) {
                foreach ( $v['items'] as &$val ) {
                    $t = $val;
                    $val = array();
                    $val['data-id'] = $t;
                    $val['title'] = $item_t[$t];
                }
                unset($val);
            }
            unset($v);

            $result = $data_d;
        }

        return $result;
    }

    public function setDefault($data, $key, $value) {
        $data[$key] = $data[$key]!=null ? $data[$key] : $value;
        return $data;
    }
    public function keyDefault($key, $value) {
        $data = $this->_data;
        $data[$key] = $data[$key]!==null ? $data[$key] : $value;
        $this->_data = $data;
        return $this;
    }
    public function groupLocalComment($group_name, $mod) {
        $mod = strtoupper($mod);
        $this->keyDefault($mod . '_LOCAL_COMMENT_CAN_GUEST', 1);
        $this->keyDefault($mod . '_LOCAL_COMMENT_ORDER', 0);
        $this->keyDefault($mod . '_LOCAL_COMMENT_COUNT', 10);
        $this->keyRadio($mod . '_LOCAL_COMMENT_CAN_GUEST', '_COMMENTS_ALLOW_VISITOR_IF_', '_ALLOW_DEFAULT_', array(0 => '_DISALLOW_', 1 => '_ALLOW_'))
            ->keyRadio($mod . '_LOCAL_COMMENT_ORDER', '_COMMENTS_SORT_', '_DESC_DEFAULT_', array(0 => '_DESC_', 1 => '_ASC_'))
            ->keyText($mod . '_LOCAL_COMMENT_COUNT', '_COMMENTS_PAGE_DISPLAY_COUNT_', '_COMMENTS_PAGE_DISPLAY_COUNT_DESC');
        $this->group($group_name, $mod . '_LOCAL_COMMENT_CAN_GUEST,' . $mod . '_LOCAL_COMMENT_ORDER,' . $mod . '_LOCAL_COMMENT_COUNT');
        return $this;
    }

    public function keyUserDefined($name, $title, $subtitle, $display='', $param='') {
        $this->assign('param', $param);
        $this->assign('name', $name);
        $html = $this->fetch($display);

        $key = array(
        	'name' => $name,
        	'title' => $title,
	        'subtitle' => $subtitle,
	        'type' => 'userDefined',
	        'definedHtml' => $html
        );
        $this->_keyList[] = $key;
        return $this;
    }

    public function addCustomJs($script) {
        $this->assign('myJs', $script);
    }
}