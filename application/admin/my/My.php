<?php
namespace app\admin\my;

// use Admin\Model\AuthGroupModel; // 权限认证
use think\Controller; // 使用头部尾部

abstract class My extends Controller {
    public function fetch($template='', $vars=array(), $replace=array(), $config=array()) {
        $template = dirname(__FILE__) . '/../view/my/' . $template . '.html';
        return parent::fetch($template);
    }

    protected function compileHtmlAttr($attr) {
        $result = array();
        foreach ( $attr as $key=>$value ) {
            $value = htmlspecialchars($value);
            $result[] = "$key=\"$value\"";
        }
        $result = implode(' ', $result);
        return $result;
    }
}