<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
// $Id$
return [

    //模板参数替换
    'view_replace_str'       => array(
        '__CSS__'    => '/static/admin/css',
        '__JS__'     => '/static/admin/js',
        '__IMG__' => '/static/admin/images',
       
        '__STATIC__' => '/static',
        '__PUBLIC__' => '',
        '__WEUI__'   => '/static/weui/dist',
        '__NOTE__'   => '/static/note',

       
    ),

    //管理员状态
    'user_status' => [
        '1' => '正常',
        '2' => '禁止登录'
    ],
    //角色状态
    'role_status' => [
        '1' => '启用',
        '2' => '禁用'
    ],
    'captcha'  => [ // 验证码
        'imageH'   => 30, // 高度
        'imageW'   => 100, // 宽度
        'length'   => 5, // 位数
        'fontSize' => 25, // 字体大小
        'codeSet'  => '2345678abcdefhijkmnpqrstuvwxyzABCDEFGHJKLMNPQRTUVWXY', // 字符集合
        'useCurve' => true, // 混淆
        'reset'    => true, // 验证成功后是否重置 
	],
];
