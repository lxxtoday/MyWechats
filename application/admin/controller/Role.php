<?php
// +----------------------------------------------------------------------
// | my
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://baiyf.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: NickBai <1902822973@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;
use app\admin\my\MyPage;
use app\admin\my\MyConfig;

class Role extends Base
{
   
    public function role()
    {
        $my = new MyPage();
        $my->title('角色列表')->buttonNew("roleEdit")
            ->keyId()
            ->keyText('rolename', '角色名称')
            
            ->actionEdit('roleEdit?id=###', '编辑')
            ->actionStatus('admin_role')
            ->actionEdit('giveAccess?id=###&type=enter', '分配权限');

      
           $data=my_data('AdminRole');
           return $my->data($data)->fetch();
        

    }
    
   
    public function roleEdit($id='')
    {
       
        if(request()->isPost()){
           $res = model('admin/AdminRole')->editData();
           if ($res>0) $this->success('更新成功','role');
           $this->error('更新失败');

        }else{
            $data=my_edit("admin/AdminRole",$id);
            $my = new MyConfig();
            return $my->title("编辑用户")->keyId()
                    ->keyText('rolename', '角色名称')
                    ->data($data)
                    ->fetch(); 
        }

    }

   
    //分配权限
    public function giveAccess()
    {
        $param = input('param.');
        
        if('giveAccess' == $param['type']&&isset($param['rule'])){
            $doparam = [
                'id' => $param['id'],
                'rule' => $param['rule']
            ];
           
            $res = model('admin/AdminRole')->editAccess($doparam);
           
            if ( $res ) {
                $this->success("成功！", 'role');
            } else {
                $this->error("失败！");
            }
            return false;
        }
        
        if('giveAccess' == $param['type']){
            $nodeStr = model('admin/AdminNode')->getNodeInfo($param['id']);
            return json(['code' => 1, 'data' => $nodeStr, 'msg' => 'success']);
        }
        if('enter' == $param['type']){
            return $this->fetch();
        }
        if('index' == $param['type']){
            $this->success("成功！", 'role');
        }

    }
}