<?php
// +----------------------------------------------------------------------
// | my
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://baiyf.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: NickBai <1902822973@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;

class Index extends Base
{
    public function index()
    {
        return $this->fetch('/index');
    }

    /**
     * 后台默认首页
     * @return mixed
     */
    public function indexPage()
    {
        
    // session(null, 'think');
        return $this->fetch('index');
    }
    
    //记住用户是否打开搜索面板
    public function switchAside()
    {
       if (session('ADMIN_SWITCH_ASIDE')==null){
          session('ADMIN_SWITCH_ASIDE',1);
        }else{
          session('ADMIN_SWITCH_ASIDE',null);
       }
    }

     public function dingjia($name='')
    {
        mylog($name);
          session('ONFOCUS',$name);
       
    }



    

}

