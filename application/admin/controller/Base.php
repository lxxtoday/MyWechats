<?php

namespace app\admin\controller;

use app\admin\model\AdminNode;
use think\Controller;

class Base extends Controller
{
    public function _initialize()
    {
       
        if(empty(session('user_auth'))){

            $this->redirect(url('login/index'));
        }


        //检测权限
        $control = request()->controller();
        $action = request()->action();

        //跳过登录系列的检测以及主页权限
        if(!in_array($control, ['login', 'index','Index']) and !is_admin() ){

            if(!in_array($control . '/' . $action, session('action'))){
                $this->error('没有权限',$_SERVER['HTTP_REFERER']);
            }
        }

        //获取权限菜单
        $node = new AdminNode();
        $this->assign([
            'name' => session('user_auth.name'),
            'aid_title' => session('aid_title'),
            'menu' => $node->getMenu(is_admin()?'':session('rule')),
            'rolename' => session('role')
        ]);

    }
    // 加载html - bootstrap table 数据库
    public function loadhtml() {
    	$table = input('table');

    	$map = array('name' => $table);
    	$table_info = db('datatable')->where($map)->find();

    	$field_map = array('datatable_id' => $table_info['id']);
    	$field_info = db('datafield')->where($field_map)->select();

    	$this->assign('table_info', $table_info);
    	$this->assign('field_info', $field_info);
    	
    	return parent::fetch('builder/json_bootstraptable');
    }
    
    public function delete($table, $id) { // 硬删除
    	$res = db($table)->where(array('id' => $id))->delete();
        if ( $res ) {
            $this->success("删除成功！");
        } else {
            $this->error("删除失败！");
        }
    }

    public function setStatus($table, $status=0) { // 软删除 还原 启用 禁用
    	
        $data=input();
        if (isset($data['ids'])){
           $id= implode(",",$data['ids']);
        }else{
            $id= $data['id']; 
        }
        $res = db($table)->where('id', 'in', $id)->update(array('status' => $status));
        if ( $res ) {
        	$this->success("状态设置成功！");
	    } else {
        	$this->error("状态设置失败！");
        }
    }
}