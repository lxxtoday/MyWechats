<?php

namespace app\admin\controller;
use app\admin\my\MyPage;
use app\admin\my\MyConfig;

class Admin extends Base
{
   
    public function admin()
    {
        $data=my_data('AdminAdmin');
        $my = new MyPage();
        $my->title('企业管理')->keyId()
            ->keyText('title', '企业名称')
            ->keyText('mobile', '电话')
            ->keyText('qq', 'QQ')
            ->keyText('motto','广告' )
            ->keyText('address','地址' )
            ->actionEdit('adminEdit?id=###', '编辑');
             if (is_admin())$my->buttonNew()->actionStatus('AdminAdmin');
         $my->mapLike('title|mobile','名称/电话');
        
        
        return $my->data($data)->fetch();
        

    }

    public function adminEdit($id='')
    {
        if(request()->isPost()){
           $res = model('admin/AdminAdmin')->editData();
           if ($res>0) $this->success('更新成功','admin');
           $this->error('更新失败');
        } else {
            $data=my_edit('admin/AdminAdmin',$id);
            $map['aid'] = session('aid');
            $roles = db('AdminRole')->where($map)->select();
            $roles =array_column($roles, 'rolename', 'id');
            $my = new MyConfig();
            return $my->title("编辑用户")->keyId()
                    ->keyText('title', '企业名称')
                    ->keyText('mobile', '联系方式')
                    ->keyText('qq', 'QQ')
                    ->keyText('address', '地址')
                    ->keyText('motto', '广告语')
                    ->keyImage('logo', 'logo')
                    
                    
                    ->data($data)
                    ->fetch();
        }
    }

   
}