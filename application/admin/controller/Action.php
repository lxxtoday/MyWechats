<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: huajie <banhuajie@163.com>
// +----------------------------------------------------------------------

namespace app\admin\controller;

use app\admin\my\MyPage;
use app\admin\my\MyConfig;


class Action extends Base {


    public function actionLog(){

        $my = new MyPage();
        $my->title('行为日志列表')
           
            ->keyId()
            ->keyJoin('action_id', '行为名称', 'id', 'title', 'action')
            ->keyJoin('user_id', '执行者', 'id', 'name', 'admin_user')
            ->keyText('action_ip', '执行者IP')
            ->keyText('remark', '日志内容')
            ->keyTime('create_time', '执行时间');
            $data=my_data('ActionLog');
       
            return $my->data($data)->fetch();
      
    }
    public function scoreLog($r=20,$p=1){

        if(I('type')=='clear'){
            D('ScoreLog')->where(array('id>0'))->delete();
            $this->success('清空成功。',U('scoreLog'));
            exit;
        }else{
            $aUid=I('uid',0,'intval');
            $aType=I('get.type',0,'intval');
            if($aUid){
                $map['uid']=$aUid;
            }
            if($aType){
                $map['type']=$aType;
            }
            $listBuilder=new AdminListBuilder();
            $listBuilder->title('积星日志');
            $map['status']    =   array('gt', -1);
            $scoreLog=D('ScoreLog')->where($map)->order('create_time desc')->findPage($r);

            $scoreTypes=D('Ucenter/Score')->getTypeListByIndex();
            foreach ($scoreTypes as $score){
                $scoreTypesSelect[]=array('value'=>$score['title'],'id'=>$score['id']);
            }
            foreach ($scoreLog['data'] as &$v) {
                $v['adjustType']=$v['action']=='inc'?'增加':'减少';
                $v['scoreType']=$scoreTypes[$v['type']]['title'];
                $class=$v['action']=='inc'?'text-success':'text-danger';
                $v['value']='<span class="'.$class.'">' .  ($v['action']=='inc'?'+':'-'). $v['value']. $scoreTypes[$v['type']]['unit'].'</span>';
                $v['finally_value']= $v['finally_value']. $scoreTypes[$v['type']]['unit'];
            }


            $listBuilder->data($scoreLog['data']);

            $listBuilder->keyId()->keyUid('uid','用户')->keyText('scoreType','积星类型')->keyText('adjustType','调整类型')->keyHtml('value','积星变动')->keyText('finally_value','积星最终值')->keyText('remark','变动描述')->keyCreateTime();
            $listBuilder->pagination($scoreLog['count'],$r);
            $listBuilder->search(L('_SEARCH_'),'uid','text','输入UID');
            $listBuilder->select('积星类型 ','type','select','积星的类型',null,null,$scoreTypesSelect);
            $listBuilder->button('清空日志',array('url'=>U('scoreLog',array('type'=>'clear')),'class'=>'btn ajax-get confirm'));
            $listBuilder->display();
        }



    }

    /**
     * 查看行为日志
     * @author huajie <banhuajie@163.com>
     */
    public function edit($id = 0){
        empty($id) && $this->error(L('_PARAMETER_ERROR_'));

        $info = M('ActionLog')->field(true)->find($id);

        $this->assign('info', $info);
        $this->meta_title = L('_CHECK_THE_BEHAVIOR_LOG_');
        $this->display();
    }

   

    /**
     * 清空日志
     */
    public function clear(){
        $res = M('ActionLog')->where('1=1')->delete();
        if($res !== false){
            $this->success(L('_LOG_EMPTY_SUCCESSFULLY_'));
        }else {
            $this->error(L('_LOG_EMPTY_'));
        }
    }

   

}
