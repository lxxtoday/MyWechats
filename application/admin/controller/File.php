<?php
namespace app\admin\controller;

class File extends Base {
    // 文件上传 
    public function upload() {
        $return  = array('status' => 1, 'info' => lang('upload success'), 'data' => '');
        // 调用文件上传组件上传文件 
        $File = db('file');
        $file_driver = config('DOWNLOAD_UPLOAD_DRIVER');
        $info = $File->upload(
            $_FILES,
            config('DOWNLOAD_UPLOAD'),
            config('DOWNLOAD_UPLOAD_DRIVER'),
            config("UPLOAD_{$file_driver}_CONFIG")
        );

        if ( $info ) { // 记录附件信息 
            $return['data'] = think_encrypt(json_encode($info['download']));
            $return['info'] = $info['download']['name'];
        } else {
            $return['status'] = 0;
            $return['info']   = $File->getError();
        }

        $this->ajaxReturn($return);
    }
    // 下载文件 
    public function download($id=null) {
        if ( empty($id) || !is_numeric($id) ) $this->error(lang('parameter error'));

        $logic = db('Download', 'Logic');
        if ( !$logic->download($id) ) $this->error($logic->getError());
    }
	// 上传图片
    public function uploadPicture() {
        $picture = db('picture');
        $return = array(
        	'status' => 1,
        	'info' => lang('upload success'),
        	'data' => ''
        );

        $driver = modconfig('PICTURE_UPLOAD_DRIVER', 'local', 'config'); // 读取config模块图片上传驱动的配置 默认local
        // $driver = check_driver_is_exist($driver); // 判断驱动是否存在
        // log_file($driver);
        $uploadConfig = get_upload_config($driver);

        $info = $picture->upload($_FILES, config('picture_upload'), $driver, $uploadConfig);
        
        if ( $info ) { // 记录图片信息 
            $return['status'] = 1;
            empty($info['download']) && $info['download']= $info['file'];
            $return = array_merge($info['download'], $return);
        } else {
            $return['status'] = 0;
            $return['info']   = $picture->getError();
        }

        $this->ajaxReturn($return);
    }
}