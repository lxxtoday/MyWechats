<?php
// +----------------------------------------------------------------------
// | my
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://baiyf.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: NickBai <1902822973@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\admin\model\UserType;
use think\Controller;

class Login extends Controller
{
    //登录页面
    public function index(){
        
        $this->assign('myJssdk',['appid'=>'wxa25d0c338370cda1']);

       return $this->fetch('/login');
    
    }
    //登录操作
    public function doLogin()
    {
        $name = input("param.name");
        $password = input("param.password");
        $code = input("param.code");

        $result = $this->validate(compact('name', 'password'), 'AdminValidate');
        if(true !== $result){
            return json(['code' => -5, 'data' => '', 'msg' => $result]);
        }

        $this->validate(compact('code'), [
		    'code' => 'require|captcha'
		]);

        //根据aid来登录并获得相应的权限
        $names = explode('@', $name);
        $name = $names[0];
        $aid = 0;
        if (count($names) == 2) {
            $aid = $names[1];
            $aid_title=db('AdminAdmin')->where('id',$aid)->value('title');
           
        }

        $hasUser = db('admin_user')->where(array('name|mobile' => $name,'aid' => $aid))->find();
        if(empty($hasUser)){
            return json(['code' => -1, 'data' => '', 'msg' => '管理员不存在']);
        }

        if(md5(md5($password) . config('data_auth_key'))!= $hasUser['password']){
            return json(['code' => -2, 'data' => '', 'msg' => '密码错误']);
        }

        if(1 != $hasUser['status']){
            return json(['code' => -6, 'data' => '', 'msg' => '该账号被禁用']);
        }
        

        //获取该管理员的角色信息
        $info = model('AdminRole')->getRoleInfo($hasUser['role_id']);
        if(!$info)$this->error('没有后台权限！');
        session('user_auth', $hasUser);
        session('role', $info['rolename']);  //角色名
        session('rule', $info['rule']);  //角色节点
        session('action', $info['action']);  //角色权限
        session('user_auth_sign', data_auth_sign($hasUser));
        session('aid',$aid);
        session('aid_title',$aid_title);
        session('shopid',$hasUser['shopid']);
        session('boss',$hasUser['boss']);
        session('id',$hasUser['id']);
        session('name',$name);
       

        //更新管理员状态
        $param = [
            'loginnum' => $hasUser['loginnum'] + 1,
            'last_login_ip' => request()->ip(),
            'last_login_time' => time()
        ];

        db('admin_user')->where('id', $hasUser['id'])->update($param);

        //记录行为
        // action_log('user_login', 'Login', $hasUser['id'], $hasUser['id']);

        return json(['code' => 1, 'data' => url('index/index'), 'msg' => '登录成功']);
    }
    //退出操作
    public function loginOut()
    {
        session('user_auth', null);
        session('id', null);
        session('aid', null);
        session('role', null);  //角色名
        session('rule', null);  //角色节点
        session('action', null);  //角色权限
        $this->redirect(url('index'));
    }

    public function loginOutClear()
    {
        session( null);
        $this->redirect(url('index'));
    }


}