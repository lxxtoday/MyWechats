<?php
namespace app\admin\controller;
use think\Controller;
use app\admin\sdk\Thinkwechat;
class Api extends Controller{
    
    public function index($id=0){

        //获取配置连接微信
       
        $weObj=qwe_init($id);
        // dump($weObj->options->toArray());
        $res=$weObj->valid();

        //分解数据获得常用字段
        $this->openid = $weObj->getRev()->getRevFrom();
        $this->type = $weObj->getRev()->getRevType();
        $this->data = $weObj->getRev()->getRevData();
      
        $weObj->text('22:00-24:00系统升级维护')->reply();
      
    }

    // 处理微信消息总入口
    public function messageType(){
       
        switch($this->type) {
            case Thinkwechat::MSGTYPE_TEXT:
                   $reply=model('rob/Rob')->reply($this->data['Content'],$this->data);
                    break;
            case Thinkwechat::MSGTYPE_IMAGE:
                   $reply=model('rob/Rob')->reply("imageP",$this->data,1);  //根据类型进准匹配
                    break;
            case Thinkwechat::MSGTYPE_VOICE:
                   if (isset($this->data['Recognition'])){
                    $keyword=$this->data['Recognition']; 
                   }else{
                     $keyword=$this->type;
                   }
                  
                   $reply=model('rob/Rob')->reply($keyword,$this->data);
                   break;
            case Thinkwechat::MSGTYPE_MUSIC:
                   
                    break;
            
            case Thinkwechat::MSGTYPE_VIDEO:
                

                    break;
            case Thinkwechat::MSGTYPE_LOCATION:
                 
                    break;
            case Thinkwechat::MSGTYPE_LINK:
                  
                    break;
            case Thinkwechat::MSGTYPE_EVENT:
                  $reply=$this->messageEvent();
                    break;
            default: 
                 $reply=['type'=>'text','message'=>'success'];
                  break;
                    
        }
        return $reply;
    }
    // 处理微信事件消息
    public function messageEvent(){
        $reply=[];
        switch($this->data['Event']){
            case Thinkwechat::EVENT_SUBSCRIBE:
                $res=db('WeFans')->where('openid',$this->openid)->update(['subscribe'=>1,'subscribe_time'=>time()]);
                list($answer,$data)=model('WeSceneScan')->scan($this->data);
                $this->data=$data;
                if ($answer) $reply=model('rob/Rob')->reply($answer,$this->data);
               
                break;
            case Thinkwechat::EVENT_UNSUBSCRIBE:
                $res=db('WeFans')->where('openid',$this->openid)->update(['subscribe'=>0,'unsubscribe_time'=>time()]);
                $reply=['type'=>'text','message'=>'success'];
                break;
            case Thinkwechat::EVENT_SCAN:
                list($answer,$data)=model('WeSceneScan')->scan($this->data);
                $this->data=$data;
                if ($answer) $reply=model('rob/Rob')->reply($answer,$this->data);
                break;
            case Thinkwechat::EVENT_LOCATION:
                $res=model('we/WeFansLoction')->loctionAdd($this->data);
                $reply['message']="";
                break;
            case Thinkwechat::EVENT_MENU_CLICK:
                $reply=model('rob/Rob')->reply($this->data['EventKey'],$this->data,1);
                break;
            case Thinkwechat::EVENT_MENU_SCAN_PUSH:
                 
                break;
            case Thinkwechat::EVENT_MENU_SCAN_WAITMSG:
               
                
                break;
            case Thinkwechat::EVENT_MENU_PIC_SYS:
                
                break;
            case Thinkwechat::EVENT_MENU_PIC_PHOTO:
               
                break;
            case Thinkwechat::EVENT_MENU_PIC_WEIXIN:
               
                break;
            case Thinkwechat::EVENT_MENU_LOCATION:
               
                break;
            case Thinkwechat::EVENT_SEND_MASS:
                $reply['message']='success';
                break;
            case Thinkwechat::EVENT_SEND_TEMPLATE:
                //模板消息发送成功
                // $map['ToUserName']=$this->data['FromUserName'];
                // $map['FromUserName']='templateMessage';
                // $message=db('WeMessage')->where($map)->order('CreateTime desc')->find();
                // $res=db('WeMessage')->where(['id'=>$message['id']])->setField('status',1);
                $reply['message']='success';
                break;
            case Thinkwechat::EVENT_KF_SEESION_CREATE:
                
                break;
            case Thinkwechat::EVENT_KF_SEESION_CLOSE:
                
                break;
            case Thinkwechat::EVENT_KF_SEESION_SWITCH:
                
                break;
            case Thinkwechat::EVENT_CARD_PASS:
                
                break;
            case Thinkwechat::EVENT_CARD_NOTPASS:
                
                break;
            case Thinkwechat::EVENT_CARD_USER_GET:
                $reply['message']='恭喜您获得卡券，请在微信卡包查看，欢迎到店使用！';
                 break;
            case Thinkwechat::EVENT_CARD_USER_DEL:
                $reply['message']='您删除了卡券，是我们的卡券优惠力度不够么？反馈建议，获得更大卡券！';
                
                break;
            case Thinkwechat::WifiConnected :
                
                break;
            case Thinkwechat::ShakearoundUserShake :
                
                break;
            default:
                $reply['message']='success';
                break;
           }
            return $reply;
        }


        public function have(){
            
            if (isset($this->data['MsgId'])){
                $have=model('WeMessage')->info($this->data['MsgId']);
            }else{
                $have=db('WeMessage')->where('FromUserName',$this->openid)->where('CreateTime',$this->data['CreateTime'])->find();
            }
            return $have;
            
        }
   
  
}


