<?php
// +----------------------------------------------------------------------
// | my
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://baiyf.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: NickBai <1902822973@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\admin\my\MyPage;
use app\admin\my\MyConfig;

class User extends Base
{
   
    function _initialize()
    {
        parent::_initialize();
        $this->leave =array('1'=>'辞退','2'=>'干得不爽','3'=>'工资不够','4'=>'无理由自离');
        $this->join_from=array(1=>'线下招聘',2=>'熟人介绍',3=>'微信',4=>'全程/智联',5=>'58赶集',6=>'QQ/QQ群');
        $this->gender=array('未知','男','女');
    }

    public function user()
    {
        $shops=my_select('admin_shop');
        $cell=array(array('name','姓名'), array('shopid','分店'), array('mobile','电话'),array('idcard','身份证'),array('bankcard','银行卡'),array('amount','余额'),array('status','状态'),array('id','数量'));
        $group_arr=['shopid'=>'分店统计'];
        $data=my_data('AdminUser',['status'=>['egt',-1]]);

        $my = new MyPage();
        $my ->title('用户列表')
            ->buttonNew("userEdit")->buttonXls();

        if (isset($data['group'])){
            switch ($data['group']) {
                case 'shopid':
                    $my ->keyJoin($data['group'],$group_arr[$data['group']],'id', 'title', 'admin_shop');
                    break;
               
                
                default:
                     $my ->keyText($data['group'],$group_arr[$data['group']]);
                    break;
            }
            
        $my ->keyText('id', '人数')
            ->echart('统计图表','小标题',['人数'=>'id'],$data['group']);
        }else{

        $my ->keyId()
            ->keyText('name', '姓名')
            // ->keyJoin('aid', '集团', 'id', 'title', 'admin_admin')
            ->keyJoin('shopid', '分店', 'id', 'title', 'admin_shop')
            // ->keyJoin('role_id', '后台角色', 'id', 'rolename', 'admin_role')
            ->keyText('mobile', '电话')
            ->keyText('idcard', '身份证')
            ->keyText('bankcard', '银行卡')
            ->keyDate('birthday', '生日')
            ->keyText('amount', '余额')
            ->keyMap('status','状态', [-1=>'离职',0=>'禁用',1=>'正常'])
            
            
            ->actionEdit('userEdit?id=###', '编辑')
            ->action('password?id=###', '重置密码')
            ->actionStatus();
        }

        $my ->mapLike('name','姓名') 
            ->mapLike('mobile','电话') 
            ->mapSelect('shopid', '分店',$shops)
            ->mapSelect('status', '状态', [-1=>'离职',0=>'禁用',1=>'正常'])
             ->mapGroup('统计',$group_arr,'id');
         
            return $my->data($data)->xls('用户信息表',$cell)->fetch();
        

    }

    public function userEdit($id='')
    {
        if(request()->isPost()){
          
           $res = model('admin/AdminUser')->editData();
           if ($res>0) $this->success('更新成功');
           $this->error('更新失败','user');
        } else {
            $shops=my_select('AdminShop');
            $aids=my_select('AdminAdmin');
            $data=my_edit('admin/AdminUser',$id);
            $roles=my_select('AdminRole','id','rolename');
            
           
            $my = new MyConfig();
            $my->title("编辑用户")->keyId();
            if (is_admin())  $my->keySelect('aid','企业',$aids);
                $my ->keyText('name', '姓名')
                    ->keyText('nickname', '昵称')
                    ->keySelect('shopid','分店',$shops)
                    ->keyDate('birthday', '生日')

                    ->keySelect('calendar', '历法',['阳历','农历'])
                    ->keySelect('gender', '性别',$this->gender)
                    ->keySelect('marriage', '婚否',['未知','已婚','未婚'])
                    ->keySelect('status','状态', [-1=>'离职',0=>'禁用',1=>'正常'])
                    ->keyText('position', '职位')
                    ->keyText('mobile', '电话')
                    ->keyText('qq', 'QQ')
                    ->keyText('email', '邮箱')
                    ->keyText('address', '地址')
                    ->keyText('urgent_name', '紧急联系人')
                    ->keyText('urgent_mobile', '紧急联系电话')
                    ->keyText('weixinid', '微信')
                    ->keyText('idcard', '身份证')
                    ->keyText('bankcard', '银行卡')
                    ->keyText('amount', '余额')
                    ->keyBool('boss','店长级权限')

                    ->keyDate('joinday', '入职日期')
                    ->keySelect('join_from', '来自',$this->join_from)

                    ->keyDate('leaveday', '离职日期')
                    ->keySelect('leave_type', '离职类型',$this->leave)
                    ->keyText('leave_why', '离职理由')->keyText('userid', 'Userid');
                if (session('aid')!=session('id')) $my ->keySelect('role_id','角色',$roles);
                   

                $my ->keyEditor('description', '备注')
                    ->data($data)
                    ->group( '基本', 'fa-cog', 'id,name,nickname,gender,shopid,birthday,calendar,marriage')
                    ->group( '联系方式', 'fa-wechat', 'mobile,qq,email,weixinid,address,urgent_name,urgent_mobile')
                    ->group( '入职信息', 'fa-wechat', 'position,idcard,joinday,join_from,description')
                    ->group( '离职信息', 'fa-wechat', 'status,leaveday,leave_type,leave_why')
                    ->group( '财务信息', 'fa-wechat', 'bankcard,amount')
                    ->group( '企业微信', 'fa-wechat', 'userid')
                    ->group( '权限安全', 'fa-wechat', 'role_id,aid,boss');
            return $my->fetch();
        }
    }


    

    public function password($id='')
    {
        if (!$id)$this->error('失败');
        $admin=db('admin_user')->where('id',$id)->find();
        if (!$admin['mobile'])$this->error('失败');
        $password=md5(md5($admin['mobile']) . config('data_auth_key'));
        $res=db('admin_user')->where('id',$id)->update(['password'=>$password]);
        if ($res)$this->success('成功重置用户密码为他的手机号');
        $this->error('失败或者密码本来就是用户的手机号');

    }

   
}