<?php
// +----------------------------------------------------------------------
// | my
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://baiyf.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: NickBai <1902822973@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;
use app\admin\my\MyPage;
use app\admin\my\MyConfig;
use app\admin\my\MySort;


class Node extends Base
{
   
    public function node($pid=0)
    {
        $my = new MyPage();
        $my->title('权限列表') ->buttonNew(url("nodeEdit",['pid'=>$pid]))->buttonSort(url('sort',['pid'=>$pid]))
            ->keyId()
            ->keyIcon()
            ->keyText('title', '名称')
            ->keyJoin('pid', '父级', 'id', 'title', 'admin_node')
            ->keyText('module_name', 'module')
            ->keyText('control_name', 'control')
            ->keyText('action_name', 'action')
            ->keyMap('is_menu', '菜单', ['否','是'])
            ->keyText('sort', '排序')
           
            ->actionEdit('nodeEdit?id=###', '编辑')
            ->Action('node?pid=###', '子权限')
            ->Action('nodeEdit?id=###', '删除');
            $data=my_data('AdminNode',['pid'=>$pid],['order'=>'sort asc']);
            return $my->data($data)->fetch();
       

    }


    public function sort($ids = null,$pid=0)
    {
        if(request()->isPost()){
            $my = new MySort;
            $my->doSort('AdminNode', $ids);
        } else {
            $map['status'] = array('egt', 0);
            $map['pid']=$pid;
            $list = model('AdminNode')->field('id,title,sort')->where($map)->order('sort asc')->select();
           
            foreach ($list as $key => $val) {
                $list[$key]['title'] = $val['title'];
            }
          
            $my = new MySort;
            $my->meta_title = "1111";
            $my->data($list);
            $my->buttonSubmit(url('sort'))->buttonBack();
            return $my->fetch();
        }
    }
    
    public function nodeEdit($id='',$pid='')
    {
        
        if(request()->isPost()){
            $res = model('AdminNode')->editData();
            if ($res) $this->success('更新成功','node');
            $this->error('更新失败','node');
        
        } else {
            $data=my_edit('Admin/AdminNode',$id);
            $data['pid']=$pid;
            $nodes = db('admin_node')->field(true)->select();
            $nodes = model('common/Tree')->toFormatTree($nodes,'title','id','pid');
            $nodes = array_merge(array(0=>array('id'=>0,'title_show'=>'顶级菜单')), $nodes);
            $nodes =array_column($nodes, 'title_show', 'id');
            

            $my = new MyConfig();
            return $my->title('编辑节点')
                ->keyId()
                ->keyText('title', '节点')
                ->keyText('icon', '图标')
                ->keyText('module_name', '模块')
                ->keyText('control_name', '控制器')
                ->keyText('action_name', '操作')
                ->keySelect('is_menu', '菜单显示', ['否',1=>'是'])
                ->keySelect('pid', '父节点',  $nodes)
                ->data($data)
                ->fetch();
        }
    }

    

    
    public function giveAccess()
    {
        $param = input('param.');
        $node = new Node();
        //获取现在的权限
        if('get' == $param['type']){

            $nodeStr = model('admin/AdminNode')->getNodeInfo($param['id']);
            return json(['code' => 1, 'data' => $nodeStr, 'msg' => 'success']);
        }
        //分配新权限
        if('give' == $param['type']){

            $doparam = [
                'id' => $param['id'],
                'rule' => $param['rule']
            ];
            
            $flag = model('admin/AdminUser')->editAccess($doparam);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }
    }
}