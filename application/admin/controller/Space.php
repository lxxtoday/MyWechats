<?php
namespace app\admin\controller;
use think\Controller;
use app\we\sdk\Thinkwechat;


class Space extends Controller{


   
     public function login($openid=''){
        if (!$openid)$this->error('需要识别您的微信身份');
        $fans=model('we/WeFans')->info($openid);
        if(request()->isPost()){
            $data=input('post.');
            $map['mobile']=$data['mobile'];
            $map['aid']=$fans['aid'];
            $map['status']=['egt',1];
            $table=db('admin_user');
            $hasUser =$table ->where($map)->find();
           
            if(empty($hasUser)) $this->error('管理员不存在');
            if(md5(md5($data['password']) . config('data_auth_key'))!= $hasUser['password']) $this->error('密码错误');
            if(1 != $hasUser['status']) $this->error('账号被禁用'); 
            $res=db('WeFans')->where('openid',$openid)->update(['admin'=>$hasUser['id']]);
            if ($res) $this->success($hasUser['name'].'登录成功',url('index',['id'=>$hasUser['id']]));
            $this->error('不需要重复登录');
        }else{
            $this->assign('fans',$fans);
            return $this->fetch();
        }
    }

    public function index($id='',$openid=''){
        if (!$id  and $openid){
            $fans=model('we/WeFans')->info($openid);
            $id=$fans['admin'];
        }
        $admin=model('admin/AdminUser')->info($id);
        $shop=model('admin/AdminShop')->info($admin['shopid']);
        $map['shopid']=$admin['shopid'];
        $map['status']=['egt',0];
        $feed_total=db('we_feed')->where($map)->whereTime('create_time','today')->count();
        $print_total=db('printer_pic')->where($map)->whereTime('create_time','today')->count();
        $buy_total=db('we_fans_buy')->where($map)->whereTime('create_time','today')->sum('price');
        $qrcode_scan=db('we_scene_scan')->where($map)->where('do',0)->whereTime('create_time','today')->count('id');
        $qrcode_subscribe=db('we_scene_scan')->where($map)->where('do',1)->whereTime('create_time','today')->count('id');
        $scan_total=$qrcode_subscribe."/".$qrcode_scan;
        $amount_sum=db('fans_cfo')->where('aid',$admin['aid'])->sum('amount');
        $amount_add=db('we_fans_cfo_log')->where($map)->where('um>0')->whereTime('create_time','today')->sum('um');
        $amount_sale=db('we_fans_cfo_log')->where($map)->where('um<0')->whereTime('create_time','today')->sum('um');

        //考核数据
        $kpi_bad=db('kpi')->where('total_price<0')->where('toadmin',$id)->whereTime('create_time','month')->sum('total_price');
        $kpi_good=db('kpi')->where('total_price>0')->where('toadmin',$id)->whereTime('create_time','month')->sum('total_price');
        
        $this->assign('user',$admin);
        $this->assign('openid',$openid);
        $this->assign('shop',$shop);
        $this->assign('feed_total',$feed_total);
        $this->assign('print_total',$print_total);
        $this->assign('buy_total',$buy_total);
        $this->assign('scan_total',$scan_total);
        $this->assign('amount_sum',$amount_sum);
        $this->assign('amount_add',$amount_add);
        $this->assign('amount_sale',$amount_sale);
        $this->assign('kpi_bad',$kpi_bad);
        $this->assign('kpi_good',$kpi_good);
        
        return $this->fetch();
      
    }

     public function amount($openid=''){
        $user=model('we/WeFans')->info($openid);
        $map['openid']=$openid;
        $map['type']=0;
        $map['status']=['egt',0];
        $logs=db('WeFansCfoLog')->where($map)->select();
       
        $this->assign('logs',$logs);
        $this->assign('user',$user);
        
        return $this->fetch();
      
    }

     public function buy($shopid=''){
        $map['shopid']=$shopid;
        $map['status']=['egt',0];
        $field="count(id) as desk_total,sum(customer) as customer_total,sum(price) as price_total";
        $buy=model('we/WeFansBuy')->field($field)->where($map)->whereTime('create_time','today')->find();
        $yesterday_buy=model('we/WeFansBuy')->field($field)->where($map)->whereTime('create_time','yesterday')->find();
        $week_buy=model('we/WeFansBuy')->field($field)->where($map)->whereTime('create_time','week')->find();
        $shop=model('admin/AdminShop')->info($shopid);
        $this->assign('buy',$buy);
        $this->assign('yesterday_buy',$yesterday_buy);
        $this->assign('week_buy',$week_buy);
        $this->assign('shop',$shop);
        return $this->fetch();
      
    }

     public function feed($shopid='',$admin_openid=''){

       
        $map['shopid']=$shopid;
        $map['status']=['egt',0];
        $map['product|service']=3;
        $feeds_bads=model('we/WeFeed')->where($map)->whereTime('create_time','today')->select();
        $yesterday_bads=model('we/WeFeed')->where($map)->whereTime('create_time','yesterday')->count();
        foreach ($feeds_bads as $key => &$feeds_bad) {
           $feeds_bad['url']=url('we/feed/chatAdmin',['feed_id'=>$feeds_bad['id'],'admin_openid'=>$admin_openid]);
           $feeds_bad['chats']=model('we/WeFeedChat')->chat_total($feeds_bad['id']);
        }
        $map['product|service']=2;
        $feeds_oks=model('we/WeFeed')->where($map)->whereTime('create_time','today')->select();
        $yesterday_oks=model('we/WeFeed')->where($map)->whereTime('create_time','yesterday')->count();
        foreach ($feeds_oks as $key => &$feeds_ok) {
           $feeds_ok['url']=url('we/feed/chatAdmin',['feed_id'=>$feeds_ok['id'],'admin_openid'=>$admin_openid]);
           $feeds_ok['chats']=model('we/WeFeedChat')->chat_total($feeds_ok['id']);
        }
        unset($map['product|service']);
        $map['product']=1;
        $map['service']=1;
        $feeds_goods=model('we/WeFeed')->where($map)->whereTime('create_time','today')->select();
        $yesterday_goods=model('we/WeFeed')->where($map)->whereTime('create_time','yesterday')->count();
        foreach ($feeds_goods as $key => &$feeds_good) {
           $feeds_good['url']=url('we/feed/chatAdmin',['feed_id'=>$feeds_good['id'],'admin_openid'=>$admin_openid]);
           $feeds_good['chats']=model('we/WeFeedChat')->chat_total($feeds_good['id']);
        }

        //回复排行榜
        $chat_table=db('feed_chat');
        $admin_chats=$chat_table->field('count(id) as total,admin_openid,admin_nickname,feed,shopid')->where('shopid',$shopid)->where('status=1')->whereTime('create_time','month')->group('admin_openid')->order('total desc')->select();
        $admin=model('we/WeFans')->info($admin_openid);
        $shop=model('admin/AdminShop')->info($shopid);
        $this->assign('feeds_bads',$feeds_bads);
        $this->assign('feeds_oks',$feeds_oks);
        $this->assign('feeds_goods',$feeds_goods);
        $this->assign('yesterday_bads',$yesterday_bads);
        $this->assign('yesterday_oks',$yesterday_oks);
        $this->assign('yesterday_goods',$yesterday_goods);
        $this->assign('shop',$shop);
        $this->assign('admin_chats',$admin_chats);
        $this->assign('admin',$admin);
        return $this->fetch();
      
    }

     public function ajaxFeedNotice($openid='',$myswitch=0){
        $table=db('we_fans');
        if ($myswitch=='true')$myswitch=1;
        if ($myswitch=='false')$myswitch=0;
        $res=$table->where('openid',$openid)->update(['feed_notice'=>$myswitch]);
     }

     public function scan($shopid=''){

       
        $map['shopid']=$shopid;
        $map['status']=['egt',0];
        $map['product|service']=3;
        
        //回复排行榜
        $scan_table=db('we_scene_scan');
        $admin_scans=$scan_table->field('count(id) as total,shopid,admin')->where('shopid',$shopid)->where('status=1 and admin<>""')->whereTime('create_time','month')->group('admin')->order('total desc')->select();
        foreach ($admin_scans as $key => &$admin_scan) {
            $admin=model('admin/AdminUser')->info($admin_scan['admin']);
            $admin_scan['admin_name']=$admin['name'];
        }
        
        $shop=model('admin/AdminShop')->info($shopid);
       
        $this->assign('shop',$shop);
        $this->assign('admin_scans',$admin_scans);
        return $this->fetch();
      
    }

    public function edit($id='',$old_password='',$password=''){
      $user=model('admin/AdminUser')->info($id);
      $shops=my_select('AdminShop','id','title',['aid'=>$user['aid']]);
      if(request()->isPost()){
        $data=input('post.');
        if($data['password']){
            if (!$data['old_password'])$this->error('请输入原密码以验证您的身份');
            $old_password=md5(md5($data['old_password']) . config('data_auth_key'));
            unset($data['old_password']);
            if ($old_password!=$user['password'])$this->error('原密码不对，请重试，或者联系企业管理员重置');
            $data['password']=md5(md5($data['password']) . config('data_auth_key')); 
        }else{
            unset($data['password']);
            unset($data['old_password']);
        } 

        $data['birthday']=strtotime($data['birthday']);
        $res=db('admin_user')->where('id',$id)->update($data);
        if ($res)$this->success('成功',url('index',['id'=>$id]));
        $this->success('没有更新任何类容');
      }else{
        $this->assign('id',$id);
        $this->assign('user',$user);
        $this->assign('shops',$shops);
        return $this->fetch();
      }
    }


}