<?php
// +----------------------------------------------------------------------
// | my
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://baiyf.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: NickBai <1902822973@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\admin\my\MyPage;
use app\admin\my\MyConfig;

class Shop extends Base
{
   

    public function shop()
    {
        $my = new MyPage();
        $my->title('分店管理')
            ->buttonNew("shopEdit")

            ->keyId()->keyText('title', '分店名称')
            ->keyJoin('aid','集团','id', 'title', 'admin_admin')
            ->keyText('phone', '电话')
            ->keyText('qq', 'QQ')
            ->keyText('address','地址' )
            // ->keyText('older_reward','老顾客提成' )
            ->actionEdit('shopEdit?id=###', '编辑')
            ->actionStatus('AdminShop');
            $data=my_data('AdminShop','','','id asc');
            $my->mapLike('title|phone','名称/电话');
            return $my->data($data)->fetch();
    
    }

    public function shopEdit($id='')
    {
        if(request()->isPost()){
           $res = model('admin/AdminShop')->editData();
           if ($res>0) $this->success('更新成功','shop');
           $this->error('更新失败');
        } else {
            $data=my_edit('admin/AdminShop',$id);
            $my = new MyConfig();
            return $my->title("编辑分店")->keyId()
                    ->keyText('title', '店名')
                    ->keyText('phone', '座机')
                    ->keyText('boss_mobile', '老板手机')
                    ->keyText('manager_mobile', '店长手机')
                    ->keyText('qq', '客服QQ')
                    ->keyText('address', '地址')
                    ->keyText('dz_shopid', '大众店ID')
                    ->keyText('older_reward','老顾客提成' )
                    ->keyText('wifi', 'WIFI密码')
                    ->group( '基本', 'fa-wechat', 'id,title,phone,boss_mobile,manager_mobile,qq,address')
                    ->group( '运营', 'fa-wechat', 'dz_shopid,wifi')
                    ->data($data)->fetch();
        }
    }

   
}