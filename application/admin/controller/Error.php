<?php
namespace app\admin\Controller;

use think\Controller;
use think\Request;

// 空控制器
class Error extends Controller {
	// 空操作
	public function _empty($name='') {
		$control = request()->controller();
		$action = request()->action();
		$vars = request()->param();

		require_once(APP_PATH . lcfirst($control). '/' . 'controller' . '/' .$control. '.php');
		$controller = controller( 'admin/'.$control );

		try {
			$reflect = new \ReflectionMethod($controller, $name);

	        if ( $reflect->getNumberOfParameters() > 0 ) { // 参数个数
	            $params = $reflect->getParameters(); // 参数数组
	    		$type = key($vars) === 0 ? 1 : 0; // 判断数组类型 数字数组时按顺序绑定参数 1索引数组 0关联数组
	        	$args = [];

	            foreach ( $params as $param ) {
	                $name = $param->getName();

	                if ( 1 == $type && !empty($vars) ) { // 索引数组
	                    $args[] = array_shift($vars);
	                } elseif ( 0 == $type && isset($vars[$name]) ) { // 关联数组
	                    $args[] = $vars[$name];
	                } elseif ( $param->isDefaultValueAvailable() ) { // 默认值
	                    $args[] = $param->getDefaultValue();
	                } else { // 参数错误
	                    throw new \InvalidArgumentException('method param miss:' . $name);
	                }
	            }

	            array_walk_recursive($args, [Request::instance(), 'filterExp']); // 全局过滤
	            return $reflect->invokeArgs($controller, $args);
	        } else {
	        	return $reflect->invoke($controller);
	        }
		} catch ( \ReflectionException $e ) {
			$this->error(lang("404"));
		}
	}
}