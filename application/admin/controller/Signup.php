<?php
// +----------------------------------------------------------------------
// | my
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2022 http://baiyf.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: NickBai <1902822973@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\admin\model\UserType;
use think\Controller;

class Signup extends Controller
{
    //登录页面
    public function index(){
        
       if (input('user')){
       $ok='注册成功！请记住账号：'.input('user').' ，密码'.input('password');
       $this->assign('ok',$ok);
       }

       return $this->fetch('/signup');
    
    }
    //登录操作
    public function doSignup()
    {
        mylog(input());
        $name = input("param.name");
        $mobile = input("param.mobile");
        $password = input("param.password");
        
        //创建集团
        $aid= db('admin_admin')->insertGetId(['title'=>$name,'status'=>1,'create_time'=>time()]);
        
        $admin_res= db('admin_admin')->where('id',$aid)->update(['aid'=>$aid]);

        //创建用户
        $data['password']=md5(md5($password) . config('data_auth_key'));
        $data['name']=$name;
        $data['mobile']=$mobile;
        $data['status']=1;
        $data['role_id']=2;
        $data['aid']=$aid;
        $user_res = db('admin_user')->insert($data);
       

        return json(['code' => 1,'data' => url('signup/index',['user'=>$mobile.'@'.$aid,'password'=>$password]),  'msg' => '注册成功！请记住账号：'.$mobile.'@'.$aid.' 密码'.$password]);
        // return json(['code' => 1, 'data' => url('login/index'), 'msg' => '注册成功！请记住账号：'.$mobile.'@'.$aid.' 密码'.$password]);
    }
    //退出操作
    public function loginOut()
    {
        session('user_auth', null);
        session('id', null);
        session('aid', null);
        session('role', null);  //角色名
        session('rule', null);  //角色节点
        session('action', null);  //角色权限
        $this->redirect(url('index'));
    }

    public function loginOutClear()
    {
        session( null);
        $this->redirect(url('index'));
    }


}