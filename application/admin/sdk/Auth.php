<?php
/**
 * 微信oAuth认证示例
 */
namespace app\we\sdk;
use app\we\sdk\Wechat;
class Auth {
	private $options;
	public $openid;
	public $wxuser;
	
	public function __construct($options,$scope="snsapi_base"){
		$this->options = $options;
		$this->$scope=$scope;
		$this->wxoauth();

	}
	
	public function wxoauth(){
		$scope = 'snsapi_base';
		$code = isset($_GET['code'])?$_GET['code']:'';
		$token_time = session('token_time')?session('token_time'):0;

		if(!$code and   session('openid') && session('user_token') && $token_time>time()-3600)
		{
			if (!$this->wxuser) {
				$this->wxuser = session('wxuser');
			}
			$this->openid = session('openid');
			return $this->openid;

		}else{
			$options = array(
					'token'=>$this->options["token"], //填写你设定的key
					'appid'=>$this->options["appid"], //填写高级调用功能的app id
					'appsecret'=>$this->options["appsecret"] //填写高级调用功能的密钥
			);

			$we_obj = new Wechat($options);
			if ($code) {
				$json = $we_obj->getOauthAccessToken();
				
				if (!$json) {
				
					if(session('wx_redirect')) session('wx_redirect',null);
					die('获取用户授权失败，请重新确认');
				}
				session('openid', $json["openid"]);
				$this->openid = $json["openid"];
				$access_token = $json['access_token'];
				session('user_token',$access_token);
				session('token_time', time());

			
				$userinfo = $we_obj->getUserInfo($this->openid);

				if ($userinfo && !empty($userinfo['nickname'])) {
					$this->wxuser = array(
							'openid'=>$this->openid,
							'nickname'=>$userinfo['nickname'],
							'sex'=>intval($userinfo['sex']),
							'location'=>$userinfo['province'].'-'.$userinfo['city'],
							'avatar'=>$userinfo['headimgurl']
					);
				} elseif (strstr($json['scope'],'snsapi_userinfo')!==false) {
					$userinfo = $we_obj->getOauthUserinfo($access_token,$this->openid);
					if ($userinfo && !empty($userinfo['nickname'])) {
						$this->wxuser = array(
								'openid'=>$this->openid,
								'nickname'=>$userinfo['nickname'],
								'sex'=>intval($userinfo['sex']),
								'location'=>$userinfo['province'].'-'.$userinfo['city'],
								'avatar'=>$userinfo['headimgurl']
						);
					} else {
						return $this->openid;
					}
				}
				if ($this->wxuser) {
					session('wxuser',$this->wxuser);
					session('openid', $json["openid"]);
					
				    session('wx_redirect',null);
					return $this->openid;
				}
				$scope = 'snsapi_userinfo';
			}
			if ($scope=='snsapi_base') {
				$url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
				session('wx_redirect',$url);
			} else {
				$url = isset($_SESSION['wx_redirect'])?$_SESSION['wx_redirect']:'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
			}
			if (!$url) {
				session('wx_redirect',null);
				die('获取用户授权失败');
			}

			$oauth_url = $we_obj->getOauthRedirect($url,"wxbase",$scope);
			header('Location:'.$oauth_url);
			die;
			
		}
	}
}
// $options = array(
// 		'token'=>'tokenaccesskey', //填写你设定的key
// 		'appid'=>'wxdk1234567890', //填写高级调用功能的app id, 请在微信开发模式后台查询
// 		'appsecret'=>'xxxxxxxxxxxxxxxxxxx', //填写高级调用功能的密钥
// );
// $auth = new wxauth($options);
// var_dump($auth->wxuser);
