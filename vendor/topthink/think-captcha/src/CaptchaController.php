<?php
namespace think\captcha;

use think\Config;

class CaptchaController {
    public function index($id = "") {
        $captcha = new Captcha((array)Config::get('captcha'));
        return $captcha->entry($id);
    }
}